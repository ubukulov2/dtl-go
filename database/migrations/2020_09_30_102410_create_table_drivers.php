<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableDrivers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('carrier_id');
            $table->string('last_name')->nullable();
            $table->string('first_name')->nullable();
            $table->string('patronymic')->nullable();
            $table->string('birth_date')->nullable();
            $table->string('phone')->unique();
            $table->string('passport_number')->nullable();
            $table->string('passport_date')->nullable();
            $table->string('passport_data')->nullable();
            $table->string('driver_number')->nullable();
            $table->string('driver_date_in')->nullable();
            $table->string('file_driver')->nullable();
            $table->string('med_number')->nullable();
            $table->string('file_med')->nullable();
            $table->timestamps();

            $table->foreign('carrier_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
