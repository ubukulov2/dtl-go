<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Driver extends Model
{
    protected $imagePath = '/uploads/carrier/drivers/';

    protected $fillable = [
        'carrier_id', 'last_name', 'first_name', 'patronymic', 'birth_date', 'phone', 'passport_number',
        'passport_date', 'passport_data', 'driver_number', 'driver_date_in', 'file_driver', 'med_number', 'file_med'
    ];

    public function carrier()
    {
        return $this->belongsTo('App\Models\User', 'carrier_id');
    }

    public static function exists($phone)
    {
        $driver = Driver::where(['phone' => $phone])->first();
        return ($driver) ? true : false;
    }

    public static function my_drivers()
    {
        return Driver::where(['carrier_id' => Auth::id()])->get();
    }

    public function getFullname()
    {
        return $this->last_name." ".$this->first_name. " ".$this->patronymic;
    }

    public function driver_img()
    {
        return url($this->imagePath.$this->file_driver);
    }
}
