<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password', 'first_name', 'last_name', 'phone', 'country_id', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function exists($email)
    {
        $result = User::where(['email' => $email])->first();
        return ($result) ? true : false;
    }

    public function url_cabinet()
    {
        if ($this->type == 'carrier') {
            return route('carrier.cabinet');
        }
    }

    public function getFullname()
    {
        return $this->first_name." ".$this->last_name;
    }

    public function drivers()
    {
        return $this->hasMany('App\Models\Driver', 'carrier_id');
    }

    public function hasDrivers()
    {
        $drivers = Driver::where(['carrier_id' => $this->id])->get();
        return (count($drivers) == 0) ? false : true;
    }
}
