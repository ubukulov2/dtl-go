<?php

namespace App\Http\Controllers\Carrier;

use App\Http\Controllers\BaseController;
use App\Http\Controllers\Controller;
use App\Models\Driver;
use Illuminate\Http\Request;
use Image;

class CarrierController extends BaseController
{
    protected $imagePath = 'uploads/carrier/drivers/';

    public function cabinet()
    {
        return view('carrier.orders');
    }

    public function workers()
    {
        return view('carrier.workers');
    }

    public function drivers()
    {
        $drivers = Driver::my_drivers();
        return view('carrier.drivers', compact('drivers'));
    }

    public function driverCreate()
    {
        return view('carrier.drivers_create');
    }

    public function driverStore(Request $request)
    {
        $data = $request->all();
        if (!Driver::exists($data['phone'])) {
            Driver::create($data);
            return redirect()->route('carrier.drivers');
        }
    }

    public function driverEdit($id)
    {
        $driver = Driver::findOrFail($id);
        return view('carrier.drivers_edit', compact('driver'));
    }

    public function driverUpdate(Request $request, $id)
    {
        $driver = Driver::findOrFail($id);
        $driver->update($request->except(['file_med', 'file_driver']));
        if ($request->hasFile('file_driver')) {
            $file_driver = $request->file('file_driver');
            $img = Image::make($file_driver->getPathname());
            $hash_name = md5($file_driver->getClientOriginalName());
            $file_name = $driver->id."_".$hash_name.'.jpg';
            $save_path = base_path('public/'.$this->imagePath);
            $img->save($save_path.$file_name);
            $driver->file_driver = $file_name;
            $driver->save();
        }
        return redirect()->route('carrier.drivers');
    }
}
