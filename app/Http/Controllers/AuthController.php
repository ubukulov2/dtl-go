<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Auth;

class AuthController extends BaseController
{
    public function signUp()
    {
        $this->seo()->setTitle('Регистрация');
        return view('sign-up');
    }

    public function register(Request $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['user_password']);
//        $data['type'] = ($data['user_type'] == 0) ? 'cargo_owner' : 'carrier';
        $data['type'] = 'carrier';
        $data['country_id'] = 1;
        if (!User::exists($data['email'])) {
            $user = User::create($data);
            Auth::login($user);
            return response([
                'data' => route('carrier.cabinet')
            ], 200);
        } else {
            return response([
                'data' => 'Пользователь уже зарегистрирован'
            ], 409);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('home');
    }

    public function login()
    {
        return view('login');
    }

    public function authenticate(Request $request)
    {
        $data = $request->all();
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            return response([
                'data' => route('carrier.cabinet')
            ], 200);
        } else {
            return response([
                'data' => 'Пользователь не найдено'
            ], 404);
        }
    }
}
