<?php

namespace App\Http\Controllers;

use App\Models\Country;
use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;
use View;

class BaseController extends Controller
{
    use SEOToolsTrait;

    public function __construct()
    {
        $countries = Country::orderBy('id')->get()->toArray();
        View::share('countries', $countries);
    }
}
