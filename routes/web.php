<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@welcome')->name('home');
Route::get('/signup', 'AuthController@signUp')->name('signup');
Route::post('/register/user', 'AuthController@register')->name('register');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/login/user', 'AuthController@authenticate')->name('authenticate');
# Carrier's routes
Route::group(['prefix' => 'carrier', 'namespace' => 'Carrier', 'middleware' => ['web','auth']], function(){
    Route::get('/orders', 'CarrierController@cabinet')->name('carrier.cabinet');
    Route::get('/workers', 'CarrierController@workers')->name('carrier.workers');
    Route::get('/drivers', 'CarrierController@drivers')->name('carrier.drivers');
    Route::get('/drivers/create', 'CarrierController@driverCreate')->name('carrier.drivers.create');
    Route::post('/drivers/store', 'CarrierController@driverStore')->name('carrier.drivers.store');
    Route::get('/drivers/{id}/edit', 'CarrierController@driverEdit')->name('carrier.drivers.edit');
    Route::post('/drivers/{id}/update', 'CarrierController@driverUpdate')->name('carrier.drivers.update');
});
