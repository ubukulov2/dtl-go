<div class="footer ">
    <div class="footer__wrap">
        <div class="menu-two__items">
            <div class="menu-two__item active">
                <div class="footer-head">Документы</div>
                <ul class="list-docs doc">
                    <li class="doc__item doc__pdf">
                        <a href="/files/groozgo_offer.pdf" target="_blank" class="userAgree">
                            Пользовательское соглашение                        </a>
                    </li>
                    <li class="doc__edo">
                        <a href="/edi"  class="edoDoc">
                            ЭДО первичных документов                        </a>
                    </li>
                </ul>
                <div class="footer-subhead">Грузовладельцу</div>
                <ul class="list-docs doc">
                    <li class="doc__item doc__pdf">
                        <a href="/files/groozgo_broker.pdf" class="dogovorBroker" target="_blank">
                            Договор                        </a>
                    </li>
                    <li class="doc__item doc__pdf">
                        <a href="/user-assets/files/broker_manual.pdf" class="manualBroker" target="_blank">
                            Инструкция                        </a>
                    </li>
                    <li class="doc__video">
                        <a href="https://youtu.be/OVEhuNnA9C8" class="videoManualBroker" target="_blank">
                            Видеоинструкция                            </a>
                    </li>
                </ul>
                <div class="footer-subhead">Перевозчику</div>
                <ul class="list-docs doc">
                    <li class="doc__item doc__pdf">
                        <a href="/user-assets/files/groozgo_carrier.pdf" class="dogovorCarrier" target="_blank">
                            Договор с перевозчиком                        </a>
                    </li>
                    <li class="doc__item doc__pdf">
                        <a href="/user-assets/files/carrier_manual.pdf" class="manualCarrier" target="_blank">
                            Инструкция                        </a>
                    </li>
                    <li class="doc__video">
                        <a href="https://youtu.be/i0kz_koKl04" class="videoManualCarrier" target="_blank">
                            Видеоинструкция                            </a>
                    </li>
                    <li class="doc__item doc__pdf">
                        <a href="/files/groozgo_edo_contract.pdf" class="dogovorEdo" target="_blank">
                            Соглашение о ПЭП                        </a>
                    </li>
                    <li class="doc__item doc__pdf">
                        <a href="/user-assets/files/manual.pdf" class="manualTransport" target="_blank">
                            Процедура грузоперевозки                        </a>
                    </li>
                </ul>
            </div>
            <div class="menu-two__item active">
                <div class="footer-head">Услуги</div>
                <ul class="ul-services">
                    <li><a href="/zakaz-perevozki">Найти перевозчика</a></li>
                    <li><a href="/nayti-gruz">Найти груз</a></li>
                    <li><a href="/login">Отследить перевозки</a></li>
                    <li><a href="/uslugi/transportnaya-ekspediciya">Экспедиция грузов</a>
                    </li>
                    <li><a href="/uslugi/straxovanie-gruzov">Страхование груза</a></li>
                </ul>
                <div class="footer-subhead">Мы в соцсетях</div>
                <div class="main-menu__social social">
                    <div class="social__item social__facebook">
                        <a target="_blank" href="https://www.facebook.com/groozgo/"></a>
                    </div>
                    <div class="social__item social__vk">
                        <a target="_blank" href="http://vk.com/groozgo"></a>
                    </div>
                    <!--<div class="social__item social__instagram">
                        <a target="_blank" href="https://www.instagram.com/groozgo/?hl=ru"></a>
                    </div>-->
                </div>
                <div class="footer-subhead">Мобильные приложения</div>
                <p><a href="https://itunes.apple.com/ru/app/groozgo/id1398716560?ls=1&amp;mt=8" target="_blank"><img class="app_link_img" src="/images/app_store.png" alt="Загрузите в App Store" title="AppStore"></a></p>
                <p><a href="https://play.google.com/store/apps/details?id=ru.groozgo" target="_blank"><img class="app_link_img" src="/images/google_play.png" alt="Загрузите на Google Play" title="GooglePlay"></a></p>
            </div>
            <div class="menu-two__item active">
                <div class="footer-head">Контакты</div>
                <p class="call_phone_hide"><a href="tel:8-499-322-48-28"  class="footer_phone_link">8 (499) 322-48-28</a> - для <span class="hidden-sm hidden-xs">звонков из</span> Москвы</p>
                <p class="call_phone_1"><a href="tel:8-800-222-80-57"  class="footer_phone_link">8 (800) 222-80-57</a> - для <span class="hidden-sm hidden-xs">звонков из</span> регионов</p>
                <div class="footer-subhead">E-mail</div>
                <p>
                    <a class="mailto_btn" href="mailto:info@nadamu.kz?Subject=Вопрос%20с%20сайта%20Groozgo.ru">info@nadamu.kz</a>
                </p>
                <div class="footer-subhead">Адрес для корреспонденции</div>
                <p>140073, Московская область, Люберецкий район, п.Томилино, Новорязанского шоссе 23-й км., стр 27/1</p>

                <p><noindex><a href="http://sk.ru" rel="nofollow" target="_blank"><img class="app_link_img" src="/images/Skolkovo.jpg" alt="Логотип Сколково" title="Инновационный центр «Сколково»"></a></noindex></p>
                <p class="copyright">&copy; 2017-2020 NaDamu</p>
            </div>
        </div>
    </div>
</div>    </div>
