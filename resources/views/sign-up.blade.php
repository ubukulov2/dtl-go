@extends('layouts.app2')
@section('content')
    <div class="col-md-12">
        <div class="register-form">
            <register :countries="{{ json_encode($countries) }}"></register>
        </div>
    </div>
@stop
