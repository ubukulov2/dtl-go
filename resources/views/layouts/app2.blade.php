
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta property="og:type" content="website">
    <meta name="robots" content="noindex, nofollow">
{{--    <meta property="og:image" content="https://groozgo.ru/images/groozgo.jpg"/>--}}
{{--    <link rel="image_src" href="https://groozgo.ru/images/groozgo.jpg"/>--}}
    <meta property="og:image:width" content="250"/>
    <meta property="og:image:height" content="250"/>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <link rel="icon" href="/images/favicon.png">

    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    {!! SEO::generate() !!}
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "NaDamu.kz - онлайн сервис компании грузоперевозок",
            "url": "https://NaDamu.kz/",
            "sameAs": [
                "https://vk.com/NaDamu",
                "https://www.facebook.com/NaDamu/",
                "https://www.instagram.com/NaDamu/"
            ]
        }
    </script>
    <link href="/css/bootstrap.min.css?v=1550073338" rel="stylesheet">
    <link href="/css/OpenSans.css?v=1504033189" rel="stylesheet">
    <link href="/css/font-awesome.min.css?v=1523951020" rel="stylesheet">
    <link href="/css/simple-line-icons.min.css?v=1499547189" rel="stylesheet">
    <link href="/css/bootstrap-switch.min.css?v=1499547188" rel="stylesheet">
    <link href="/css/components.min.css?v=1574369993" rel="stylesheet">
    <link href="/css/plugins.min.css?v=1574369993" rel="stylesheet">
    <link href="/css/signup.css?v=1560111327" rel="stylesheet">
    <link href="/css/jquery-ui.min.css?v=1499547189" rel="stylesheet">
    <link href="/css/suggestions.css?v=1504033189" rel="stylesheet">
    <link href="/css/style.css?v=1600439494" rel="stylesheet">
    <link href="/css/horizontal_menu.css?v=1554189165" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">
    <script src="/js/jquery.min.js?v=1516469217"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
    <!--[if lte IE9]>
    <script src="/js/html5shiv.js"></script>
    <![endif]-->
    <style>
        .header {
            position: relative;
        }
    </style>
</head>
<body class="body_view_light ">
<div id="app" class="page">
    <div class="main-menu main-menu_hidden">
        <!--<div class="main-menu__wrap">-->
        <div class="main-menu__links">
            <div class="main-menu__reg-links">
                <a class="link button button_view_action register_btn" href="/signup">Регистрация</a>
                <a class="link button" href="/login">Вход</a>
            </div>
            <ul class="ul-menu">
                <li><a href="/">Главная</a></li>
                <li><a href="/perevozchiky">Перевозчикам</a></li>
                <li><a href="/gruzovladelcy">Грузовладельцам</a></li>
                <li><a href="/voprosy-otvety">Часто задаваемые вопросы</a></li>
                <li><a href="/otzyvy">Отзывы</a></li>
                <li><a href="/o-kompanii">О компании</a></li>
                <li><a href="/podarki"> 🎁 Подарки</a></li>
                <li><a href="/edi">ЭДО</a></li>
                <li><a href="#">Услуги</a>
                    <ul>
                        <li><a href="/uslugi/transportnaya-ekspediciya">Экспедиция грузов</a></li>
                        <li><a href="/uslugi/straxovanie-gruzov">Страхование груза</a></li>
                    </ul>
                </li>
                <li><a href="/tipy-kuzovov-ts">Типы кузовов ТС</a></li>
                <li><a href="/vidy-perevozok">Виды перевозок</a></li>
                <li><a href="/dostavka-urozhaya-v-1-klik">Перевозка с/х грузов</a></li>
                <li><a href="/blog/">Блог</a></li>
            </ul>
            <div class="text-center header__phone call_phone_1 link__back-call">
                <a href="tel:8-800-222-80-57" class="header_phone_link">8 (800) 222-80-57</a><br><br>
            </div>
        </div>
        <!--</div>-->
    </div>
    <div class="page__wrap">
        <div class="loading hidden" id="loading"><div class="loading-anim"></div></div>
        <div class="header header__index header_z-index">
            <div class="header__wrap">

                <div class="hidden-sm hidden-xs">
                    <div class="header__logo header__logo_fix">
                        <div class="logo logo_size_m logo_theme_dark">
                            <a href="/">
                                <div class="logo__img logo_150"></div>
                                <div class="logo__title">Онлайн-сервис грузоперевозок</div>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="header__left">
                    <div class="header__menu-button hidden-md hidden-lg">
                        <div class="menu-button">
                            <span class="menu-button__lines"></span>
                            <div class="menu-button__text">Меню</div>
                        </div>
                    </div>
                    <div class="desktop__menu hidden-sm hidden-xs">
                        <ul class="nav nav-pills nav-main-header">
                            <li><a href="/gruzovladelcy">Грузовладельцам</a></li>
                            <li><a href="/perevozchiky">Перевозчикам</a></li>
                            <li><a href="/voprosy-otvety">FAQ</a></li>
                            <li><a href="/o-kompanii">О компании</a></li>
                            <li><a href="/podarki">🎁 Подарки</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Еще <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/edi">ЭДО</a></li>
                                    <li><a href="/blog/">Блог</a></li>
                                    <li><a href="/otzyvy">Отзывы</a></li>
                                    <li><a href="/uslugi/transportnaya-ekspediciya">Экспедиция грузов</a></li>
                                    <li><a href="/uslugi/straxovanie-gruzov">Страхование груза</a></li>
                                    <li><a href="/vidy-perevozok">Виды перевозок</a></li>
                                    <li><a href="/tipy-kuzovov-ts">Типы кузовов ТС</a></li>
                                    <li><a href="/dostavka-urozhaya-v-1-klik">Перевозка с/х грузов</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="hidden-md hidden-lg header__center__mobile">
                    <div class="header__center">
                        <div class="header__logo header__logo_fix">
                            <div class="logo logo_size_m logo_theme_dark">
                                <a href="/">
                                    <div class="logo__img"></div>
                                    <div class="logo__title">Онлайн-сервис грузоперевозок</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="header__right">
                    <div class="header__account">
                        <div class="account account__guest">
                            <div class="header__contacts">
                                <div class="header__phone call_phone_1">
                                    <a href="tel:8-800-222-80-57" class="header_phone_link">8 (800) 222-80-57</a>
                                </div>
                            </div>
                            <div class="header__buttons">
                                <a class="link button button_view_action register_btn" href="/signup">Регистрация</a>
                                <a class="link button" href="/login">Вход</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
    @include('patterns.footer')
</div>
<div class="modal modal__alert offset-width offset-height" data-modal="alert" data-gui-block="alert">
    <div class="modal__wrap">
        <div class="modal__closer"></div>
        <div class="modal__content">
            <div class="heading heading_level_2" data-gui-field="title"></div>
            <div class="heading message" data-gui-field="message"></div>
        </div>
    </div>
</div>

<div class="modal modal__no_custom_price">
    <div class="modal__wrap">
        <div class="modal__closer"></div>
        <div class="modal__content">
            <div class="heading heading_level_2">Для данного маршрута цена не может быть рассчитана – обратитесь к команде Groozgo</div>
            <!--            --><!--<div class="steps__buttons"><span class="button button_view_light button_size_l button_note_no_price">Сохранить в черновик</span></div>-->        </div>
    </div>
</div><div class="modal modal__back-call" data-modal="back-call">
    <div class="modal__wrap">
        <div class="modal__closer"></div>
        <div class="modal__content">
            <div class="heading heading_level_2">Заказать обратный звонок</div>
            <form class="form" id="callback-landing" method="post" action="/callback">
                <span class="input input_width_available input_theme_dark input_size_l">
                    <input class="input__control" name="name" maxlength="60" placeholder="Как к вам обращаться?"/>
                </span>
                <span class="help-block"></span>

                <span class="input input_width_available input_theme_dark input_size_l">
                    <input class="input__control" name="phone" type="tel" data-gro-mask="phone" maxlength="60" placeholder="Номер телефона"/>
                </span>
                <span class="help-block"></span>

                <div class="modal__buttons">
                    <button class="button button_view_action button_size_l modal__send">Заказать звонок</button>
                </div>

            </form>
        </div>
    </div>
</div><div class="modal fade signup__style" id="modal_sign_up_success" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <div class="modal-body">
                <div id="modal_message"></div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer" id="modal_footer"></div>

        </div>
    </div>
</div>

<script src="/js/bootstrap.js?v=1550073338"></script>
<!--[if lt IE 9]>
<script src="/js/respond.min.js?v=1499547189"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="/js/excanvas.min.js?v=1499547189"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="/js/icons-lte-ie7.js?v=1499547189"></script>
<![endif]-->
<script src="/js/js.cookie.min.js?v=1499547189"></script>
<script src="/js/bootstrap-hover-dropdown.min.js?v=1499547188"></script>
<script src="/js/jquery.slimscroll.min.js?v=1499547189"></script>
<script src="/js/jquery.blockui.min.js?v=1499547189"></script>
<script src="/js/bootstrap-switch.min.js?v=1499547188"></script>
<script src="/js/app.min.js?v=1574369993"></script>
<script src="/js/global.js?v=1575483727"></script>
{{--<script src="/js/jquery.inputmask.bundle.min.js?v=1499547189"></script>--}}
<script src="/js/inputmask.numeric.extensions.min.js?v=1499547189"></script>
<script src="/js/login.js?v=1569399367"></script>
{{--<script src="/js/jquery.maskedinput.min.js?v=1504033192"></script>--}}
<script src="/js/es6-shim.js?v=1523951021"></script>
<script src="/js/es6-promise.auto.min.js?v=1523951021"></script>
<script src="/js/main.js?v=1600439494"></script>
<!--[if lt IE 10]>
<script src="/js/jquery.xdomainrequest.min.js?v=1504033189"></script>
<![endif]-->
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="/js/jquery-ui.min.js?v=1499547189"></script>
<script src="/js/jquery.suggestions.min.js?v=1504033189"></script>
<script src="https://enterprise.api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=49da227a-feea-40d0-9e33-b877e4ee764b&amp;load=package.route,package.map"></script>
<script src="/js/yandex_map_api_counter.js?v=1568235333"></script>
<script src="/js/landing.js?v=1600439494"></script>
{{--<script src="/js/signup-wizard.js?v=1567372229"></script>--}}
<script src="/js/stats.js?v=1574061564"></script>
<script src="/js/app.js"></script>
{{--<script>jQuery(window).on('load', function () {--}}

{{--        (function(d, w, m) {--}}
{{--            window.supportAPIMethod = m;--}}
{{--            var s = d.createElement('script');--}}
{{--            s.type ='text/javascript'; s.id = 'supportScript'; s.charset = 'utf-8';--}}
{{--            s.async = true;--}}
{{--            var id = 'ee4c21a74929debe7d067a98a54c0603';--}}
{{--            s.src = '//lcab.talk-me.ru/support/support.js?h='+id;--}}
{{--            var sc = d.getElementsByTagName('script')[0];--}}
{{--            w[m] = w[m] || function() { (w[m].q = w[m].q || []).push(arguments); };--}}
{{--            if (sc) sc.parentNode.insertBefore(s, sc);--}}
{{--            else d.documentElement.firstChild.appendChild(s);--}}
{{--        })(document, window, 'TalkMe');--}}
{{--    });</script>--}}


<div class="mango-callback" data-settings='{"type":"", "id": "MTAwMDYzMDQ=","autoDial" : "0", "lang" : "ru-ru", "host":"widgets.mango-office.ru/", "errorMessage": "В данный момент наблюдаются технические проблемы и совершение звонка невозможно"}'>
</div>
<script>!function(t){function e(){i=document.querySelectorAll(".button-widget-open");for(var e=0;e<i.length;e++)"true"!=i[e].getAttribute("init")&&(options=JSON.parse(i[e].closest('.'+t).getAttribute("data-settings")),i[e].setAttribute("onclick","alert('"+options.errorMessage+"(0000)'); return false;"))}function o(t,e,o,n,i,r){var s=document.createElement(t);for(var a in e)s.setAttribute(a,e[a]);s.readyState?s.onreadystatechange=o:(s.onload=n,s.onerror=i),r(s)}function n(){for(var t=0;t<i.length;t++){var e=i[t];if("true"!=e.getAttribute("init")){options=JSON.parse(e.getAttribute("data-settings"));var o=new MangoWidget({host:window.location.protocol+'//'+options.host,id:options.id,elem:e,message:options.errorMessage});o.initWidget(),e.setAttribute("init","true"),i[t].setAttribute("onclick","")}}}host=window.location.protocol+"//widgets.mango-office.ru/";var i=document.getElementsByClassName(t);o("link",{rel:"stylesheet",type:"text/css",href:host+"css/widget-button.css"},function(){},function(){},e,function(t){document.documentElement.insertBefore(t,document.documentElement.firstChild)}),o("script",{type:"text/javascript",src:host+"widgets/mango-callback.js"},function(){("complete"==this.readyState||"loaded"==this.readyState)&&n()},n,e,function(t){document.documentElement.appendChild(t)})}("mango-callback");</script>
</body>
</html>
