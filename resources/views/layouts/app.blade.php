
<!DOCTYPE html>
<html lang="ru" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta property="og:type" content="website">
{{--    <meta property="og:image" content="https://groozgo.ru/images/groozgo.jpg"/>--}}
{{--    <link rel="image_src" href="https://groozgo.ru/images/groozgo.jpg"/>--}}
    <meta property="og:image:width" content="250"/>
    <meta property="og:image:height" content="250"/>
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <link rel="icon" href="/images/favicon.png">
    <meta name="robots" content="noindex, nofollow">
    <title>Онлайн сервис грузоперевозок - организация перевозок груза по России, Москве и области: официальный сайт GroozGo</title>
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "NaDamu.kz - онлайн сервис компании грузоперевозок",
            "url": "https://NaDamu.kz/",
            "sameAs": [
                "https://vk.com/NaDamu",
                "https://www.facebook.com/NaDamu/",
                "https://www.instagram.com/NaDamu/"
            ]
        }
    </script>
    <meta name="title" content="Онлайн сервис грузоперевозок - организация перевозок груза по Казахстана, Алматы и области: официальный сайт NaDamu">
    <link href="/css/bootstrap.min.css?v=1550073338" rel="stylesheet">
    <link href="/css/jquery-ui.min.css?v=1499547189" rel="stylesheet">
    <link href="/css/suggestions.css?v=1504033189" rel="stylesheet">
    <link href="/css/font-awesome.min.css?v=1523951020" rel="stylesheet">
    <link href="/css/style.css?v=1600439494" rel="stylesheet">
    <link href="/css/horizontal_menu.css?v=1554189165" rel="stylesheet">
    <link rel="stylesheet" href="/css/app.css">
    <script src="/js/jquery.min.js?v=1516469217"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
    <!--[if lte IE9]>
    <script src="/js/html5shiv.js"></script>
    <![endif]--></head>
<body class=" ">
<div id="app" class="page">

    <div class="main-menu main-menu_hidden">
        <!--<div class="main-menu__wrap">-->
        <div class="main-menu__links">
            <div class="main-menu__reg-links">
                <!--<div class="link button button_view_action link__auth link_modal link__reg" data-modal="auth" data-group="0" data-tabs="auth">Регистрация</div>
<div class="link button link__auth link_modal tab__group_1" data-modal="auth" data-group="1" data-tabs="auth">Вход</div>-->
                <a class="link button button_view_action visible-xs visible-sm register_btn" href="/signup">Регистрация</a>
                <a class="link button visible-xs visible-sm" href="/login">Вход</a>
            </div>
            <ul class="ul-menu">
                <li><a href="/">Главная</a></li>
                <li><a href="#">Перевозчикам</a></li>
                <li><a href="#">Грузовладельцам</a></li>
                <li><a href="#">Часто задаваемые вопросы</a></li>
                <li><a href="#">Отзывы</a></li>
                <li><a href="#">О компании</a></li>
                <li><a href="#"> 🎁 Подарки</a></li>
                <li><a href="#">ЭДО</a></li>
                <li><a href="#">Услуги</a>
                    <ul>
                        <li><a href="#">Экспедиция грузов</a></li>
                        <li><a href="#">Страхование груза</a></li>
                    </ul>
                </li>
                <li><a href="#">Типы кузовов ТС</a></li>
                <li><a href="#">Виды перевозок</a></li>
                <li><a href="#">Перевозка с/х грузов</a></li>
                <li><a href="#">Блог</a></li>
            </ul>
            <div class="text-center header__phone call_phone_1 link__back-call">
                <a href="tel:8-800-222-80-57" class="header_phone_link">8 (800) 222-80-57</a><br><br>
            </div>
        </div>
        <!--</div>-->
    </div>    <div class="page__wrap">
        <div class="loading hidden" id="loading"><div class="loading-anim"></div></div>
        <div class="header header__index header_z-index">
            <div class="header__wrap">

                <div class="hidden-sm hidden-xs">
                </div>

                <div class="header__left">
                    <div class="header__menu-button hidden-md hidden-lg">
                        <div class="menu-button">
                            <span class="menu-button__lines"></span>
                            <div class="menu-button__text">Меню</div>
                        </div>
                    </div>
                    <div class="desktop__menu hidden-sm hidden-xs">
                        <ul class="nav nav-pills nav-main-header">
                            <li><a href="#">Грузовладельцам</a></li>
                            <li><a href="#">Перевозчикам</a></li>
                            <li><a href="#">FAQ</a></li>
                            <li><a href="#">О компании</a></li>
                            <li><a href="#">🎁 Подарки</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Еще <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="#">ЭДО</a></li>
                                    <li><a href="#">Блог</a></li>
                                    <li><a href="#">Отзывы</a></li>
                                    <li><a href="#">Экспедиция грузов</a></li>
                                    <li><a href="#">Страхование груза</a></li>
                                    <li><a href="#">Виды перевозок</a></li>
                                    <li><a href="#">Типы кузовов ТС</a></li>
                                    <li><a href="#">Перевозка с/х грузов</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="hidden-md hidden-lg header__center__mobile">
                    <div class="header__center">
                    </div>
                </div>

                <div class="header__right">
                    <div class="header__account">
                        <div class="account account__guest">
                            <div class="header__contacts">
                                <div class="header__phone call_phone_1">
                                    <a href="tel:8-800-222-80-57" class="header_phone_link">8 (800) 222-80-57</a>
                                </div>
                            </div>
                            <div class="header__buttons">
                                @if(Auth::check())
                                    <a class="link button button_view_action register_btn" href="{{ Auth::user()->url_cabinet() }}">Личный кабинет</a>
                                    <a class="link button" href="{{ route('logout') }}">Выход</a>
                                @else
                                    <a class="link button button_view_action register_btn" href="{{ route('signup') }}">Регистрация</a>
                                    <a class="link button" href="{{ route('login') }}">Вход</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>        <section class="layout section_fit layout_view_main layout_first_index">
            <div class="road-animation">
                <div class="road-animation__video background__mobile"></div>
            </div>
            <div class="layout__content content">
                <div class="content__wrap">
                    <div class="content__center_fit">
                        <div class="logo logo_size_l logo_theme_dark">
                            <div class="logo__img"></div>
                            <div class="logo__title">онлайн-сервис грузоперевозок</div>
                        </div>
                        <div class="promo-main">
                            <div class="promo-main__wrap">
                                <div class="promo-main__title">
                                    <h1 class="h1-main">
                                        <span class="bold">Крупнейший агрегатор перевозчиков</span>
                                        с полной ответственностью за груз и перевозку                        </h1></div>
                            </div>
                        </div>
                        <div class="promo-main__sub__title"><b>14000+</b> единиц транспорта и <b>100+</b> грузов ежедневно</div>
                        <div class="buttons">
                            <a href="#" class="link button button_view_action button_size_l button__no_upper">Я - владелец груза <br> <span class="uppercase__text">рассчитать и заказать </span></a>
                            <a href="#" class="link button button_size_l button__no_upper">Я - перевозчик <br> <span class="uppercase__text"> Найти груз </span></a>
                        </div>
                    </div>
                </div>
                <div class="content__bottom">
                    <div class="promo">
                        <div class="heading heading_level_4 heading_align_center">Технологично</div>
                        <div class="promo__text">
                            Просто, удобно, никаких звонков. Вся организация перевозки онлайн.                </div>
                    </div>
                    <div class="promo">
                        <div class="heading heading_level_4 heading_align_center">Прозрачно</div>
                        <div class="promo__text">
                            Никаких посредников. Онлайн отслеживание. Один договор.                </div>
                    </div>
                    <div class="promo">
                        <div class="heading heading_level_4 heading_align_center">Надежно</div>
                        <div class="promo__text">
                            Проверенные перевозчики. Быстрая оплата. Полная страховка.                </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="layout layout_view_main-owners narrow_content">
            <div class="layout__content content ">
                <div class="content__wrap">

                    <div class="form-owners">
                        <form class="form" action="/zakaz-perevozki" method="get">
                            <div class="form__inputs">
            <span class="input input_theme_black input_size_xl">
                <input class="input__control address-suggestions" name="from" maxlength="200" placeholder="Откуда" value=""/>
            </span>
                                <span class="input input_theme_black input_size_xl">
                <input class="input__control address-suggestions" name="to" maxlength="200" placeholder="Куда"  value=""/>
            </span>

                                <span class="input dropdown input_theme_black input_size_xl">
                <input class="input__control" name="capacity" maxlength="50" placeholder="Тип ТС"/>
                <input name="ts_category" type="hidden"/>
                <input name="type_body" type="hidden"/>
                <ul class="dropdown-menu">
                                        <li class="dropdown-menu__item">
                        <span class="dropdown-menu__item-text item-category"
                              data-value="1,5т/9м3/4плт" data-id = "3">1,5т/9м3/4плт                        </span>
                        <ul>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="1,5т/9м3/4плт Реф" data-category="3"
                                      data-body="1">
                                    Рефрижераторный фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="1,5т/9м3/4плт Изотерм" data-category="3"
                                      data-body="2">
                                    Изотермический фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="1,5т/9м3/4плт Тент" data-category="3"
                                      data-body="3">
                                    Тентованный                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="1,5т/9м3/4плт Борт" data-category="3"
                                      data-body="4">
                                    Бортовой                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="1,5т/9м3/4плт Фургон" data-category="3"
                                      data-body="7">
                                    Фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="1,5т/9м3/4плт П/п ТОНАР >26м3" data-category="3"
                                      data-body="9">
                                    Самосвальный п/п >26м3                                </span>
                            </li>
                                                </ul>
                    </li>
                                        <li class="dropdown-menu__item">
                        <span class="dropdown-menu__item-text item-category"
                              data-value="3т/16м3/6плт" data-id = "5">3т/16м3/6плт                        </span>
                        <ul>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="3т/16м3/6плт Реф" data-category="5"
                                      data-body="1">
                                    Рефрижераторный фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="3т/16м3/6плт Изотерм" data-category="5"
                                      data-body="2">
                                    Изотермический фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="3т/16м3/6плт Тент" data-category="5"
                                      data-body="3">
                                    Тентованный                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="3т/16м3/6плт Борт" data-category="5"
                                      data-body="4">
                                    Бортовой                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="3т/16м3/6плт Фургон" data-category="5"
                                      data-body="7">
                                    Фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="3т/16м3/6плт П/п ТОНАР >26м3" data-category="5"
                                      data-body="9">
                                    Самосвальный п/п >26м3                                </span>
                            </li>
                                                </ul>
                    </li>
                                        <li class="dropdown-menu__item">
                        <span class="dropdown-menu__item-text item-category"
                              data-value="5т/36м3/8плт" data-id = "7">5т/36м3/8плт                        </span>
                        <ul>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="5т/36м3/8плт Реф" data-category="7"
                                      data-body="1">
                                    Рефрижераторный фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="5т/36м3/8плт Изотерм" data-category="7"
                                      data-body="2">
                                    Изотермический фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="5т/36м3/8плт Тент" data-category="7"
                                      data-body="3">
                                    Тентованный                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="5т/36м3/8плт Борт" data-category="7"
                                      data-body="4">
                                    Бортовой                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="5т/36м3/8плт Фургон" data-category="7"
                                      data-body="7">
                                    Фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="5т/36м3/8плт П/п ТОНАР >26м3" data-category="7"
                                      data-body="9">
                                    Самосвальный п/п >26м3                                </span>
                            </li>
                                                </ul>
                    </li>
                                        <li class="dropdown-menu__item">
                        <span class="dropdown-menu__item-text item-category"
                              data-value="10т/54м3/15плт" data-id = "9">10т/54м3/15плт                        </span>
                        <ul>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="10т/54м3/15плт Реф" data-category="9"
                                      data-body="1">
                                    Рефрижераторный фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="10т/54м3/15плт Изотерм" data-category="9"
                                      data-body="2">
                                    Изотермический фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="10т/54м3/15плт Тент" data-category="9"
                                      data-body="3">
                                    Тентованный                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="10т/54м3/15плт Борт" data-category="9"
                                      data-body="4">
                                    Бортовой                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="10т/54м3/15плт Фургон" data-category="9"
                                      data-body="7">
                                    Фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="10т/54м3/15плт П/п ТОНАР >26м3" data-category="9"
                                      data-body="9">
                                    Самосвальный п/п >26м3                                </span>
                            </li>
                                                </ul>
                    </li>
                                        <li class="dropdown-menu__item">
                        <span class="dropdown-menu__item-text item-category"
                              data-value="20т/82м3/33плт" data-id = "11">20т/82м3/33плт                        </span>
                        <ul>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="20т/82м3/33плт Реф" data-category="11"
                                      data-body="1">
                                    Рефрижераторный фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="20т/82м3/33плт Изотерм" data-category="11"
                                      data-body="2">
                                    Изотермический фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="20т/82м3/33плт Тент" data-category="11"
                                      data-body="3">
                                    Тентованный                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="20т/82м3/33плт Борт" data-category="11"
                                      data-body="4">
                                    Бортовой                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="20т/82м3/33плт Фургон" data-category="11"
                                      data-body="7">
                                    Фургон                                </span>
                            </li>
                                                    <li class="dropdown-menu__item ">
                                <span class="dropdown-menu__item-text item-body" data-value="20т/82м3/33плт П/п ТОНАР >26м3" data-category="11"
                                      data-body="9">
                                    Самосвальный п/п >26м3                                </span>
                            </li>
                                                </ul>
                    </li>
                                    </ul>
            </span>
                            </div>
                            <button class="button button_size_l button_view_action modal__send" id="btn_calculate_book">Рассчитать стоимость</button>
                        </form>
                    </div>        </div>
            </div>
        </section>

        <section id="how" class="layout layout_view_how-it-works layout_several">
            <div class="layout__content content">
                <div class="content__wrap">
                    <div class="content__block">

                        <div class="heading heading_level_3 heading_align_center">
                            Сервис подбирает под загрузки ближайшего подходящего перевозчика, что позволяет снизить стоимость перевозок до 10%
                            и найти тс по любому маршруту.                </div>
                        <div class="index-dashboard">
                            <div class="index-dashboard__item">
                                <div class="heading heading_level_3 heading_align_center text-no-dec">Для грузовладельцев</div>
                                <p>
                                    Самый быстрый способ перевезти груз: оставьте онлайн – заказ и получите заявку с данными на машину и водителя от 5 минут до 3 часов в зависимости от направления. Никаких звонков. Только собственники транспорта.                        </p>
                                <div class="index-dashboard__button">
                                    <a href="/gruzovladelcy" class="link button button_bold button_view_action button_size_l">
                                        Я - грузовладелец</a>
                                </div>
                                <br>
                                <div class="index-dashboard__button">
                                    <a href="https://youtu.be/OVEhuNnA9C8" class="link videoManualBroker" target="_blank">
                                        ВИДЕОИНСТРУКЦИЯ                                </a>
                                </div>
                            </div>
                            <div class="index-dashboard__placeholder"></div>
                            <div class="index-dashboard__item">
                                <div class="heading heading_level_3 heading_align_center text-no-dec">Для перевозчиков</div>
                                <p>
                                    Самый быстрый способ найти груз: под Ваши машины будут подобраны грузы напрямую от грузовладельцев.
                                    100+ грузов ежедневно по всей России.                        </p>
                                <div class="index-dashboard__button">
                                    <a href="/perevozchiky" class="link button button_bold button_view_action button_size_l">
                                        Я - перевозчик</a>
                                </div>
                                <br>
                                <div class="index-dashboard__button">
                                    <a href="https://youtu.be/i0kz_koKl04" class="link videoManualCarrier" target="_blank">
                                        ВИДЕОИНСТРУКЦИЯ                                </a>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </section>

        <section class="layout body_view_light">
            <div class="layout__content content">
                <div class="content__wrap">
                    <div class="heading heading_level_1 heading_align_center heading_mobile_align">
                        Часто задаваемые вопросы            </div>
                    <div class="content__block">
                        <div class="slider slider_quatro slider_auto">
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">Сколько стоит грузоперевозка?</div>
                                        <div class="slider__text">
                                            Быстро рассчитайте цену перевозки по любому маршруту в                                    <a href="/zakaz-perevozki">онлайн-заявке</a>.
                                            Не согласны? Укажите свою цену, и перевозчик будет найден по вашей ставке.                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">Как работает сервис?</div>
                                        <div class="slider__text">Вы грузовладелец?<br/>
                                            Оставьте <a href="/zakaz-perevozki">онлайн-заказ</a>,
                                            в течение 3 часов Вам на почту придет заявка с данными на водителя и машину. После перевозки Вам будут доставлены все документы.<br/>
                                            Вы перевозчик?<br/>
                                            Регистрируйтесь, укажите данные машины и водителей и выбирайте подобранные вам грузы.                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">С кем я подписываю договор?</div>
                                        <div class="slider__text">Вы подписываете один договор с ООО &laquo;КПД-ТРАНСПОРТ&raquo; и получаете доступ к 4500 перевозчиков.<br/>
                                            Если вы перевозчик, Вы также подписываете договор с нами и получаете сразу доступ ко всем грузам от прямых грузовладельцев.                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">Как работать в сервисе?</div>
                                        <div class="slider__text">В сервисе могут работать только юридические лица: компании-грузовладельцы или перевозчики-собственники транспорта. В сервисе нет посредников: экспедиторов или диспетчеров.                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">Какие грузоперевозки я могу заказать?</div>
                                        <div class="slider__text">Вы можете заказать междугороднюю перевозку груза полными машинами от 1,5 до 20 тонн. Мы предоставляем все типы машин – рефрижераторы, изотермы, евротенты и борта. Также в сервисе можно заказать городскую доставку в Москве и Московской области. Мы не занимаемся сборными грузами или международными перевозками.                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">Какой срок оплаты?</div>
                                        <div class="slider__text">
                                            Стандартный срок оплаты грузоперевозки  для грузовладельцев – 5 банковских дней по оригиналам документов.                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">Кто берет ответственность за мой груз?</div>
                                        <div class="slider__text">
                                            Онлайн-сервис GroozGo (ООО «КПД-ТРАНСПОРТ») несет полную ответственность за сохранность груза. Наша ответственность застрахована на 5 млн руб. Грузы выше стоимостью страхуются по льготной ставке. Все перевозчики проходят многоуровневую проверку СБ.                                </div>
                                    </div>
                                </div>
                            </div>
                            <div class="slider__item">
                                <div class="slider__item-wrap">
                                    <div class="slider__item-two-wrap">
                                        <div class="slider__title">Как понять, где мой груз?</div>
                                        <div class="slider__text">
                                            Вы можете отследить любую перевозку онлайн прямо из Личного кабинета. Также у каждого пользователя назначается персональный менеджер, к которому можно обратиться с любым вопросом.                                </div>
                                    </div>
                                </div>
                            </div>

                            <div class="slider__arrow slider__left"></div>
                            <div class="slider__arrow slider__right"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="layout layout_bottom-arrow">
            <div class="layout__content content">
                <div class="content__wrap">
                    <div class="heading heading_level_1 heading_align_center heading_mobile_align">
                        Ведущие компании уже работают в NaDamu            </div>
                    <div class="content__block">
                        <div class="logos">
                            <div class="logos__text">
                                <p>В GroozGo могут работать как крупные, так и небольшие компании. Всё, что необходимо сделать -                            <a href="/zakaz-perevozki">оставить заявку</a>
                                    и получать отклики от перевозчиков. Вся организация грузоперевозки - в надежных руках GroozGo.</p>
                            </div>
                            <div class="logos__items">
                                <div class="logos__item"><img src="images/2.png"/></div>
                                <div class="logos__item"><img src="images/3.png"/></div>
                                <div class="logos__item"><img src="images/4.png"/></div>
                                <div class="logos__item"><img src="images/froneri.png"/></div>
                                <div class="logos__item"><img src="images/lamb.png"/></div>
                                <div class="logos__item logos__text-two">...и ещё 1000+ компаний</div>
                            </div>
                        </div>
                    </div>

                    <div id="smi" class="smi">

                        <div class="smi__wrap">
                            <div class="heading heading_level_2 heading_align_center">Мы в СМИ</div>
                            <div class="mass-media">
                                <div class="mass-media__marquee">
                                    <div class="mass-media__wrap">
                                        <div class="mass-media__block">
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="https://www.vedomosti.ru/management/articles/2018/05/18/769914-onlainovii-servis-gruzoperevozok?shared_token=9c566ec35b1a163fc04ee5acf95e655af41e5599&utm_source=facebook&utm_campaign=share&utm_medium=social&utm_content=769914-onlainovii-servis-gruzoperevozok"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/logo-vedomosti.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Две москвички cоздали онлайновый сервис грузоперевозок                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="https://incrussia.ru/understand/obychnyj-biznes-predprinimatelnitsy-o-muzhskih-professiyah-i-batthyortah/"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/inc.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Предпринимательницы — о «мужских» профессиях и баттхёртах                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="https://www.retail.ru/articles/147496/"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/retail.ru.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Ключевые тренды на рынке грузоперевозок и их роль для ритейлеров                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="https://rb.ru/opinion/sozdat-b2b-startap/"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/rus_base1.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Как создать B2B-стартап: ошибки, которые вы точно совершите (и советы, как их избежать)                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="http://www.logistics.ru/automation/news/onlayn-servis-gruzoperevozok-groozgo-vnedryaet-ispolzovanie-elektronnoy-transportnoy"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/logistics_180x180.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Онлайн-сервис грузоперевозок NaDamu внедряет использование электронной транспортной накладной                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="https://vc.ru/37109-v-groozgo-my-2-raza-menyali-biznes-model-i-5-raz-monetizaciyu"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/Vc.ru-logo.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    В NaDamu мы 2 раза меняли бизнес-модель и 5 раз — монетизацию                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="http://www.logistics.ru/index.php/transportation/news/razrabotka-etn-neobhodimye-shagi"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/logistics_180x180.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Разработка ЭТН: необходимые шаги                                         </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="//www.retail-loyalty.org/news/groozgo-nachinaet-sotrudnichestvo-s-kompaniyami-dymov-i-severstal/"
                                                       target="_blank">RETAIL & LOYALTY NEWS</a>
                                                </div>
                                                <div class="mass-media__text">
                                                    NaDamu начинает сотрудничество с компаниями Дымов и Северсталь                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="//transler.ru/news/hi-tech/internet_transport_i_biznes.html"
                                                       target="_blank">Transler</a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Вторая международная конференция и выставка «Интернет+Транспорт» стартовали вчера в столице.                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="//truckandroad.ru/groozego-belaya-dacha-perevozki-20171114"
                                                       target="_blank">Грузовики и дороги</a>
                                                </div>
                                                <div class="mass-media__text">
                                                    NaDamu стал транспортным партнером «Лэм Уэстон Белая Дача»                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__title">
                                                    <a href="//vc.ru/28748-kak-sozdat-svoy-startap-i-ne-sdatsya"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/Vc.ru-logo.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Как создать свой стартап и не сдаться                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__logo">
                                                    <a href="http://www.forbes.ru/tehnologii/339837-dalnoboyshchiki-v-mobayle-chego-zhdat-ot-uber-dlya-gruzoperevozok"
                                                       target="_blank"><span class="mass-media__logo-wrap"><img
                                                                src="images/forbes1.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Дальнобойщики в мобайле: чего ждать от «Uber для грузоперевозок»?                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__logo">
                                                    <a href="https://rb.ru/opinion/b2b-sales-logistics/"><span
                                                            class="mass-media__logo-wrap"><img
                                                                src="images/rus_base1.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    B2B-продажам в логистике пора выходить в онлайн                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__logo">
                                                    <a href="https://www.kommersant.ru/doc/3313431" target="_blank"><span
                                                            class="mass-media__logo-wrap"><img
                                                                src="images/kommersant1.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Из «Сколково» вызвали грузовики ГК КПД купила онлайн-сервис NaDamu                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__logo">
                                                    <a href="http://tass.ru/ekonomika/3775688" target="_blank"><span
                                                            class="mass-media__logo-wrap"><img
                                                                src="images/tass1.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Web Summit в Лиссабоне: беспилотникам, роботам, стартапам нужны стабильность и инвесторы                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__logo">
                                                    <a href="https://cargolink.ru/ls/blog/4715.html" target="_blank"><span
                                                            class="mass-media__logo-wrap"><img
                                                                src="images/kargolink1.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Основатель GroozGo о состоянии рынка: «Перевозчики начали отказываться от загрузки, если цена не устраивает»                                        </div>
                                            </div>
                                            <div class="mass-media__item">
                                                <div class="mass-media__logo">
                                                    <a href="https://rb.ru/opinion/log-tech/" target="_blank"><span
                                                            class="mass-media__logo-wrap"><img
                                                                src="images/rus_base1.png"/></span></a>
                                                </div>
                                                <div class="mass-media__text">
                                                    Какие технологии в логистике используют Amazon, DHL, Alibaba и другие гиганты                                        </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>

        <section id="we-work" class="layout layout_view_we-work layout_several section_height_auto">

            <div class="layout__content content content__have-desc">
                <div class="content__wrap">

                    <div class="heading heading_level_1 heading_center heading_align_center">Мы работаем по всей России</div>
                    <div class="content__block">
                        <!--<p>текст</p>-->
                        <div class="map-russia">
                            <div class="map-russia__cities_1 map-russia__item">
                                <ul>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-kazan">Казань</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-volgograd">Волгоград</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-nizhniy-novgorod">Нижний Новгород</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-novosibirsk">Новосибирск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-ekaterinburg">Екатеринбург</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-krasnoyarsk">Красноярск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-chelyabinsk">Челябинск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-ufa">Уфа</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-vladivostok">Владивосток</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-saratov">Саратов</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-dolgoprydnuy">МО, Долгопрудный</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-barnaul">Барнаул</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-smolensk">Смоленск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-tula">Тула</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-vladimir">Владимир</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-yaroslavl">Ярославль</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-belgorod">Белгород</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-kyrsk">Курск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-izhevsk">Ижевск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-novgorod">Новгород</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-orel">Орел</a></li>
                                </ul>
                            </div>
                            <div class="map-russia__center map-russia__item">
                                <div class="map-russia__img"></div>
                            </div>
                            <div class="map-russia__cities_2 map-russia__item">
                                <ul>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-krasnodar">Краснодар</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-samara">Самара</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-sankt-peterburg">Санкт-Петербург</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-rostov-na-donu">Ростов-на-Дону</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-stavropol-moskva">Ставрополь</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-voronej">Воронеж</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-irkutsk">Иркутск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-tumen">Тюмень</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-omsk">Омск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-perm">Пермь</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-domodedovo">МО, Домодедово</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-dubna">МО, Дубна</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-kaluga">Калуга</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-ryazan">Рязань</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-ivanovo">Иваново</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-tver">Тверь</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-bryansk">Брянск</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-lipeck">Липецк</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-tolyatti">Тольятти</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-pskov">Псков</a></li>
                                    <li><a href="/zakaz-perevozki/perevozki-gruzov-moskva-kostroma">Кострома</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="content__button">
                            <a href="/zakaz-perevozki" class="link button button_size_xl button_transform_none">Рассчитать стоимость</a>
                        </div>
                    </div>

                </div>
            </div>

        </section>

        @include('patterns.footer')
</div>
<div class="modal modal__alert offset-width offset-height" data-modal="alert" data-gui-block="alert">
    <div class="modal__wrap">
        <div class="modal__closer"></div>
        <div class="modal__content">
            <div class="heading heading_level_2" data-gui-field="title"></div>
            <div class="heading message" data-gui-field="message"></div>
        </div>
    </div>
</div>

<div class="modal modal__no_custom_price">
    <div class="modal__wrap">
        <div class="modal__closer"></div>
        <div class="modal__content">
            <div class="heading heading_level_2">Для данного маршрута цена не может быть рассчитана – обратитесь к команде Groozgo</div>
            <!--            --><!--<div class="steps__buttons"><span class="button button_view_light button_size_l button_note_no_price">Сохранить в черновик</span></div>-->        </div>
    </div>
</div><div class="modal modal__back-call" data-modal="back-call">
    <div class="modal__wrap">
        <div class="modal__closer"></div>
        <div class="modal__content">
            <div class="heading heading_level_2">Заказать обратный звонок</div>
            <form class="form" id="callback-landing" method="post" action="/callback">
                <span class="input input_width_available input_theme_dark input_size_l">
                    <input class="input__control" name="name" maxlength="60" placeholder="Как к вам обращаться?"/>
                </span>
                <span class="help-block"></span>

                <span class="input input_width_available input_theme_dark input_size_l">
                    <input class="input__control" name="phone" type="tel" data-gro-mask="phone" maxlength="60" placeholder="Номер телефона"/>
                </span>
                <span class="help-block"></span>

                <div class="modal__buttons">
                    <button class="button button_view_action button_size_l modal__send">Заказать звонок</button>
                </div>

            </form>
        </div>
    </div>
</div><div class="modal fade signup__style" id="modal_sign_up_success" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="modal_title"></h4>
            </div>
            <div class="modal-body">
                <div id="modal_message"></div>
                <div class="clearfix"></div>
            </div>
            <div class="modal-footer" id="modal_footer"></div>

        </div>
    </div>
</div>

<script src="/js/es6-shim.js?v=1523951021"></script>
<script src="/js/es6-promise.auto.min.js?v=1523951021"></script>
<script src="/js/main.js?v=1600439494"></script>
<!--[if lt IE 10]>
<script src="/js/jquery.xdomainrequest.min.js?v=1504033189"></script>
<![endif]-->
<script src="/js/bootstrap.js?v=1550073338"></script>
<script src="/js/js.cookie.min.js?v=1499547189"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="/js/jquery-ui.min.js?v=1499547189"></script>
<script src="/js/jquery.suggestions.min.js?v=1504033189"></script>
{{--<script src="https://enterprise.api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=49da227a-feea-40d0-9e33-b877e4ee764b&amp;load=package.route,package.map"></script>--}}
<script src="/js/yandex_map_api_counter.js?v=1568235333"></script>
<script src="/js/jquery.maskedinput.min.js?v=1504033192"></script>
<script src="/js/landing.js?v=1600439494"></script>
<script src="/js/signup-wizard.js?v=1567372229"></script>
<script src="/js/stats.js?v=1574061564"></script>
<script src="/js/app.js"></script>
<script>var no_regions = ["Республика Дагестан","Республика Ингушетия","Республика Калмыкия","Карачаево-Черкесская Республика","Республика Северная Осетия-Алания","Чеченская республика","Приморский край","Хабаровский край","Калининградская область","Камчатская область","Магаданская область","Еврейская автономная область","Коми-Пермяцкий автономный округ","Корякский автономный округ","Ненецкий автономный округ","Таймырский (Долгано-Ненецкий) автономный округ","Усть-Ордынский Бурятский автономный округ","Чукотский автономный округ","Эвенкийский автономный округ","Республика Крым","Алматинская область"];</script>

<div class="mango-callback" data-settings='{"type":"", "id": "MTAwMDYzMDQ=","autoDial" : "0", "lang" : "ru-ru", "host":"widgets.mango-office.ru/", "errorMessage": "В данный момент наблюдаются технические проблемы и совершение звонка невозможно"}'>
</div>
<script>!function(t){function e(){i=document.querySelectorAll(".button-widget-open");for(var e=0;e<i.length;e++)"true"!=i[e].getAttribute("init")&&(options=JSON.parse(i[e].closest('.'+t).getAttribute("data-settings")),i[e].setAttribute("onclick","alert('"+options.errorMessage+"(0000)'); return false;"))}function o(t,e,o,n,i,r){var s=document.createElement(t);for(var a in e)s.setAttribute(a,e[a]);s.readyState?s.onreadystatechange=o:(s.onload=n,s.onerror=i),r(s)}function n(){for(var t=0;t<i.length;t++){var e=i[t];if("true"!=e.getAttribute("init")){options=JSON.parse(e.getAttribute("data-settings"));var o=new MangoWidget({host:window.location.protocol+'//'+options.host,id:options.id,elem:e,message:options.errorMessage});o.initWidget(),e.setAttribute("init","true"),i[t].setAttribute("onclick","")}}}host=window.location.protocol+"//widgets.mango-office.ru/";var i=document.getElementsByClassName(t);o("link",{rel:"stylesheet",type:"text/css",href:host+"css/widget-button.css"},function(){},function(){},e,function(t){document.documentElement.insertBefore(t,document.documentElement.firstChild)}),o("script",{type:"text/javascript",src:host+"widgets/mango-callback.js"},function(){("complete"==this.readyState||"loaded"==this.readyState)&&n()},n,e,function(t){document.documentElement.appendChild(t)})}("mango-callback");</script>
</body>
</html>
