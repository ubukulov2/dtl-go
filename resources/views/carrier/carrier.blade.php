<!DOCTYPE html>
<!--[if IE 8]>
<html lang='en' class="ie8 no-js"
      prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if IE 9]>
<html lang='en' class="ie9 no-js"
      prefix="og: http://ogp.me/ns#"><![endif]-->
<!--[if !IE]>
<html lang='en' prefix="og: http://ogp.me/ns#"><![endif]-->
<head>
    <meta charset="UTF-8">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://groozgo.ru/images/groozgo.jpg"/>
    <link rel="image_src" href="https://groozgo.ru/images/groozgo.jpg"/>
    <meta property="og:image:width" content="250"/>
    <meta property="og:image:height" content="250"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">

    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token" content="XEwVFjRM2OZICbywdwIogFMsapeDdtIFxzpH1qdAs0MmIVFdUxy3pRA-zMI2ZW-0FR4n_MIEn3K-AxCY3g7QJQ==">

    <title>Заказы</title>
    <link rel="icon" href="/images/favicon.png">

    <link href="/assets/carrier/css/jquery-ui.min.css?v=1499547189" rel="stylesheet">
    <link href="/assets/carrier/css/bootstrap.min.css?v=1550073338" rel="stylesheet">
    <link href="/assets/carrier/css/OpenSans.css?v=1504033189" rel="stylesheet">
    <link href="/assets/carrier/css/font-awesome.min.css?v=1523951020" rel="stylesheet">
    <link href="/assets/carrier/css/simple-line-icons.min.css?v=1499547189" rel="stylesheet">
    <link href="/assets/carrier/css/bootstrap-switch.min.css?v=1499547188" rel="stylesheet">
    <link href="/assets/carrier/css/components.min.css?v=1574369993" rel="stylesheet">
    <link href="/assets/carrier/css/plugins.min.css?v=1574369993" rel="stylesheet">
    <link href="/assets/carrier/css/jquery.datetimepicker.css?v=1504033189" rel="stylesheet">
    <link href="/assets/carrier/css/select2.min.css?v=1499547189" rel="stylesheet">
    <link href="/assets/carrier/css/select2-bootstrap.min.css?v=1499547189" rel="stylesheet">
    <link href="/assets/carrier/css/toastr.min.css?v=1499547188" rel="stylesheet">
    <link href="/assets/carrier/css/layout.min.css?v=1545679834" rel="stylesheet">
    <link href="/assets/carrier/css/darkyellow.min.css?v=1574369993" rel="stylesheet">
    <link href="/assets/carrier/css/custom.min.css?v=1576058831" rel="stylesheet">
    <link href="/assets/carrier/css/order.css?v=1560111327" rel="stylesheet">
    <link href="/assets/carrier/css/bootstrap-datepicker3.min.css?v=1539084889" rel="stylesheet">
    <link href="/assets/carrier/css/datepicker-kv.min.css?v=1539084889" rel="stylesheet">
    <link href="/assets/carrier/css/kv-widgets.min.css?v=1552497294" rel="stylesheet">
    <link href="/assets/carrier/css/field-range-bs3.min.css?v=1539084772" rel="stylesheet">
    <link href="/assets/carrier/css/kv-grid-expand.min.css?v=1553197347" rel="stylesheet">
    <link href="/assets/carrier/css/kv-grid.min.css?v=1553197347" rel="stylesheet">
    <link href="/assets/carrier/css/bootstrap-dialog-bs3.min.css?v=1539072531" rel="stylesheet">
    <link href="/assets/carrier/css/select2.min.css?v=1536307876" rel="stylesheet">
    <link href="/assets/carrier/css/select2-addl.min.css?v=1536307876" rel="stylesheet">
    <link href="/assets/carrier/css/select2-bootstrap.min.css?v=1536307876" rel="stylesheet">
    <link href="/assets/carrier/css/activeform.min.css?v=1550951101" rel="stylesheet">
    <link href="/assets/carrier/css/style.css?v=1583979514" rel="stylesheet">
    <script src="/assets/carrier/js/jquery.min.js?v=1516469217"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
    <script src="/assets/carrier/js/dialog.min.js?v=1539072531"></script>
    <script src="/assets/carrier/js/angular.min.js?v=1499547188"></script>
    <script>window.dadataKey = 'ccb0d98b026309b17b32879e6dbca30bf0756795'
        window.kvDatepicker_1643d6f1 = {"autoclose":true,"format":"dd.mm.yyyy","language":"ru"};

        var kvExpandRow_29b1a174 = {"gridId":"w0","hiddenFromExport":true,"detailUrl":"/carrier/order/detail","onDetailLoaded":"","expandIcon":"<span class=\"glyphicon glyphicon-expand\"></span>","collapseIcon":"<span class=\"glyphicon glyphicon-collapse-down\"></span>","expandTitle":"Развернуть","collapseTitle":"Свернуть","expandAllTitle":"Развернуть всё","collapseAllTitle":"Свернуть всё","rowCssClass":"info","animationDuration":"slow","expandOneOnly":true,"enableRowClick":true,"enableCache":true,"rowClickExcludedTags":["A","BUTTON","INPUT"],"collapseAll":false,"expandAll":false,"extraData":[]};

        var krajeeDialogDefaults_d7a8441c = {"alert":{"type":"type-info","title":"Информация","buttonLabel":"<span class=\"glyphicon glyphicon-ok\"></span> Ok"},"confirm":{"type":"type-warning","title":"Подтверждение","btnOKClass":"btn-warning","btnOKLabel":"<span class=\"glyphicon glyphicon-ok\"></span> Ok","btnCancelLabel":"<span class=\"glyphicon glyphicon-ban-circle\"></span>  Отмена"},"prompt":{"draggable":false,"title":"Информация","buttons":[{"label":"Отмена","icon":"glyphicon glyphicon-ban-circle","cssClass":"btn-default"},{"label":"Ok","icon":"glyphicon glyphicon-ok","cssClass":"btn-primary"}],"closable":false},"dialog":{"draggable":true,"title":"Информация","buttons":[{"label":"Отмена","icon":"glyphicon glyphicon-ban-circle","cssClass":"btn-default"},{"label":"Ok","icon":"glyphicon glyphicon-ok","cssClass":"btn-primary"}]}};
        var krajeeDialog_ead3f524 = {"id":"w3"};
        var krajeeDialog=new KrajeeDialog(true,krajeeDialog_ead3f524,krajeeDialogDefaults_d7a8441c);
        var s2options_6cc131ae = {"themeCss":".select2-container--bootstrap","sizeCss":"","doReset":true,"doToggle":false,"doOrder":false};
        window.select2_9e592884 = {"allowClear":true,"theme":"bootstrap","width":"100%","minimumResultsForSearch":Infinity,"placeholder":"Любой диспетчер","language":"ru"};

        window.select2_bcd4f98a = {"allowClear":true,"theme":"bootstrap","width":"100%","minimumResultsForSearch":Infinity,"placeholder":"Любой тип кузова","language":"ru"};

        window.select2_d02f5596 = {"allowClear":true,"theme":"bootstrap","width":"100%","minimumResultsForSearch":Infinity,"placeholder":"Любая категория ТС","language":"ru"};

        window.select2_3fd52f6f = {"allowClear":true,"theme":"bootstrap","width":"100%","minimumResultsForSearch":Infinity,"placeholder":"Любой статус","language":"ru"};
    </script>        <script type="text/javascript">
        window.language = 'ru';
        window.angApp = angular.module('admin', []);
    </script>
</head>
<body class="page-header-fixed page-fullheight page-sidebar-closed-hide-logo page-content-white "
      ng-app="admin">
<div class="modal custom-modal-simple" role="dialog" tabindex="-1" data-gui-block="alert">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" data-gui-field="title"></h4>
            </div>
            <div class="modal-body">
                <p class="large-text" data-gui-field="message"></p>
                <p class="smal-text" data-gui-field="submessage"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand wMax" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>
<div class="modal custom-modal-simple" role="dialog" tabindex="-1" data-gui-block="docs">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" data-gui-field="title"></h4>
            </div>
            <div class="modal-body text-center">
                <p class="large-text" data-gui-field="message"></p>
                <p class="smal-text" data-gui-field="submessage"></p>
            </div>
            <div class="modal-footer">
                <div class="col-sm-4 col-sm-offset-1" data-gui-field="link"></div>
                <div class="col-sm-4 col-sm-offset-1">
                    <button type="button" class="btn wMax" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="modal-label" style="z-index: 1600">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-label">Подтверждение удаления</h4>
            </div>
            <div class="modal-body" id="delete-modal-body">
                Пожалуйста, подтвердите удаление            </div>
            <form id="w4" class="modal-footer" action="/carrier/order/index" method="post">
                <input type="hidden" name="_csrf" value="XEwVFjRM2OZICbywdwIogFMsapeDdtIFxzpH1qdAs0MmIVFdUxy3pRA-zMI2ZW-0FR4n_MIEn3K-AxCY3g7QJQ=="><button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button><button type="submit" class="btn btn-brand">Да</button></form>        </div>
    </div>
</div>    <div class="modal fade" id="modal-confirm" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirm-modal-title">Подтверждение удаления</h4>
            </div>
            <div class="modal-body" id="confirm-modal-body">
                Пожалуйста, подтвердите удаление            </div>
            <form id="w5" class="modal-footer" action="/carrier/order/index" method="post">
                <input type="hidden" name="_csrf" value="XEwVFjRM2OZICbywdwIogFMsapeDdtIFxzpH1qdAs0MmIVFdUxy3pRA-zMI2ZW-0FR4n_MIEn3K-AxCY3g7QJQ=="><button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button><button type="submit" class="btn btn-brand">Да</button></form>        </div>
    </div>
</div>    <div class="modal fade" id="modal-submit-confirm" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirm-submit-modal-title">Отправка формы</h4>
            </div>
            <div class="modal-body" id="confirm-submit-modal-body">
                Пожалуйста, подтвердите отправку формы!            </div>
            <div class="modal-footer">
                <button type="button" id="confirm-submit-modal-button-no" class="btn btn-default" data-dismiss="modal">Отмена</button><button type="submit" id="confirm-submit-modal-button-yes" class="btn btn-brand">Да</button>            </div>

        </div>
    </div>
</div>    <div class="modal fade" id="modal_error" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Ошибка</h4>
            </div>
            <div class="modal-body">
                <p name="message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>            </div>

        </div>
    </div>
</div>    <div class="modal fade" id="empty_modal" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="empty_modal_title"></h4>
            </div>
            <div class="modal-body" id="empty_message_block">

            </div>
            <div class="clearfix"></div>
            <div class="modal-footer">
                <button class="btn btn-brand" name="save"></button>
                <button class="btn btn-brand" data-dismiss="modal" name="cancel"></button>
            </div>
        </div>
    </div>
</div>        <div class="modal col-xs-12 col-sm-10 col-md-10 col-lg-7" style="margin: auto; margin-top: 5%" role="dialog" tabindex="-1" data-gui-block="no_pallets">
    <div class="">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" data-gui-field="title"></h4>
            </div>
            <div class="modal-body text-left">
                <p style="font-size: 13pt" data-gui-field="message"></p>
                <p data-gui-field="submessage"></p>
            </div>
            <div class="modal-footer">
                <div class="col-sm-4 col-sm-offset-1" data-gui-field="link"></div>
                <div class="col-sm-4 col-sm-offset-1">
                    <button type="button" class="btn wMax" data-dismiss="modal">Отмена</button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="edo_error" tabindex="-1" role="dialog" aria-labelledby="modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Предупреждение</h4>
            </div>
            <div class="modal-body">
                К сожалению, вы не можете совершать юридически значимые действия до заключения
                соглашения об электронном взаимодействии  и электронного подписания
                пользовательского соглашения и договора. Мы готовы ответить на ваши вопросы.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>            </div>

        </div>
    </div>
</div>                <div class="loading hidden" id="loading"><div class="loading-anim"></div></div>

<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/">
                <img src="/images/logo_theme_dark.png" alt="logo" class="logo-default"/> </a>
            <div class="menu-toggler sidebar-toggler">
                <span class="hidden-lg hidden-md"></span>
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
           data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                <li class="dropdown dropdown-user">
                    <a href="javascript:;" class="dropdown-toggle dropdown-toggle-company" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <div class="username username-hide-on-mobile">
                            {{ Auth::user()->getFullname() }}&nbsp;
                            <div class="company_name">Компания ООО "ЛОГИСТИЧЕСКАЯ ГРУППА ДАМУ"</div>
                            <div class="company_type">Перевозчик</div>
                        </div>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="/carrier/user/index">
                                <i class="icon-user"></i> Профиль </a>
                        </li>
                        <li>
                            <a href="/carrier/member/index">
                                <i class="icon-users"></i> Водители </a>
                        </li>
                        <li>
                            <a href="/carrier/car/index">
                                <i class="icon-bar-chart"></i> ТС </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/logout">
                                <i class="icon-logout"></i> Выход </a>
                        </li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>            <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<!-- BEGIN HEADER & CONTENT DIVIDER -->
<div class="clearfix"></div>
<!-- END HEADER & CONTENT DIVIDER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <div class="page-sidebar-wrapper">
        <div class="page-sidebar navbar-collapse collapse">
            @include('carrier.left_menu')
            <div class="col-xs-12 col-md-10 text-center hidden-lg app_block">
                <p><a href="https://itunes.apple.com/ru/app/groozgo/id1398716560?ls=1&amp;mt=8" target="_blank"><img class="app_link_img" src="/images/app_store.png" alt="" title="AppStore" style="width:150px;margin-left:-15px"></a></p>
                <p><a href="https://play.google.com/store/apps/details?id=ru.groozgo" target="_blank"><img class="app_link_img" src="/images/google_play.png" alt="" title="GooglePlay" style="width:150px;margin-left:-15px"></a></p>
                <p class="video_link">
                    <a class="videoManualCarrier" href="https://youtu.be/i0kz_koKl04" target="_blank">Видеоинструкция</a>        </p>
            </div>

            <div class="hidden-xs hidden-sm hidden-md app_block">
                <p><a href="https://itunes.apple.com/ru/app/groozgo/id1398716560?ls=1&amp;mt=8" target="_blank"><img class="app_link_img" src="/images/app_store.png" alt="" title="AppStore" style="width:150px;margin-left:10px"></a></p>
                <p><a href="https://play.google.com/store/apps/details?id=ru.groozgo" target="_blank"><img class="app_link_img" src="/images/google_play.png" alt="" title="GooglePlay" style="width:150px;margin-left:10px"></a></p>
                <p class="video_link">
                    <a class="videoManualCarrier" href="https://youtu.be/i0kz_koKl04" target="_blank">Видеоинструкция</a>        </p>
            </div>

            <div class="clearfix"></div>
            <div style="margin:0px;" class="row text-center">

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            @yield('content')
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<div class="page-footer">
    <div class="page-footer-inner"> &copy; 2017-2020 nadamu.kz</div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>
<script src="/assets/carrier/js/jquery-ui.min.js?v=1499547189"></script>
{{--<script src="https://enterprise.api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=49da227a-feea-40d0-9e33-b877e4ee764b&amp;load=package.route,package.map"></script>--}}
{{--<script src="/user-assets/js/yandex_map_api_counter.js?v=1568235333"></script>--}}
{{--<script src="/assets/d62831ab/yii.js?v=1551337386"></script>--}}
<script src="/assets/carrier/js/bootstrap.js?v=1550073338"></script>
<!--[if lt IE 9]>
<script src="/assets/37ae8fbc/respond.min.js?v=1499547189"></script>
<![endif]-->
<!--[if lt IE 9]>
<script src="/assets/9c324a02/excanvas.min.js?v=1499547189"></script>
<![endif]-->
<!--[if lt IE 7]>
<script src="/assets/4b5dc0e8/icons-lte-ie7.js?v=1499547189"></script>
<![endif]-->
<script src="/assets/carrier/js/js.cookie.min.js?v=1499547189"></script>
<script src="/assets/carrier/js/bootstrap-hover-dropdown.min.js?v=1499547188"></script>
<script src="/assets/carrier/js/jquery.slimscroll.min.js?v=1499547189"></script>
<script src="/assets/carrier/js/jquery.blockui.min.js?v=1499547189"></script>
<script src="/assets/carrier/js/bootstrap-switch.min.js?v=1499547188"></script>
<script src="/assets/carrier/js/app.min.js?v=1574369993"></script>
<script src="/assets/carrier/js/global.js?v=1575483727"></script>
<script src="/assets/carrier/js/jquery.datetimepicker.full.js?v=1504033189"></script>
<script src="/assets/carrier/js/moment.min.js?v=1499547189"></script>
<script src="/assets/carrier/js/select2.full.min.js?v=1499547189"></script>
<script src="/assets/carrier/js/i18n/ru.js?v=1499547189"></script>
<script src="/assets/carrier/js/jquery.inputmask.bundle.min.js?v=1499547189"></script>
<script src="/assets/carrier/js/inputmask/inputmask.numeric.extensions.min.js?v=1499547189"></script>
<script src="/assets/carrier/js/toastr.min.js?v=1499547188"></script>
<script src="/assets/carrier/js/layout.min.js?v=1523951020"></script>
<script src="/assets/carrier/js/demo.min.js?v=1523951020"></script>
<script src="/assets/carrier/js/quick-sidebar.min.js?v=1523951020"></script>
<script src="/assets/carrier/js/ui-modals.min.js?v=1523951020"></script>
<script src="/assets/carrier/js/wired_select.js?v=1523951020"></script>
<script src="/assets/carrier/js/carrier-order-index.js?v=1574369993"></script>
<script src="/assets/carrier/js/sign_app_carrier.js?v=1583979514"></script>
<script src="/assets/carrier/js/bootstrap-datepicker.min.js?v=1539084889"></script>
<script src="/assets/carrier/js/datepicker-kv.min.js?v=1539084889"></script>
<script src="/assets/carrier/js/locales/bootstrap-datepicker.ru.min.js?v=1539084889"></script>
<script src="/assets/carrier/js/kv-widgets.min.js?v=1552497294"></script>
<script src="/assets/carrier/js/field-range.min.js?v=1539084772"></script>
<script src="/assets/carrier/js/kv-grid-expand.min.js?v=1553197347"></script>
<script src="/assets/carrier/js/bootstrap-dialog.min.js?v=1539072531"></script>
<script src="/assets/carrier/js/dialog-yii.min.js?v=1539072531"></script>
<script src="/assets/carrier/js/kv-grid-export.min.js?v=1553197347"></script>
<script src="/assets/carrier/yii.gridView.js?v=1551337386"></script>
<script src="/assets/carrier/js/select2.full.min.js?v=1536307876"></script>
<script src="/assets/carrier/js/select2-krajee.min.js?v=1536307876"></script>
<script src="/assets/carrier/js/i18n/ru.js?v=1536307876"></script>
<script src="/assets/carrier/yii.activeForm.js?v=1551337386"></script>
<script src="/assets/carrier/js/activeform.min.js?v=1550951101"></script>
<script src="/assets/carrier/yii.validation.js?v=1551337386"></script>
<script src="/assets/carrier/js/jquery.pjax.js?v=1507803074"></script>
<script src="/assets/carrier/js/gui.js?v=1527474472"></script>
<script src="/assets/carrier/js/app-run.js?v=1523951021"></script>
<script src="/assets/carrier/js/main_script.js?v=1599728876"></script>
<script src="/assets/carrier/js/news.js?v=1540166614"></script>
<script src="/assets/carrier/js/stats.js?v=1574061564"></script>
<script>$(document).ready(function()
    {
        $('body').on('click','.edo_state_check', function(e){
            e.preventDefault();
            $('#edo_error').modal('show');
        });
    });
</script>
<script>jQuery(function ($) {
        jQuery.fn.kvDatepicker.dates={};
        jQuery&&jQuery.pjax&&(jQuery.pjax.defaults.maxCacheLength=0);
        if (jQuery('#ordersearch-start_date').data('kvDatepicker')) { jQuery('#ordersearch-start_date').kvDatepicker('destroy'); }
        jQuery('#ordersearch-start_date-kvdate').kvDatepicker(kvDatepicker_1643d6f1);

        initDPRemove('ordersearch-start_date', true);
        kvExpandRow(kvExpandRow_29b1a174, 'w0_4e51f266');
        jQuery('#w1').dropdown();
        krajeeYiiConfirm('krajeeDialog');
        var kvGridExp_2aac1ef3={"gridId":"w0","action":"/gridview/export/download","module":"gridview","encoding":"utf-8","bom":1,"target":"_blank","messages":{"allowPopups":"Чтобы скачать файл, отключите блокировку всплывающих окон в вашем браузере.","confirmDownload":"Продолжить?","downloadProgress":"Файл для экспорта генерируется. Пожалуйста, ожидайте...","downloadComplete":"Сейчас начнётся загрузка файла. Пожалуйста, ожидайте..."},"exportConversions":[{"from":"<span class=\"glyphicon glyphicon-ok text-success\"></span>","to":"Вкл"},{"from":"<span class=\"glyphicon glyphicon-remove text-danger\"></span>","to":"Выкл"}],"showConfirmAlert":true};
        var kvGridExp_f9af2e03={"filename":"экспорт-таблицы","showHeader":true,"showPageSummary":true,"showFooter":true};
        var kvGridExp_af2f73a8={"dialogLib":"krajeeDialog","gridOpts":kvGridExp_2aac1ef3,"genOpts":kvGridExp_f9af2e03,"alertMsg":"Будет сгенерирован HTML файл для загрузки.","config":{"cssFile":["https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"]}};
        var kvGridExp_6cba30ed={"dialogLib":"krajeeDialog","gridOpts":kvGridExp_2aac1ef3,"genOpts":kvGridExp_f9af2e03,"alertMsg":"Будет сгенерирован CSV файл для загрузки.","config":{"colDelimiter":",","rowDelimiter":"\r\n"}};
        var kvGridExp_0ca41019={"dialogLib":"krajeeDialog","gridOpts":kvGridExp_2aac1ef3,"genOpts":kvGridExp_f9af2e03,"alertMsg":"Будет сгенерирован текстовый файл для загрузки.","config":{"colDelimiter":"\t","rowDelimiter":"\r\n"}};
        var kvGridExp_d2871733={"dialogLib":"krajeeDialog","gridOpts":kvGridExp_2aac1ef3,"genOpts":kvGridExp_f9af2e03,"alertMsg":"Будет сгенерирован EXCEL файл для загрузки.","config":{"worksheet":"ЭкспортPабочийЛист","cssFile":""}};
        var kvGridExp_8cef65d1={"dialogLib":"krajeeDialog","gridOpts":kvGridExp_2aac1ef3,"genOpts":kvGridExp_f9af2e03,"alertMsg":"Будет сгенерирован PDF файл для загрузки.","config":{"mode":"UTF-8","format":"A4-L","destination":"D","marginTop":20,"marginBottom":20,"cssInline":".kv-wrap{padding:20px}","methods":{"SetHeader":[{"odd":{"L":{"content":"PDF экспорт таблицы Yii2","font-size":8,"color":"#333333"},"C":{"content":"Экспорт таблицы","font-size":16,"color":"#333333"},"R":{"content":"Сгенерировано: Wed, 23-Sep-2020","font-size":8,"color":"#333333"}},"even":{"L":{"content":"PDF экспорт таблицы Yii2","font-size":8,"color":"#333333"},"C":{"content":"Экспорт таблицы","font-size":16,"color":"#333333"},"R":{"content":"Сгенерировано: Wed, 23-Sep-2020","font-size":8,"color":"#333333"}}}],"SetFooter":[{"odd":{"L":{"content":"© Krajee Yii2 Расширения","font-size":8,"font-style":"B","color":"#999999"},"R":{"content":"[ {PAGENO} ]","font-size":10,"font-style":"B","font-family":"serif","color":"#333333"},"line":true},"even":{"L":{"content":"© Krajee Yii2 Расширения","font-size":8,"font-style":"B","color":"#999999"},"R":{"content":"[ {PAGENO} ]","font-size":10,"font-style":"B","font-family":"serif","color":"#333333"},"line":true}}]},"options":{"title":"Экспорт таблицы","subject":"PDF сгенерирован расширением kartik-v/yii2-grid","keywords":"krajee, grid, сетка, export, экспорт, yii2-grid, pdf"},"contentBefore":"","contentAfter":""}};
        var kvGridExp_4fb605c9={"dialogLib":"krajeeDialog","gridOpts":kvGridExp_2aac1ef3,"genOpts":kvGridExp_f9af2e03,"alertMsg":"Будет сгенерирован JSON файл для загрузки.","config":{"colHeads":[],"slugColHeads":false,"jsonReplacer":function(k,v){return typeof(v)==='string'?$.trim(v):v},"indentSpace":4}};
        var kvGridInit_3897a575=function(){
            jQuery('#w0 .export-html').gridexport(kvGridExp_af2f73a8);jQuery('#w0 .export-csv').gridexport(kvGridExp_6cba30ed);jQuery('#w0 .export-txt').gridexport(kvGridExp_0ca41019);jQuery('#w0 .export-xls').gridexport(kvGridExp_d2871733);jQuery('#w0 .export-pdf').gridexport(kvGridExp_8cef65d1);jQuery('#w0 .export-json').gridexport(kvGridExp_4fb605c9);
        };
        kvGridInit_3897a575();
        jQuery('#w0').yiiGridView({"filterUrl":"\/carrier\/order\/index","filterSelector":"#w0-filters input, #w0-filters select","filterOnFocusOut":true});
        if (jQuery('#ordersearch-user_search').data('select2')) { jQuery('#ordersearch-user_search').select2('destroy'); }
        jQuery.when(jQuery('#ordersearch-user_search').select2(select2_9e592884)).done(initS2Loading('ordersearch-user_search','s2options_6cc131ae'));

        if (jQuery('#ordersearch-type_body').data('select2')) { jQuery('#ordersearch-type_body').select2('destroy'); }
        jQuery.when(jQuery('#ordersearch-type_body').select2(select2_bcd4f98a)).done(initS2Loading('ordersearch-type_body','s2options_6cc131ae'));

        if (jQuery('#ordersearch-ts_category').data('select2')) { jQuery('#ordersearch-ts_category').select2('destroy'); }
        jQuery.when(jQuery('#ordersearch-ts_category').select2(select2_d02f5596)).done(initS2Loading('ordersearch-ts_category','s2options_6cc131ae'));

        if (jQuery('#ordersearch-status').data('select2')) { jQuery('#ordersearch-status').select2('destroy'); }
        jQuery.when(jQuery('#ordersearch-status').select2(select2_3fd52f6f)).done(initS2Loading('ordersearch-status','s2options_6cc131ae'));

        var $el=jQuery("#search-filter .kv-hint-special");if($el.length){$el.each(function(){$(this).activeFieldHint()});}
        if (jQuery('#status-m').data('select2')) { jQuery('#status-m').select2('destroy'); }
        jQuery.when(jQuery('#status-m').select2(select2_3fd52f6f)).done(initS2Loading('status-m','s2options_6cc131ae'));

        if (jQuery('#type_body-m').data('select2')) { jQuery('#type_body-m').select2('destroy'); }
        jQuery.when(jQuery('#type_body-m').select2(select2_bcd4f98a)).done(initS2Loading('type_body-m','s2options_6cc131ae'));

        if (jQuery('#ts_category-m').data('select2')) { jQuery('#ts_category-m').select2('destroy'); }
        jQuery.when(jQuery('#ts_category-m').select2(select2_d02f5596)).done(initS2Loading('ts_category-m','s2options_6cc131ae'));

        jQuery('#search-filter').yiiActiveForm([{"id":"ordersearch-status","name":"status","container":".field-status-m","input":"#status-m","validate":function (attribute, value, messages, deferred, $form) {yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Значение «Статус» должно быть целым числом.","skipOnEmpty":1});}},{"id":"ordersearch-type_body","name":"type_body","container":".field-type_body-m","input":"#type_body-m","validate":function (attribute, value, messages, deferred, $form) {yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Значение «Типы кузова ТС» должно быть целым числом.","skipOnEmpty":1});}},{"id":"ordersearch-ts_category","name":"ts_category","container":".field-ts_category-m","input":"#ts_category-m","validate":function (attribute, value, messages, deferred, $form) {yii.validation.number(value, messages, {"pattern":/^\s*[+-]?\d+\s*$/,"message":"Значение «Категория ТС» должно быть целым числом.","skipOnEmpty":1});}},{"id":"ordersearch-from_address","name":"from_address","container":".field-ordersearch-from_address","input":"#ordersearch-from_address","validate":function (attribute, value, messages, deferred, $form) {yii.validation.string(value, messages, {"message":"Значение «Откуда» должно быть строкой.","skipOnEmpty":1});}},{"id":"ordersearch-to_address","name":"to_address","container":".field-ordersearch-to_address","input":"#ordersearch-to_address","validate":function (attribute, value, messages, deferred, $form) {yii.validation.string(value, messages, {"message":"Значение «Куда» должно быть строкой.","skipOnEmpty":1});}}], []);
        jQuery(document).pjax("#pjax-grid a", {"push":true,"replace":false,"timeout":1000,"scrollTo":false,"container":"#pjax-grid"});
        jQuery(document).on("submit", "#pjax-grid form[data-pjax]", function (event) {jQuery.pjax.submit(event, {"push":true,"replace":false,"timeout":1000,"scrollTo":false,"container":"#pjax-grid"});});
        jQuery('#w4').yiiActiveForm([], []);
        jQuery('#w5').yiiActiveForm([], []);
    });
</script>
<script>jQuery(window).on('load', function () {

        (function(d, w, m) {
            window.supportAPIMethod = m;
            var s = d.createElement('script');
            s.type ='text/javascript'; s.id = 'supportScript'; s.charset = 'utf-8';
            s.async = true;
            var id = 'ee4c21a74929debe7d067a98a54c0603';
            s.src = '//lcab.talk-me.ru/support/support.js?h='+id;
            var sc = d.getElementsByTagName('script')[0];
            w[m] = w[m] || function() { (w[m].q = w[m].q || []).push(arguments); };
            if (sc) sc.parentNode.insertBefore(s, sc);
            else d.documentElement.firstChild.appendChild(s);
        })(document, window, 'TalkMe');

        TalkMe('setClientInfo', {
            name: "Гимельфарб Ольга",
            email: "olga-gim@mail.ru",
            phone: "+77017183482",
            custom: {
                organization: 'ООО ЛОГИСТИЧЕСКАЯ ГРУППА ДАМУ',
                inn: "5047225986"
            }
        });

    });
</script>
@stack('scripts')


<script type="text/javascript">
    var isset_news = 0;
</script>

</body>
</html>
