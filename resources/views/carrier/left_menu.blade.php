<ul class="page-sidebar-menu page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200"><li class="sidebar-toggler-wrapper hide"><span class="sidebar-toggler">&nbsp;</span></li>
    <li class="nav-item start {{ Request::is('*orders') ? 'active' : '' }}"><a class="nav-link nav-toggle" href="{{ route('carrier.cabinet') }}"><i class="fa fa-list"></i><span class="title">Заказы</span><span class="badge badge-red block-badge" id="order-count">19</span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle hide" href="#"><i class="fa fa-frown-o"></i><span class="title">Упущено из подписки</span><span class="badge badge-red block-badge" id="order-count">0</span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle" href="#"><i class="fa fa-file-o"></i><span class="title">Электронные документы</span><span class="badge badge-red block-badge">0</span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle" href="#"><i class="fa fa-truck"></i><span class="title">Транспортные средства<br><small><i class='fa fa-exclamation-triangle'></i> Не добавлены</small></span><span class="selected"></span></a></li>
    <li class="nav-item start {{ Request::is('*drivers*') ? 'active' : '' }}">
        <a class="nav-link nav-toggle" href="{{ route('carrier.drivers') }}">
            <i class="fa fa-users"></i>
            <span class="title">Водители
                @if(!Auth::user()->hasDrivers())
                <br>
                <small><i class='fa fa-exclamation-triangle'></i> Не добавлены</small>
                @endif
            </span>
            <span class="selected"></span>
        </a>
    </li>
    <li class="nav-item start"><a class="nav-link nav-toggle settings" href="#"><i class="fa fa-map-marker"></i><span class="title">Отслеживание</span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle" href="#"><i class="fa fa-wechat"></i><span class="title">Подписка на заказы</span><span class="badge badge-red block-badge">1</span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle" href="#"><i class="icon-book-open"></i><span class="title">Наши новости</span><span class="badge badge-red block-badge" id="order-count">4</span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle settings" href="#"><i class="fa fa-gear"></i><span class="title">Настройка уведомлений</span></a></li>
    <li class="nav-item start {{ Request::is('*workers') ? 'active' : '' }}"><a class="nav-link nav-toggle settings" href="{{ route('carrier.workers') }}"><i class="icon-users"></i><span class="title">Пользователи компании</span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle" href="#"><i class="fa fa-user"></i><span class="title">Профиль<br><small><i class='fa fa-exclamation-triangle'></i> Соглашение об ЭДО не прикреплено</small></span><span class="selected"></span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle" href="#"><i class="fa fa-gift"></i><span class="title">Подарки</span><span class="selected"></span></a></li>
    <li class="nav-item start"><a class="nav-link nav-toggle" href="{{ route('logout') }}"><i class="icon-logout"></i><span class="title">Выход</span></a></li>
</ul>
