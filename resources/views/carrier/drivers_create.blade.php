@extends('carrier.carrier')
@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb"><li><a href="{{ route('carrier.drivers') }}">Водители</a><i class="fa fa-circle"></i></li><li>Добавление водителя</li>
        </ul>                </div>
    <h3 class="page-title hidden"> Добавление водителя</h3>
    <br>
    <div class="row">

        <div id="driver-form-wrapper">

            <div class="modal fade modal-scroll ss-modal" id="modal-image-req" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" data-gui-field="title">Требования к сканам</h4>
                        </div>
                        <div class="modal-body">
                            <p>1. Один файл – один документ.</p>
                            <p>2. Формат файла .jpg, .pdf, .png.</p>
                            <p>3. Все углы документа видны на фотографии.</p>
                            <p>4. Документ занимает по площади 50% кадра или больше.</p>
                            <p>5. Картинка четкая, без размытия и бликов.</p>
                            <p>6. На фотографии использовалось достаточно равномерное освещение для распознавания изображенного документа.</p>
                            <p>7. На фотографии отсутствуют или минимизированы перспективные искажения (т.е. страницы документа должны быть как
                                можно перпендикулярнее оптической оси и должны лежать в одной плоскости).</p>
                            <p>8. Размер фотографии необходимо иметь равным 2МП или больше, размер скан-копии - 150 dpi или больше.</p>
                            <p>9. Файлы с фотографиями должны содержать следующую дополнительную информацию об изображении (метаданные):</p>
                            <p>     – модель технического устройства, с помощью которого была получена фотография;</p>
                            <p>     – дата и время фотосъемки;</p>
                            <p> Желательно: гео-координаты (широта и долгота) местоположения технического средства контроля в момент фотосъемки.</p>
                            <p>10. Фотографии не должны быть отредактированы в программах для обработки изображений. Не допускается поворот, обрезание,
                                сжатие, изменение размера изображения и другие изменения с использованием каких-либо технических средств. Не допускается
                                цифровой или аналоговый фотомонтаж, в том числе использование изображения, перефотографированного с другого цифрового
                                или аналогового носителя.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-brand wMax" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="driver-form">
                <form id="driver_form" action="{{ route('carrier.drivers.store') }}" method="post" enctype="multipart/form-data" class="ng-pristine ng-valid">
                    @csrf
                    <input type="hidden" name="carrier_id" value="{{ Auth::id() }}">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="portlet">
                            <div class="portlet-body form">
                                <div class="form-body">
                                    <div class="row">
                                        <h4> Личные данные </h4>

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Фамилия*</label>
                                                <input type="text" class="form-control" name="last_name" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Имя*</label>
                                                <input type="text" class="form-control" name="first_name" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Отчество*</label>
                                                <input type="text" class="form-control" name="patronymic" required>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label for="">Дата рождения*</label>
                                                <input type="text" class="form-control" id="birth_date" name="birth_date" required>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <h4 style="position: relative;">
                                            Номер телефона для отслеживания        <span class="popovers label-popovers" data-placement="top" data-container="body" data-content="При назначении водителя на заказ на этот телефон будут отправлены СМС с инструкциями по отслеживанию местоположения" data-trigger="hover" data-original-title="" title="">?</span>    </h4>
                                        <div class="col-md-3 col-xs-12">
                                            <div class="form-group field-driverphone-0-phone required">

                                                <input type="tel" id="driverphone-0-phone" class="form-control" required name="phone" data-plugin-inputmask="inputmask_e8a63036">

                                                <p class="help-block help-block-error"></p>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <h4> Паспорт </h4>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group field-newdriver-passport_number">
                                                <label class="control-label" for="newdriver-passport_number">Серия/Номер</label>
                                                <input type="text" name="passport_number" id="newdriver-passport_number" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group field-newdriver-passport_date">
                                                <label class="control-label" for="passport_date">Дата выдачи</label>
                                                <div id="passport_date-kvdate" class="input-group">
                                                    <input type="text" name="passport_date" id="passport_date" class="form-control" placeholder="Выберите время...">
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group field-newdriver-passport_data">
                                                <label class="control-label" for="newdriver-passport_data">Кем выдан</label>
                                                <textarea id="newdriver-passport_data" class="form-control" name="passport_data" rows="2"></textarea>
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <h4> Водительское удостоверение </h4>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group field-newdriver-driver_number">
                                                <label class="control-label" for="newdriver-driver_number">Серия/номер</label>
                                                <input type="text" id="newdriver-driver_number" class="form-control" name="driver_number">
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <div class="form-group field-newdriver-driver_date_in">
                                                <label class="control-label" for="driver_date_in">Дата выдачи прав</label>
                                                <div id="newdriver-driver_date_in-kvdate" class="input-group  date">
                                                    <input type="text" id="driver_date_in" class="form-control" name="driver_date_in" placeholder="Выберите время...">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-6 col-xs-12 required">
                                            <a class="image-req-link " title="Требования к сканам" data-toggle="modal" data-target="#modal-image-req">Требования к сканам</a>                                                        <div class="form-group field-newdriver-file_driver">
                                                <label class="control-label" for="newdriver-file_driver">Копия ВУ</label>
                                                <input type="file" name="file_driver" value="">
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <h4> Медицинская книжка </h4>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-sm-12 col-xs-12">

                                            <div class="form-group field-newdriver-med_number">
                                                <label class="control-label" for="newdriver-med_number">Номер</label>
                                                <input type="text" id="newdriver-med_number" class="form-control" name="med_number">
                                            </div>
                                        </div>

                                        <div class="col-md-3 col-sm-12 col-xs-12">

                                            <div class="form-group field-newdriver-file_med">
                                                <label class="control-label" for="newdriver-file_med">Копия мед книжки</label>
                                                <input type="file" name="file_med" value="">
                                            </div>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-md-offset-0 col-sm-4 col-xs-8 col-sm-offset-4 col-xs-offset-2 text-center-sm text-center-xs">
                                    <button type="submit" id="btn-submit-form" class="btn btn-brand submit_new">Добавить</button>
                                    <a class="btn btn-brand-outline btn-big-responsive" href="{{ route('carrier.drivers') }}">Отмена</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@push('scripts')
    <script>
        $(document).ready(function(){
            $('#birth_date,#passport_date,#driver_date_in').datepicker({
                "dateFormat": 'yy-mm-dd'
            });
        });
    </script>
@endpush
