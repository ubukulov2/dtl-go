@extends('carrier.carrier')
@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb"><li>Водители</li>
        </ul>                </div>
    <h3 class="page-title hidden"> Водители</h3>
    <br>
    <div class="row">
        @if(count($drivers) == 0)
            <div class="col-md-12 text-center">
                <h4>У Вас пока не добавлены водители :(</h4>
                <p>
                    Для взятия заказов необходимо добавить водителей.        <br>
                    Далее они должны получить допуск к перевозкам, он выдается автоматически и обычно в течение получаса.    </p>
                <div class="col-md-12">
                    <a class="btn btn-brand" href="{{ route('carrier.drivers.create') }}" title="Добавить первого водителя" data-pjax="0">
                        <i class="fa fa-plus"></i>&nbsp;
                        <span class="text">Добавить первого водителя</span>
                    </a>
                </div>
            </div>
        @else
            <div class="col-md-12">
                <a class="btn btn-brand" href="{{ route('carrier.drivers.create') }}" title="Добавить первого водителя" data-pjax="0">
                    <i class="fa fa-plus"></i>&nbsp;
                    <span class="text">Добавить водителя</span>
                </a>

                <div class="table-scrollable">
                    <div id="carriers_for_user-container" class="table-responsive kv-grid-container">
                        <table class="table table-hover table-light kv-grid-table table-bordered table-striped kv-table-wrap" style="font-szie: .75em">
                            <thead>
                            <th>#</th>
                            <th>Ф.И.О</th>
                            <th>Телефон</th>
                            <th>Дата рождения</th>
                            <th>Вод. удостоверение</th>
                            <th>Редактировать</th>
                            </thead>
                            <tbody>
                            @foreach($drivers as $driver)
                                <tr data-key="17542">
                                    <td>{{ $loop->iteration }}</td>
                                    <td data-col-seq="0">{{ $driver->getFullname() }}</td>
                                    <td data-col-seq="1">{{ $driver->phone }}</td>
                                    <td data-col-seq="2">{{ $driver->birth_date }}</td>
                                    <td data-col-seq="2">
                                        @if(!empty($driver->file_driver))
                                        <a href="{{ $driver->driver_img() }}" target="_blank">ссылка</a>
                                        @endif
                                    </td>
                                    <td style="text-align: center; vertical-align: middle">
                                        <a class="btn btn-brand text-center" href="{{ route('carrier.drivers.edit', ['id' => $driver->id]) }}"><span class="glyphicon glyphicon-pencil"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
@stop
