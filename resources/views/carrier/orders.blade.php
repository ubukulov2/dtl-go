@extends('carrier.carrier')
@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb"><li>Заказы</li>
        </ul>                </div>
    <h3 class="page-title hidden"> Заказы</h3>
    <br>
    <div class="row">


        <div id="canCarrierTakeOrderDiv" class="hidden">
            <p>Для выполнения заказов требуется:</p>

            <ol>
                <li> Дождаться проверки компании СБ</li>
                <li> Добавить почтовый адрес в <a href="/carrier/user/index"><u>профиль</u></a></li>
                <li> Добавить платежные реквизиты в <a href="/carrier/user/index"><u>профиль</u></a></li>
                <li> Добавить транспортные средства в <a href="/carrier/car/index"><u>ЛК</u></a></li>
                <li> Добавить водителей в <a href="/carrier/member/index"><u>ЛК</u></a></li>
                <li> Прикрепить скан соглашения об электр. взаимодействии в <a href="/carrier/user/index"><u>профиль</u></a></li>
            </ol>
        </div>


        <div class="col-md-12 hidden-sm hidden-xs ">
            <a class="btn btn-brand hidden-sm hidden-xs" href="/carrier/order/report"
               title="Выгрузить в Excel" data-pjax="0">
                <i class="fa fa-file-excel-o"></i>&nbsp;
                <span class="text">Выгрузить в Excel</span>
            </a>
        </div>
        <div id="pjax-grid" data-pjax-container="" data-pjax-push-state data-pjax-timeout="1000">    <div class="col-md-12 hidden-sm hidden-xs">
                <form id="order-to-trash" action="/carrier/order/index" method="post">
                    <input type="hidden" name="_csrf" value="XEwVFjRM2OZICbywdwIogFMsapeDdtIFxzpH1qdAs0MmIVFdUxy3pRA-zMI2ZW-0FR4n_MIEn3K-AxCY3g7QJQ==">

                    <div id="w0" class="grid-view is-bs3 hide-resize" data-krajee-grid="kvGridInit_3897a575" data-krajee-ps="ps_w0_container">
                        <div class='row'>
                            <div class='col-sm-12'>
                                <div class='pull-right'>
                                    <a class='btn btn-red hide' data-pjax="0" href='/carrier/missed-order/index'>Вы упустили 0 заказов за последнюю неделю</a>
                                </div>
                                <div class='pull-right'>
                                    <a class='btn btn-red hide' data-pjax="0" href='/carrier/chain/index'></a>
                                </div>
                            </div>
                            <div class='col-sm-8 col-xs-12'>

                            </div>
                            <div class='col-sm-4 col-xs-12'>
                                <div class='pull-right'>
                                    <a class='btn btn-brand btn-reset-filter' data-pjax="1" href='/carrier/order/index'>Сброс фильтров</a>
                                </div>
                            </div>
                        </div>

                        <div class='table-scrollable'><div id="w0-container" class="table-responsive kv-grid-container"><table class="table table-light kv-grid-table table-bordered kv-table-wrap" style="font-szie: .75em"><colgroup><col class="skip-export">
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col>
                                        <col></colgroup>
                                    <thead>

                                    <tr><th class="kv-align-center kv-align-middle skip-export kv-expand-header-cell kv-batch-toggle w0_4e51f266 kv-merged-header" title="Развернуть всё" style="width:50px;" rowspan="2" data-col-seq="0"><div class='kv-expand-header-icon kv-state-collapsed'><span class="glyphicon glyphicon-expand"></span></div></th><th class="text-center" data-col-seq="1"><a class="desc" href="/carrier/order/index?sort=id" data-sort="id">Номер заказа</a></th><th class="text-center" data-col-seq="2">Диспетчер</th><th class="text-center" style="width:15%;" data-col-seq="3"><a class="desc" href="/carrier/order/index?sort=from_date" data-sort="from_date">Дата загрузки</a></th><th class="text-center" data-col-seq="4"><a href="/carrier/order/index?sort=type_body" data-sort="type_body">Типы кузова ТС</a></th><th class="text-center" data-col-seq="5"><a href="/carrier/order/index?sort=ts_category" data-sort="ts_category">Категория ТС</a></th><th class="text-center" data-col-seq="6"><a href="/carrier/order/index?sort=from_address" data-sort="from_address">Откуда</a></th><th class="text-center" data-col-seq="7"><a href="/carrier/order/index?sort=to_address" data-sort="to_address">Куда</a></th><th class="text-center" data-col-seq="8">Стоимость</th><th class="text-center" style="width:100px;" data-col-seq="9">Статус</th></tr><tr id="w0-filters" class="filters skip-export"><td><input type="text" class="form-control" name="OrderSearch[id]"></td><td><div class="kv-plugin-loading loading-ordersearch-user_search">&nbsp;</div><select id="ordersearch-user_search" class="form-control" name="OrderSearch[user_search]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_9e592884" style="display:none">
                                                <option value="">Любой диспетчер</option>
                                                <option value="17542">Гимельфарб Ольга</option>
                                            </select></td><td><div id="ordersearch-start_date-kvdate" class="input-group  input-daterange"><input type="text" id="ordersearch-start_date" class="form-control krajee-datepicker" name="OrderSearch[start_date]" placeholder="С" data-datepicker-source="ordersearch-start_date-kvdate" data-datepicker-type="5" data-krajee-kvDatepicker="kvDatepicker_1643d6f1"><span class="input-group-addon kv-field-seperator">-</span><input type="text" id="ordersearch-end_date" class="form-control" name="OrderSearch[end_date]" placeholder="По"></div></td><td><div class="kv-plugin-loading loading-ordersearch-type_body">&nbsp;</div><select id="ordersearch-type_body" class="form-control" name="OrderSearch[type_body]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_bcd4f98a" style="display:none">
                                                <option value="">Любой тип кузова</option>
                                                <option value="3">Тент</option>
                                                <option value="1">Реф</option>
                                                <option value="4">Борт</option>
                                                <option value="2">Изотерм</option>
                                                <option value="7">Фургон</option>
                                                <option value="9">П/п ТОНАР &gt;26м3</option>
                                            </select></td><td><div class="kv-plugin-loading loading-ordersearch-ts_category">&nbsp;</div><select id="ordersearch-ts_category" class="form-control" name="OrderSearch[ts_category]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_d02f5596" style="display:none">
                                                <option value="">Любая категория ТС</option>
                                                <option value="3">1,5т/9м3/4плт</option>
                                                <option value="5">3т/16м3/6плт</option>
                                                <option value="7">5т/36м3/8плт</option>
                                                <option value="9">10т/54м3/15плт</option>
                                                <option value="11">20т/82м3/33плт</option>
                                            </select></td><td><input type="text" id="ordersearch-from_address" class="form-control" name="OrderSearch[from_address]" placeholder="Пункт загрузки"></td><td><input type="text" id="ordersearch-to_address" class="form-control" name="OrderSearch[to_address]" placeholder="Пункт выгрузки"></td><td><input type="text" id="ordersearch-sum" class="popovers form-control" name="OrderSearch[sum]" placeholder="Стоимость" data-placement="bottom" data-container="body" data-content="Введите нужную сумму или диапазон сумм, разделяя символом &quot;-&quot;" data-trigger="hover"></td><td><div class="kv-plugin-loading loading-ordersearch-status">&nbsp;</div><select id="ordersearch-status" class="form-control" name="OrderSearch[status]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_3fd52f6f" style="display:none">
                                                <option value="">Любой статус</option>
                                                <option value="1">Новый</option>
                                                <option value="2">Подтвержден</option>
                                                <option value="15">Выполняется</option>
                                                <option value="12">Выполнен</option>
                                                <option value="11">Отменен</option>
                                                <option value="13">Отменен администратором</option>
                                            </select></td></tr>

                                    </thead>
                                    <tbody>
                                    <tr class="new-order" data-key="76347">
                                        <td class="w0_4e51f266 skip-export kv-align-center kv-align-middle kv-expand-icon-cell" title="Развернуть" style="width:50px;" data-col-seq="0">        <div class="kv-expand-row ">
                                                <div class="kv-expand-icon kv-state-expanded"><span class="glyphicon glyphicon-expand"></span></div>
                                                <div class="kv-expand-detail skip-export" style="display:none;">
                                                    <div class="skip-export kv-expanded-row w0_4e51f266" data-index="0" data-key="76347"></div>
                                                </div>
                                            </div>
                                        </td>
                                        <td class="text-center" data-col-seq="1">Заказ #76347</td><td class="text-center" data-col-seq="2"></td><td class="text-center" style="width:15%;" data-col-seq="3">27.09.2020<br>09:00 - 16:00</td><td class="text-center" data-col-seq="4">Тентованный<br/></td><td class="text-center" data-col-seq="5">3т/16м3/6плт</td><td data-col-seq="6">Россия, Московская область, Клин, городок Клин-9</td><td data-col-seq="7">Россия, Иркутская область, Усть-Кут, улица Кирова, 136</td><td class="text-center" data-col-seq="8"><nobr><b>62 000&nbsp;₽</b></nobr><br><small>вкл. НДС</small> </td><td class="text-center" style="width:100px;" data-col-seq="9"><i class="fa status-circle fa-hourglass-start"><fa></fa></i>Новый<br><span class="insurance-label">Этот заказ просматривают 3 чел.!</span></td></tr>

                                    <tr class="new-order" data-key="76371"><td class="w0_4e51f266 skip-export kv-align-center kv-align-middle kv-expand-icon-cell" title="Развернуть" style="width:50px;" data-col-seq="0">        <div class="kv-expand-row ">
                                                <div class="kv-expand-icon kv-state-expanded"><span class="glyphicon glyphicon-expand"></span></div>
                                                <div class="kv-expand-detail skip-export" style="display:none;">
                                                    <div class="skip-export kv-expanded-row w0_4e51f266" data-index="18" data-key="76371"></div>
                                                </div>
                                            </div></td><td class="text-center" data-col-seq="1">Заказ #76371</td><td class="text-center" data-col-seq="2"></td><td class="text-center" style="width:15%;" data-col-seq="3">23.09.2020<br>08:00 - 11:00</td><td class="text-center" data-col-seq="4">Рефрижераторный фургон<br/><nobr><i class='fa fa-thermometer-full'></i>&nbsp;-20°...-18°</nobr>&#8451;<br></td><td class="text-center" data-col-seq="5">10т/54м3/15плт</td><td data-col-seq="6">Россия, Москва, улица Академика Скрябина, вл9</td><td data-col-seq="7">Россия, Московская область, Домодедово, микрорайон Северный, Логистическая улица, 1</td><td class="text-center" data-col-seq="8"><nobr><b>8 500&nbsp;₽</b></nobr><br><small>вкл. НДС</small> </td><td class="text-center" style="width:100px;" data-col-seq="9"><i class="fa status-circle fa-hourglass-start"><fa></fa></i>Новый<br><span class="insurance-label">Этот заказ просматривает 1 чел.!</span></td></tr>
                                    </tbody></table></div></div>

                        <div class='row'>
                            <div class='col-sm-12 col-xs-12'>

                            </div>
                        </div>

                        <p><div class="summary">Показаны <b>1-2</b> из <b>2</b> записи.</div></p></div>        </form>
            </div>


            <div class="col-sm-10 col-sm-offset-1 col-xs-12 col-xs-offset-0 visible-xs visible-sm">
                <form id="search-filter" class="row wrapper-order form-vertical" action="/carrier/order/index" method="get" data-pjax="1">         <div class="col-xs-12 text-center">
                        <p><a style='max-width:430px' class='btn btn-red btn-fullwidth hide col-xs-12z' data-pjax='0' href='/carrier/missed-order/index'>Вы упустили 0 заказов за последнюю неделю</a></p>
                    </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group highlight-addon field-status-m">
                            <label class="control-label has-star" for="status-m">Статус</label>
                            <div class="kv-plugin-loading loading-status-m">&nbsp;</div><select id="status-m" class="form-control" name="OrderSearch[status]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_3fd52f6f" style="display:none">
                                <option value="">Любой статус</option>
                                <option value="1">Новый</option>
                                <option value="2">Подтвержден</option>
                                <option value="15">Выполняется</option>
                                <option value="12">Выполнен</option>
                                <option value="11">Отменен</option>
                                <option value="13">Отменен администратором</option>
                            </select>
                            <div class="help-block"></div>
                        </div>         </div>


                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group highlight-addon field-type_body-m">
                            <label class="control-label has-star" for="type_body-m">Типы кузова ТС</label>
                            <div class="kv-plugin-loading loading-type_body-m">&nbsp;</div><select id="type_body-m" class="form-control" name="OrderSearch[type_body]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_bcd4f98a" style="display:none">
                                <option value="">Любой тип кузова</option>
                                <option value="3">Тент</option>
                                <option value="1">Реф</option>
                                <option value="4">Борт</option>
                                <option value="2">Изотерм</option>
                                <option value="7">Фургон</option>
                                <option value="9">П/п ТОНАР &gt;26м3</option>
                            </select>
                            <div class="help-block"></div>
                        </div>        </div>

                    <div class="col-sm-3 col-xs-12">
                        <div class="form-group highlight-addon field-ts_category-m">
                            <label class="control-label has-star" for="ts_category-m">Категория ТС</label>
                            <div class="kv-plugin-loading loading-ts_category-m">&nbsp;</div><select id="ts_category-m" class="form-control" name="OrderSearch[ts_category]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_d02f5596" style="display:none">
                                <option value="">Любая категория ТС</option>
                                <option value="3">1,5т/9м3/4плт</option>
                                <option value="5">3т/16м3/6плт</option>
                                <option value="7">5т/36м3/8плт</option>
                                <option value="9">10т/54м3/15плт</option>
                                <option value="11">20т/82м3/33плт</option>
                            </select>
                            <div class="help-block"></div>
                        </div>         </div>

                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group highlight-addon field-ordersearch-from_address">
                            <label class="control-label has-star" for="ordersearch-from_address">Откуда</label>

                            <input type="text" id="ordersearch-from_address" class="form-control" name="OrderSearch[from_address]">

                            <div class="help-block"></div>

                        </div>         </div>

                    <div class="col-sm-3 col-xs-6">
                        <div class="form-group highlight-addon field-ordersearch-to_address">
                            <label class="control-label has-star" for="ordersearch-to_address">Куда</label>

                            <input type="text" id="ordersearch-to_address" class="form-control" name="OrderSearch[to_address]">

                            <div class="help-block"></div>

                        </div>         </div>

                </form>
                <div id="list-wrapper" class="list-wrapper"><div class="row"><div class="col-sm-12 col-xs-12"><div class="order-list"><div data-key="76347">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76347</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">27.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Тентованный<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">3т/16м3/6плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, Клин, городок Клин-9                                             →                         Россия, Иркутская область, Усть-Кут, улица Кирова, 136                                    </div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">1) 1200х800х750 - 225кг

                                                        2) 1500х650х1150 -435кг -х 3 места

                                                        3)1200х800х1650 -115кг
                                                        Можно сборным грузом</div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>62 000&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, Клин, городок Клин-9</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Иркутская область, Усть-Кут, улица Кирова, 136</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">5239 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76347m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76347m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76347m = null,
                                                                multiRoute_76347m = null;

                                                            $('#btn-toggle-map-76347m').click(function (e) {
                                                                if (myMap_76347m == null) {
                                                                    ymaps.ready(init_76347m);
                                                                }
                                                                else {
                                                                    $('#map-76347m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76347m() {
                                                                $('#map-76347m').removeClass('hidden');
                                                                multiRoute_76347m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [56.3182410,36.8104270],[56.7886230,105.7684050]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76347m = new ymaps.Map('map-76347m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76347m.geoObjects.add(multiRoute_76347m);
                                                                myMap_76347m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        27.09.2020, 09:00 - 16:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">05.10.2020, 09:00 - 15:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Изделия из металла</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                        Верхняя;&nbsp;
                                                        Боковая;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">5</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76347&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76347">Беру за <nobr><b>62 000&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76221">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76221</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">26.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Тентованный<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Свердловская область, Первоуральск, Торговая улица, 1                                             →                         Россия, Московская область, Подольск, Вишнёвая улица, 3                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>48 000&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Свердловская область, Первоуральск, Торговая улица, 1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, Подольск, Вишнёвая улица, 3</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">1806 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76221m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76221m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76221m = null,
                                                                multiRoute_76221m = null;

                                                            $('#btn-toggle-map-76221m').click(function (e) {
                                                                if (myMap_76221m == null) {
                                                                    ymaps.ready(init_76221m);
                                                                }
                                                                else {
                                                                    $('#map-76221m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76221m() {
                                                                $('#map-76221m').removeClass('hidden');
                                                                multiRoute_76221m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [56.8976500,59.9754720],[55.4751530,37.5738510]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76221m = new ymaps.Map('map-76221m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76221m.geoObjects.add(multiRoute_76221m);
                                                                myMap_76221m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        26.09.2020, 10:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">28.09.2020, 10:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Трубы</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Верхняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Насыпью</div>

                                                    <div class="order-title">Доп. оборудование:</div>
                                                    <div class="order-text">Коники, Ремни</div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76221&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76221">Беру за <nobr><b>48 000&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="75810">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #75810</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">26.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;+2°...+4°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">10т/54м3/15плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, городской округ Солнечногорск, деревня Елино, Зеленоградская улица, с1                                             →                         Россия, Москва, Курьяновская набережная, 8                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">Обязательно наличие мед. книжки у водителя </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>8 300&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Солнечногорск, деревня Елино, Зеленоградская улица, с1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Москва, Курьяновская набережная, 8</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">59 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-75810m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-75810m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_75810m = null,
                                                                multiRoute_75810m = null;

                                                            $('#btn-toggle-map-75810m').click(function (e) {
                                                                if (myMap_75810m == null) {
                                                                    ymaps.ready(init_75810m);
                                                                }
                                                                else {
                                                                    $('#map-75810m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_75810m() {
                                                                $('#map-75810m').removeClass('hidden');
                                                                multiRoute_75810m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.9845110,37.2729070],[55.6505510,37.6888810]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_75810m = new ymaps.Map('map-75810m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_75810m.geoObjects.add(multiRoute_75810m);
                                                                myMap_75810m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        26.09.2020, 09:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">26.09.2020, 11:00 - 16:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">тесто</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">15</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=75810&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=75810">Беру за <nobr><b>8 300&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76260">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76260</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">25.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;+4°...+6°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Нижний Новгород, Базовый проезд, 1                                             →                         Россия, Свердловская область, Белоярский городской округ, село Косулино                                    </div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">Возможен возврат товара и/или переезд на иную точку (точки) выгрузки с целью предложить остатки товара другому клиенту. Данные затраты обсуждаются на этапе подтверждения Исполнителем перевозки и/или по факту возникновения необходимости в переезде и/или возврате.
                                                        Наличие медицинской книжки и листа санитарной обработки кузова обязательно. Температура:  +4 град. в постоянном режиме.  Согласно инструкциям на погрузке. ВНИМАНИЕ!!! Водитель обязан присутствовать на погрузке товара и сообщать обо всех нарушениях (явные недостатки качества товара, тары, неверно оформленные документы, отсутствие стикеров и отказ поставщика вешать пломбу) ЗАБЛАГОВРЕМЕННО, до закрытия т/с и держать связь с контактом (Беллой) до момента получения документов, подтверждающих сдачу груза грузополучателю.
                                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>106 800&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Нижний Новгород, Базовый проезд, 1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Свердловская область, Белоярский городской округ, село Косулино</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Возврат возможен</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">1357 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76260m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76260m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76260m = null,
                                                                multiRoute_76260m = null;

                                                            $('#btn-toggle-map-76260m').click(function (e) {
                                                                if (myMap_76260m == null) {
                                                                    ymaps.ready(init_76260m);
                                                                }
                                                                else {
                                                                    $('#map-76260m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76260m() {
                                                                $('#map-76260m').removeClass('hidden');
                                                                multiRoute_76260m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [56.3107220,43.8642680],[56.7362910,60.9625590]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76260m = new ymaps.Map('map-76260m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76260m.geoObjects.add(multiRoute_76260m);
                                                                myMap_76260m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        25.09.2020, 13:00 - 16:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">28.09.2020, 06:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Фрукты</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">33</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76260&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76260">Беру за <nobr><b>106 800&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76407">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76407</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">25.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;-20°...-18°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">10т/54м3/15плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, городской округ Солнечногорск, деревня Елино, Зеленоградская улица, с1                                             →                         Россия, Московская область, Богородский городской округ, поселок Затишье                                    </div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">8000кг</div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>10 700&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Солнечногорск, деревня Елино, Зеленоградская улица, с1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, Богородский городской округ, поселок Затишье</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">99 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76407m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76407m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76407m = null,
                                                                multiRoute_76407m = null;

                                                            $('#btn-toggle-map-76407m').click(function (e) {
                                                                if (myMap_76407m == null) {
                                                                    ymaps.ready(init_76407m);
                                                                }
                                                                else {
                                                                    $('#map-76407m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76407m() {
                                                                $('#map-76407m').removeClass('hidden');
                                                                multiRoute_76407m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.9845110,37.2729070],[55.8420190,38.5671190]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76407m = new ymaps.Map('map-76407m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76407m.geoObjects.add(multiRoute_76407m);
                                                                myMap_76407m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        25.09.2020, 10:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">25.09.2020, 12:00 - 16:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Продукты питания (охлажденка)</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">15</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76407&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76407">Беру за <nobr><b>10 700&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76224">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76224</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">25.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Тентованный<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Свердловская область, Первоуральск, Торговая улица, 1                                             →                         Россия, Москва, Стахановская улица, 19                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>48 000&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Свердловская область, Первоуральск, Торговая улица, 1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Москва, Стахановская улица, 19</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">1776 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76224m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76224m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76224m = null,
                                                                multiRoute_76224m = null;

                                                            $('#btn-toggle-map-76224m').click(function (e) {
                                                                if (myMap_76224m == null) {
                                                                    ymaps.ready(init_76224m);
                                                                }
                                                                else {
                                                                    $('#map-76224m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76224m() {
                                                                $('#map-76224m').removeClass('hidden');
                                                                multiRoute_76224m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [56.8976500,59.9754720],[55.7211890,37.7554010]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76224m = new ymaps.Map('map-76224m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76224m.geoObjects.add(multiRoute_76224m);
                                                                myMap_76224m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        25.09.2020, 10:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">26.09.2020, 15:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">труба</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Насыпью</div>

                                                    <div class="order-title">Доп. оборудование:</div>
                                                    <div class="order-text">Коники</div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76224&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76224">Беру за <nobr><b>48 000&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76005">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76005</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">25.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;-20°...-18°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, Одинцово, Восточная улица, 5                                             →                         Россия, Москва, Курьяновская набережная, 8                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">Обязательно наличие мед. книжки у водителя </div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">Обязательно нужна доверенность на МАРР РУССИЯ</div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>10 600&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, Одинцово, Восточная улица, 5</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Москва, Курьяновская набережная, 8</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">40 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76005m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76005m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76005m = null,
                                                                multiRoute_76005m = null;

                                                            $('#btn-toggle-map-76005m').click(function (e) {
                                                                if (myMap_76005m == null) {
                                                                    ymaps.ready(init_76005m);
                                                                }
                                                                else {
                                                                    $('#map-76005m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76005m() {
                                                                $('#map-76005m').removeClass('hidden');
                                                                multiRoute_76005m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.6725040,37.3037550],[55.6505510,37.6888810]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76005m = new ymaps.Map('map-76005m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76005m.geoObjects.add(multiRoute_76005m);
                                                                myMap_76005m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        25.09.2020, 08:30                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">25.09.2020, 09:30 - 16:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">продукты питания</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">33</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76005&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76005">Беру за <nobr><b>10 600&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76290">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76290</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">25.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;+2°...+4°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, Раменский район, сельское поселение Софьинское, посёлок Раменской Агрохимстанции                                             →                         Россия, Московская область, Богородский городской округ, Ногинск, территория Ногинск-Технопарк, 8                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">Обязательно наличие мед. книжки у водителя </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>10 700&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, Раменский район, сельское поселение Софьинское, посёлок Раменской Агрохимстанции</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, Богородский городской округ, Ногинск, территория Ногинск-Технопарк, 8</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">73 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76290m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76290m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76290m = null,
                                                                multiRoute_76290m = null;

                                                            $('#btn-toggle-map-76290m').click(function (e) {
                                                                if (myMap_76290m == null) {
                                                                    ymaps.ready(init_76290m);
                                                                }
                                                                else {
                                                                    $('#map-76290m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76290m() {
                                                                $('#map-76290m').removeClass('hidden');
                                                                multiRoute_76290m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.5199060,38.0902390],[55.8261190,38.3899440]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76290m = new ymaps.Map('map-76290m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76290m.geoObjects.add(multiRoute_76290m);
                                                                myMap_76290m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        25.09.2020, 01:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">25.09.2020, 08:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">продукты питания</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">33</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76290&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76290">Беру за <nobr><b>10 700&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76287">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76287</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">25.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;+2°...+4°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, Раменский район, сельское поселение Софьинское, посёлок Раменской Агрохимстанции                                             →                         Россия, Московская область, Богородский городской округ, Ногинск, территория Ногинск-Технопарк, 8                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">Обязательно наличие мед. книжки у водителя </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>10 700&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, Раменский район, сельское поселение Софьинское, посёлок Раменской Агрохимстанции</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, Богородский городской округ, Ногинск, территория Ногинск-Технопарк, 8</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">73 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76287m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76287m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76287m = null,
                                                                multiRoute_76287m = null;

                                                            $('#btn-toggle-map-76287m').click(function (e) {
                                                                if (myMap_76287m == null) {
                                                                    ymaps.ready(init_76287m);
                                                                }
                                                                else {
                                                                    $('#map-76287m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76287m() {
                                                                $('#map-76287m').removeClass('hidden');
                                                                multiRoute_76287m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.5199060,38.0902390],[55.8261190,38.3899440]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76287m = new ymaps.Map('map-76287m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76287m.geoObjects.add(multiRoute_76287m);
                                                                myMap_76287m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        25.09.2020, 01:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">25.09.2020, 08:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">продукты питания</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">33</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76287&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76287">Беру за <nobr><b>10 700&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76281">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76281</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">24.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Изотермический фургон<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, городской округ Люберцы, рабочий посёлок Томилино, микрорайон Птицефабрика, к3                                             →                         Россия, Смоленская область, Гагарин, Молодёжная улица, 1                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>21 400&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Люберцы, рабочий посёлок Томилино, микрорайон Птицефабрика, к3</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Смоленская область, Гагарин, Молодёжная улица, 1</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">212 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76281m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76281m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76281m = null,
                                                                multiRoute_76281m = null;

                                                            $('#btn-toggle-map-76281m').click(function (e) {
                                                                if (myMap_76281m == null) {
                                                                    ymaps.ready(init_76281m);
                                                                }
                                                                else {
                                                                    $('#map-76281m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76281m() {
                                                                $('#map-76281m').removeClass('hidden');
                                                                multiRoute_76281m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.6534160,37.9157050],[55.5637780,34.9815470]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76281m = new ymaps.Map('map-76281m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76281m.geoObjects.add(multiRoute_76281m);
                                                                myMap_76281m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        24.09.2020, 15:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">25.09.2020, 09:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">сыворотка сухая</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">32</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76281&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76281">Беру за <nobr><b>21 400&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76278">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76278</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">24.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Изотермический фургон<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, городской округ Люберцы, рабочий посёлок Томилино, микрорайон Птицефабрика, к3                                             →                         Россия, Смоленская область, Гагарин, Молодёжная улица, 1                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>21 400&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Люберцы, рабочий посёлок Томилино, микрорайон Птицефабрика, к3</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Смоленская область, Гагарин, Молодёжная улица, 1</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">212 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76278m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76278m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76278m = null,
                                                                multiRoute_76278m = null;

                                                            $('#btn-toggle-map-76278m').click(function (e) {
                                                                if (myMap_76278m == null) {
                                                                    ymaps.ready(init_76278m);
                                                                }
                                                                else {
                                                                    $('#map-76278m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76278m() {
                                                                $('#map-76278m').removeClass('hidden');
                                                                multiRoute_76278m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.6534160,37.9157050],[55.5637780,34.9815470]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76278m = new ymaps.Map('map-76278m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76278m.geoObjects.add(multiRoute_76278m);
                                                                myMap_76278m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        24.09.2020, 15:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">25.09.2020, 09:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">сыворотка сухая</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">32</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76278&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76278">Беру за <nobr><b>21 400&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76428">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76428</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">24.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;-20°...-18°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        ФРОНЕРИ Буферный склад КПД Карго                                             →                         Россия, Московская область, городской округ Подольск, деревня Валищево, 12                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">погрузка строго только ГР.РФ Мед. книжка Сверять адрес выгрузки с ТСД</div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">РЦ Лента</div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>10 700&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Люберцы, посёлок городского типа Томилино, Новорязанское шоссе, 23-й километр, 27/1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Подольск, деревня Валищево, 12</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">63 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76428m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76428m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76428m = null,
                                                                multiRoute_76428m = null;

                                                            $('#btn-toggle-map-76428m').click(function (e) {
                                                                if (myMap_76428m == null) {
                                                                    ymaps.ready(init_76428m);
                                                                }
                                                                else {
                                                                    $('#map-76428m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76428m() {
                                                                $('#map-76428m').removeClass('hidden');
                                                                multiRoute_76428m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.6467128,37.9187869],[55.3239820,37.6770320]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76428m = new ymaps.Map('map-76428m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76428m.geoObjects.add(multiRoute_76428m);
                                                                myMap_76428m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        24.09.2020, 11:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">24.09.2020, 15:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Мороженое</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">25</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76428&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76428">Беру за <nobr><b>10 700&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76422">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76422</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">24.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;-20°...-18°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, Лобня, улица Лейтенанта Бойко, 104                                             →                         Россия, Нижегородская область, Дзержинск, проспект Свердлова, 65А                                             →                         Россия, Нижний Новгород, Электровозная улица, 7Е                                             →                         Россия, Республика Татарстан, Альметьевск, Объездной тракт, 23/17                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">погрузка строго только ГР.РФ Мед. книжка Сверять адрес выгрузки с ТСД</div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>71 000&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, Лобня, улица Лейтенанта Бойко, 104</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Республика Татарстан, Альметьевск, Объездной тракт, 23/17</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">4</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">1132 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76422m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76422m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76422m = null,
                                                                multiRoute_76422m = null;

                                                            $('#btn-toggle-map-76422m').click(function (e) {
                                                                if (myMap_76422m == null) {
                                                                    ymaps.ready(init_76422m);
                                                                }
                                                                else {
                                                                    $('#map-76422m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76422m() {
                                                                $('#map-76422m').removeClass('hidden');
                                                                multiRoute_76422m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [56.0215380,37.4429130],[56.2354100,43.3841190],[56.2997110,43.8672150],[54.8663890,52.2847880]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76422m = new ymaps.Map('map-76422m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76422m.geoObjects.add(multiRoute_76422m);
                                                                myMap_76422m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        24.09.2020, 10:30                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">27.09.2020, 09:00 - 14:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Продукты питания (заморозка)</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">33</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76422&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76422">Беру за <nobr><b>71 000&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76284">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76284</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">24.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;+2°...+4°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">10т/54м3/15плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, городской округ Солнечногорск, деревня Елино, Зеленоградская улица, с1                                             →                         Россия, Москва, Курьяновская набережная, 8                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">Обязательно наличие мед. книжки у водителя </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>8 400&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Солнечногорск, деревня Елино, Зеленоградская улица, с1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Москва, Курьяновская набережная, 8</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">59 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76284m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76284m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76284m = null,
                                                                multiRoute_76284m = null;

                                                            $('#btn-toggle-map-76284m').click(function (e) {
                                                                if (myMap_76284m == null) {
                                                                    ymaps.ready(init_76284m);
                                                                }
                                                                else {
                                                                    $('#map-76284m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76284m() {
                                                                $('#map-76284m').removeClass('hidden');
                                                                multiRoute_76284m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.9845110,37.2729070],[55.6505510,37.6888810]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76284m = new ymaps.Map('map-76284m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76284m.geoObjects.add(multiRoute_76284m);
                                                                myMap_76284m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        24.09.2020, 09:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">22.09.2020, 14:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">тесто</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">15</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76284&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76284">Беру за <nobr><b>8 400&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="75966">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #75966</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">24.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Изотермический фургон<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Московская область, городской округ Люберцы, рабочий посёлок Томилино, микрорайон Птицефабрика, к3                                             →                         Россия, Московская область, Истра, Московская улица, 48                                    </div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">"Истра Нутриция"</div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>15 000&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Московская область, городской округ Люберцы, рабочий посёлок Томилино, микрорайон Птицефабрика, к3</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, Истра, Московская улица, 48</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">90 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-75966m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-75966m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_75966m = null,
                                                                multiRoute_75966m = null;

                                                            $('#btn-toggle-map-75966m').click(function (e) {
                                                                if (myMap_75966m == null) {
                                                                    ymaps.ready(init_75966m);
                                                                }
                                                                else {
                                                                    $('#map-75966m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_75966m() {
                                                                $('#map-75966m').removeClass('hidden');
                                                                multiRoute_75966m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.6534160,37.9157050],[55.9048560,36.8886340]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_75966m = new ymaps.Map('map-75966m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_75966m.geoObjects.add(multiRoute_75966m);
                                                                myMap_75966m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        24.09.2020, 09:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">24.09.2020, 11:00 - 18:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Сухое молоко</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">33</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=75966&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=75966">Беру за <nobr><b>15 000&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76392">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76392</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">23.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Тентованный<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">10т/54м3/15плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Москва, поселение Марушкинское, деревня Шарапово, 5                                             →                         Россия, Московская область, Богородский городской округ, Ногинск, территория Ногинск-Технопарк, 8                                    </div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">Конечный пункт выгрузки	ООО МЕТРО КЭШ ЭНД КEРРИ 1107

                                                        Общий вес груза	1902.66 KG
                                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>8 200&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Москва, поселение Марушкинское, деревня Шарапово, 5</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, Богородский городской округ, Ногинск, территория Ногинск-Технопарк, 8</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">103 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76392m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76392m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76392m = null,
                                                                multiRoute_76392m = null;

                                                            $('#btn-toggle-map-76392m').click(function (e) {
                                                                if (myMap_76392m == null) {
                                                                    ymaps.ready(init_76392m);
                                                                }
                                                                else {
                                                                    $('#map-76392m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76392m() {
                                                                $('#map-76392m').removeClass('hidden');
                                                                multiRoute_76392m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.5837800,37.1792660],[55.8261190,38.3899440]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76392m = new ymaps.Map('map-76392m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76392m.geoObjects.add(multiRoute_76392m);
                                                                myMap_76392m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        23.09.2020, 17:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">24.09.2020, 01:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">ТНП</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">14</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76392&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76392">Беру за <nobr><b>8 200&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="75948">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #75948</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">23.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;+4°...+6°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Нижний Новгород                                             →                         Россия, Самарская область, Ставропольский район                                    </div>
                                                    <div class="order-title">Дополнительная информация:</div>
                                                    <div class="order-text">Ставка возврата обсуждается до поездки отдельно. Наличие медицинской книжки и листа санитарной обработки кузова обязательно. Температура:  +4 град. в постоянном режиме.  Согласно инструкциям на погрузке. ВНИМАНИЕ!!! Водитель обязан присутствовать на погрузке товара и сообщать обо всех нарушениях (явные недостатки качества товара, тары, неверно оформленные документы, отсутствие стикеров и отказ поставщика вешать пломбу) ЗАБЛАГОВРЕМЕННО, до закрытия т/с и держать связь с контактом (Беллой) до момента получения документов, подтверждающих сдачу груза грузополучателю.
                                                        В случае возврата товара грузополучателем, не подписывать никакие документы до согласования с контактом Заказчика (Беллой).
                                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>36 000&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Нижний Новгород</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Самарская область, Ставропольский район</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Возврат возможен</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">660 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-75948m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-75948m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_75948m = null,
                                                                multiRoute_75948m = null;

                                                            $('#btn-toggle-map-75948m').click(function (e) {
                                                                if (myMap_75948m == null) {
                                                                    ymaps.ready(init_75948m);
                                                                }
                                                                else {
                                                                    $('#map-75948m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_75948m() {
                                                                $('#map-75948m').removeClass('hidden');
                                                                multiRoute_75948m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [56.3268870,44.0059860],[53.2495640,49.4823590]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_75948m = new ymaps.Map('map-75948m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_75948m.geoObjects.add(multiRoute_75948m);
                                                                myMap_75948m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        23.09.2020, 15:00 - 16:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">25.09.2020, 06:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Фрукты</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">32</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=75948&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=75948">Беру за <nobr><b>36 000&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76410">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76410</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">23.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;+4°...+6°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">20т/82м3/33плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        МК "Ялуторовский" АО " Данон Россия"                                             →                         ИП Неклюдов                                    </div>
                                                    <div class="order-title">Общие требования:</div>
                                                    <div class="order-text" style="color:red">В случае неподачи ТС,опоздания ТС под ППР,нарушения темп.усл,отсутствия сан.обр. и мед.кн.,хищения ТМЦ,входа в склад без спец.обуви с защитн. носом,каски и жилета,Перевозчик по требованию обязан возместить суммы штрафов,уплаченные Заказчиком,в теч. 7 р.д.</div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>36 800&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Тюменская область, Ялуторовск, улица Сирина, 1</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Свердловская область, муниципальное образование Город Екатеринбург, посёлок Сысерть, улица Кузнецова, 2Г</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">425 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76410m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76410m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76410m = null,
                                                                multiRoute_76410m = null;

                                                            $('#btn-toggle-map-76410m').click(function (e) {
                                                                if (myMap_76410m == null) {
                                                                    ymaps.ready(init_76410m);
                                                                }
                                                                else {
                                                                    $('#map-76410m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76410m() {
                                                                $('#map-76410m').removeClass('hidden');
                                                                multiRoute_76410m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [56.6647670,66.3180630],[56.6817890,60.5827330]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76410m = new ymaps.Map('map-76410m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76410m.geoObjects.add(multiRoute_76410m);
                                                                myMap_76410m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        23.09.2020, 13:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">24.09.2020, 08:15</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">Молоко сухое</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">33</div>

                                                    <div class="order-title">Доп. оборудование:</div>
                                                    <div class="order-text">Санитарная обработка</div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76410&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76410">Беру за <nobr><b>36 800&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-key="76371">
                                    <div class="order-item new-order">
                                        <div class="row">
                                            <div class="js-info-container">
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">ID</div>
                                                    <div class="order-text">Заказ #76371</div>
                                                    <div class="order-title">Дата загрузки</div>
                                                    <div class="order-text">23.09.2020</div>
                                                    <div class="order-title">Типы кузова</div>
                                                    <div class="order-text">
                                                        Рефрижераторный фургон&nbsp;<small><i class='fa fa-thermometer-full'></i>&nbsp;-20°...-18°</small>&#8451;<br/>                </div>
                                                    <div class="order-title">Категория ТС</div>
                                                    <div class="order-text">10т/54м3/15плт</div>
                                                </div>
                                                <div class="col-sm-6 col-xs-12">
                                                    <div class="order-title">Маршрут</div>
                                                    <div class="order-text">
                                                        Россия, Москва, улица Академика Скрябина, вл9                                             →                         Россия, Московская область, Домодедово, микрорайон Северный, Логистическая улица, 1                                    </div>
                                                    <div class="order-title">Стоимость</div>
                                                    <div class="order-text">
                                                        <nobr><b>8 500&nbsp;₽</b></nobr> (вкл. НДС)                 </div>
                                                    <div class="order-title">Статус</div>
                                                    <div class="order-text status">
                                                        Новый                </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="js-dop-info" style="display: none;">
                                                <div class="col-sm-12 col-xs-12">
                                                    <hr>
                                                    <div class="order-title">Адрес загрузки</div>
                                                    <div class="order-text">Россия, Москва, улица Академика Скрябина, вл9</div>

                                                    <div class="order-title">Адрес выгрузки</div>
                                                    <div class="order-text">Россия, Московская область, Домодедово, микрорайон Северный, Логистическая улица, 1</div>

                                                    <div class="order-title">Возврат на загрузку:</div>
                                                    <div class="order-text">Нет возрата</div>





                                                    <div class="order-title">Всего пунктов маршрута:</div>
                                                    <div class="order-text">2</div>

                                                    <div class="order-title">Длина маршрута:</div>
                                                    <div class="order-text">35 км</div>


                                                    <span class="btn btn-brand btn-fullwidth" id="btn-toggle-map-76371m" style="max-width: 230px;margin-bottom: 15px;">Маршрут на карте</span>


                                                    <div class="col-md-12 text-center">
                                                        <div id="map-76371m" style="width: 100%; height: 300px;" class="hidden"></div>
                                                        <br/>
                                                    </div>



                                                    <script>
                                                        $(function() {
                                                            var myMap_76371m = null,
                                                                multiRoute_76371m = null;

                                                            $('#btn-toggle-map-76371m').click(function (e) {
                                                                if (myMap_76371m == null) {
                                                                    ymaps.ready(init_76371m);
                                                                }
                                                                else {
                                                                    $('#map-76371m').toggleClass('hidden');
                                                                }
                                                            });

                                                            function init_76371m() {
                                                                $('#map-76371m').removeClass('hidden');
                                                                multiRoute_76371m = new ymaps.multiRouter.MultiRoute({
                                                                    // Описание опорных точек мультимаршрута.
                                                                    referencePoints: [
                                                                        [55.7108190,37.7966340],[55.4722350,37.7132520]                    ],
                                                                    // Параметры маршрутизации.
                                                                    params: {
                                                                        // Ограничение на максимальное количество маршрутов, возвращаемое маршрутизатором.
                                                                        results: 1
                                                                    }
                                                                }, {
                                                                    // Автоматически устанавливать границы карты так, чтобы маршрут был виден целиком.
                                                                    boundsAutoApply: true
                                                                });


                                                                myMap_76371m = new ymaps.Map('map-76371m', {
                                                                    center: [
                                                                        55.751387,
                                                                        37.620868
                                                                    ],
                                                                    zoom: 15,
                                                                    controls: ['zoomControl'],
                                                                }, {minZoom: 2});

                                                                myMap_76371m.geoObjects.add(multiRoute_76371m);
                                                                myMap_76371m.behaviors.disable('scrollZoom');

                                                                //счетчик яндекс апи
                                                                if (typeof yandexMapCounterAdd === "function") {
                                                                    yandexMapCounterAdd(BUILD_ROUTE_JS);
                                                                }

                                                            }
                                                        });

                                                    </script>




                                                    <div class="order-title">Время загрузки:</div>
                                                    <div class="order-text">
                                                        23.09.2020, 08:00 - 11:00                </div>
                                                    <div class="order-title">Время выгрузки:</div>
                                                    <div class="order-text">23.09.2020, 15:00</div>

                                                    <div class="order-title">Описание груза:</div>
                                                    <div class="order-text">заморозка</div>

                                                    <div class="order-title">Загрузка:</div>
                                                    <div class="order-text">
                                                        Задняя;&nbsp;
                                                    </div>

                                                    <div class="order-title">Упаковка:</div>
                                                    <div class="order-text">Паллета EUR (0,8 x 1,2)</div>
                                                    <div class="order-title">Кол-во паллет:</div>
                                                    <div class="order-text">15</div>

                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="col-sm-12">
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6 col-xs-12"><button type="button" class="btn  offer-order btn-fullwidth" style="margin-bottom:10px" data-href="/carrier/order/view?id=76371&amp;offer=1">Предложить свою цену</button><button type="button" class="btn take-order btn-fullwidth btn-brand-outline" style="margin-bottom:10px" data-href="/carrier/order/view?id=76371">Беру за <nobr><b>8 500&nbsp;₽</b></nobr></button></div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div> </div></div>
                        <div class="col-sm-12 col-xs-12"><div class="pagination-wrapper"></div>
                            <div class="summary">Показаны записи <b>1-19</b> из <b>19</b>.</div></div></div></div>
            </div>
        </div>


    </div>
@stop
