@extends('carrier.carrier')
@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb"><li>Пользователи компании</li>
        </ul>                </div>
    <h3 class="page-title hidden"> Пользователи компании</h3>
    <br>
    <div class="row">

        <div class="block-margin-bottom">
            <div class="col-sm-6">
                <a class="btn btn-brand-outline edo_state_check" href="/carrier/worker/invite-create" title="Пригласить пользователя" data-pjax="0">
                    <i class="fa fa-plus"></i>&nbsp;
                    <span class="text">Пригласить пользователя</span>
                </a>
                <a class="btn btn-brand-outline" href="/carrier/worker/invites" title="Приглашения" data-pjax="0">
                    <i class="fa fa-file"></i>&nbsp;
                    <span class="text">Приглашения</span>
                </a>
            </div>
            <div class="clearfix"></div>
        </div>

        <div id="p0" data-pjax-container="" data-pjax-push-state="" data-pjax-timeout="1000">
            <div class="col-md-12">
                <div id="carriers_for_user" class="grid-view is-bs3 hide-resize" data-krajee-grid="kvGridInit_fdc1c697" data-krajee-ps="ps_carriers_for_user_container">
                    <div class="row">
                        <div class="col-sm-6 col-xs-12">

                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="pull-right">
                                <a class="btn btn-brand btn-reset-filter" data-pjax="1" href="/carrier/worker">Сброс фильтров</a>
                            </div>
                        </div>
                    </div>

                    <div class="table-scrollable">

                        <div id="carriers_for_user-container" class="table-responsive kv-grid-container"><table class="table table-hover table-light kv-grid-table table-bordered table-striped kv-table-wrap" style="font-szie: .75em"><thead>

                                <tr><th class="text-center" data-col-seq="0" style="width: 23.23%;">Ф.И.О.</th><th class="text-center" data-col-seq="1" style="width: 23.33%;">Телефон</th><th class="text-center" data-col-seq="2" style="width: 23.33%;">Email</th><th class="text-center" data-col-seq="3" style="width: 12.56%;"><a class="asc" href="/carrier/worker/index?sort=-role_id" data-sort="-role_id">Роль</a></th><th class="text-center" data-col-seq="4" style="width: 10.87%;"><a href="/carrier/worker/index?sort=active" data-sort="active">Активность</a></th><th class="action-column" style="width: 6.68%;">&nbsp;</th></tr><tr id="carriers_for_user-filters" class="filters skip-export"><td><input type="text" class="form-control" name="WorkerSearch[name]"></td><td><input type="text" class="form-control" name="WorkerSearch[phone]"></td><td><input type="text" class="form-control" name="WorkerSearch[email]"></td><td><select id="workersearch-role_id" class="form-control select2-hidden-accessible" name="WorkerSearch[role_id]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_7079a009" style="display:none" tabindex="-1" aria-hidden="true">
                                            <option value="">Все</option>
                                            <option value="1">Администратор</option>
                                            <option value="2">Менеджер</option>
                                            <option value="3">Просмотр</option>
                                        </select><span class="select2 select2-container select2-container--bootstrap" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-workersearch-role_id-container"><span class="select2-selection__rendered" id="select2-workersearch-role_id-container"><span class="select2-selection__placeholder">Все</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></td><td><select id="workersearch-active" class="form-control select2-hidden-accessible" name="WorkerSearch[active]" data-s2-options="s2options_6cc131ae" data-krajee-select2="select2_7079a009" style="display:none" tabindex="-1" aria-hidden="true">
                                            <option value="">Все</option>
                                            <option value="-1">Заблокирован</option>
                                            <option value="1">Активирован</option>
                                            <option value="2">Не активирован</option>
                                        </select><span class="select2 select2-container select2-container--bootstrap" dir="ltr" style="width: 100%;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-workersearch-active-container"><span class="select2-selection__rendered" id="select2-workersearch-active-container"><span class="select2-selection__placeholder">Все</span></span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span></td><td>&nbsp;</td></tr>

                                </thead>
                                <tbody>
                                    <tr data-key="17542">
                                        <td data-col-seq="0">{{ Auth::user()->getFullname() }}</td>
                                        <td data-col-seq="1">{{ Auth::user()->phone }}</td>
                                        <td data-col-seq="2">{{ Auth::user()->email }}</td>
                                        <td data-col-seq="3">Администратор</td>
                                        <td data-col-seq="4">Активирован</td>
                                        <td style="text-align: center; vertical-align: middle">
                                            <a class="btn btn-brand text-center edo_state_check" href="/carrier/worker/worker-update?id=17542" data-pjax="0"><span class="glyphicon glyphicon-pencil"></span></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                        </div>
                    </div>

                    <p></p><div class="summary">Показаны <b>1-1</b> из <b>1</b> запись.</div><p></p>
                </div>    </div>
        </div>
    </div>
@stop
