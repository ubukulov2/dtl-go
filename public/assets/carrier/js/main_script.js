"use strict";

jQuery(function(){

    $('.label-popovers').parent().css('position', 'relative');

    jQuery('.popovers[data-timeout]').on('shown.bs.popover', function() {
        var this_popover = jQuery(this);
        setTimeout(function () {
            this_popover.popover('hide');
        }, jQuery(this).data("timeout"));
    });

    bindElement(jQuery(".page-sidebar-wrapper").find(".sub-menu").siblings(".nav-link"), function(items){
        items.off("click").on("click", function(e){
            e.stopPropagation();
            jQuery(this).closest(".nav-item").toggleClass("active");
        });
    });




    jQuery('#modal-confirm').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var link = button.data('link');
        if (button.data('link-method')) {
            modal.find('form').attr('method', button.data('link-method'));
        }
        if (button.data('message')){
            jQuery('#confirm-modal-body').html(button.data('message'));
        }
        if (button.data('title')){
            jQuery('#confirm-modal-title').html(button.data('title'));
        }
        var modal = $(this);
        modal.find('form').attr('action', link);
    });


    jQuery('#modal-submit-confirm').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var form = button.parents('form');
        var button_yes = jQuery('#confirm-submit-modal-button-yes');
        var $this = this;
        if (button.data('message')){
            jQuery('#confirm-submit-modal-body').html(button.data('message'));
        }
        if (button.data('title')){
            jQuery('#confirm-submit-modal-title').html(button.data('title'));
        }
        if (button.data('button_yes')){
            button_yes.html(button.data('button_yes'));
            button_yes.attr('data-form-id', form.attr('id'));
        }
        if (button.data('button_no')){
            jQuery('#confirm-submit-modal-button-no').html(button.data('button_no'));
        }
    });

    $("#confirm-submit-modal-button-yes").on('click', function(event){
        $('#'+ $(this).attr('data-form-id')).submit();
        $('#modal-submit-confirm').modal('hide');
    });


    jQuery('#modal-delete').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var link = button.data('link');
        if (button.data('message')){
            jQuery('#delete-modal-body').html(button.data('message'));
        }
        // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);
        modal.find('form').attr('action', link)
    });
    jQuery("*").on("pjax:end", function() {
        setAllElement();
    });
    jQuery('#modal-info').on('loaded.bs.modal', function (event) {
        setAllElement();
    });
    jQuery('#modal-info').on('hidden.bs.modal', function (event) {
        jQuery(event.target).removeData("bs.modal").find(".modal-content").empty();
        if(jQuery("#pjax-grid").length>0)
        {
            jQuery.pjax.reload({container:"#pjax-grid"});
        }

    });

    $("body").on('click', "[data-type='empty-value']", function (event)
    {

        var checkbox = $(this).find("input[type=checkbox]");
        var target = $(this).data().target;

        if(jQuery(checkbox).is(":checked"))
        {
            $(target).val('').attr('disabled','disabled');
        }
        else
        {
            $(target).val('').removeAttr('disabled');
        }
    });

    setAllElement();

    $('body').on('click', '.js-addres-item', function(e){
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var $item = $this.parent('div');

        $item.toggleClass('open').find('.hidden-element-item').stop(true, false).slideToggle();
    });

    $('body').on('click', '.js-info-driver', function(e){
        e.preventDefault();
        e.stopPropagation();
        var $this = $(this);
        var $item = $this.parent('div');

        $item.toggleClass('open').find('.dop-info').stop(true, false).slideToggle();
    });

    /*$('.js-btn-map-detail').on('click', function(e){
        e.preventDefault();
        if($('.js-map-container #yandex-map').length === 0){
            $('#yandex-map').appendTo('.js-map-container');
        }
        $('.js-map-container').slideToggle();
    });*/

    $('body').on('click', '.js-info-container', function(e){
        e.preventDefault();
        var $parent = $(this).parent('.row');
        $parent.find('.js-dop-info').slideToggle();
    });

    $('.responsive-toggler').on('click', function(){
        $(this).toggleClass('open');
        $('.page-sidebar-wrapper').toggleClass('open');
        $('.page-sidebar-wrapper').parent().toggleClass('open-sidebar');
    });
});

function bindElement(elements, callback){
    if (jQuery(elements).length)
        if (typeof callback == "function")
            callback(jQuery(elements));
}

function goSystemMessage(sm, isChat) {
    if (!isChat) isChat = false;
    if (sm.items) {
        if (window.longPool.sound) {
            var audio = new Audio(); // РЎРѕР·РґР°С‘Рј РЅРѕРІС‹Р№ СЌР»РµРјРµРЅС‚ Audio
            audio.src = window.longPool.sound; // РЈРєР°Р·С‹РІР°РµРј РїСѓС‚СЊ Рє Р·РІСѓРєСѓ "РєР»РёРєР°"
            audio.autoplay = true; // РђРІС‚РѕРјР°С‚РёС‡РµСЃРєРё Р·Р°РїСѓСЃРєР°РµРј
        }
        var issetReload = false;
        sm.items.forEach(function (item, index) {
            if (item.type != "chat") issetReload = true;
            if (item.type != "chat" || !isChat) {
                wakeToastr(item.title, item.text, false, item.type == "chat", item.link);
            }
        });
        if (issetReload) {
            // bindElement("#section-system-message", function (element) {
            //     jQuery.pjax.reload({container: '#pjax'});
            // });
        }
    }
}

function wakeToastr(title, text, sound, isChatType, link){
    if (!sound) sound = false;
    if (!isChatType) isChatType = false;
    if (!link) link = null;
    if (typeof toastr !== 'undefined'){
        if (sound){
            if (window.longPool.sound) {
                var audio = new Audio(); // РЎРѕР·РґР°С‘Рј РЅРѕРІС‹Р№ СЌР»РµРјРµРЅС‚ Audio
                audio.src = window.longPool.sound; // РЈРєР°Р·С‹РІР°РµРј РїСѓС‚СЊ Рє Р·РІСѓРєСѓ "РєР»РёРєР°"
                audio.autoplay = true; // РђРІС‚РѕРјР°С‚РёС‡РµСЃРєРё Р·Р°РїСѓСЃРєР°РµРј
            }
        }
        var params = {
            'closeButton': true,
            'debug': false,
            'positionClass': 'toast-bottom-right',
            'onclick': null,
            'showDuration': '1000',
            'hideDuration': '1000',
            'timeOut': '5000',
            'extendedTimeOut': '1000',

            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut'
        };
        if (link){
            params['onclick'] = function(){
                window.location.href = link;
            };
        }
        if (isChatType) {
            toastr.info(text, title, params);
        }else{
            toastr.success(text, title, params);
        }
    }
}

var bikAutocompleteFlag = false;

function setAllElement(){
    if (typeof jQuery == "function"){

        bindElement("#order-to-trash", function (element) {
            jQuery(element).change(function (e) {
                e.preventDefault();
                setTimeout(function() {
                    if (element.find("tbody").find(":checked").length)
                        jQuery("[data-action='trash-button-view'], [data-action='link-button-view']").removeClass("hidden");
                    else
                        jQuery("[data-action='trash-button-view'], [data-action='link-button-view']").addClass("hidden");
                }, 100);
            });
        });

        bindElement("#filter-button", function(elements){
            elements.on("click", function(e){
                jQuery("#filter-panel").toggleClass("hidden");
                jQuery(this)
                    .toggleClass("btn-primary")
                    .toggleClass("btn-default");
            });
        });

        bindElement("input[type='tel']", function (elements) {
            for (var i=0; i<elements.length; i++){
                jQuery(elements[i])
                    .inputmask({"mask": "+7 (999) 999-9999"});
            }
        });

        bindElement("input[data-type='date'], [data-provide='datepicker']", function (elements) {
            for (var i=0; i<elements.length; i++){
                jQuery(elements[i])
                    .inputmask({"mask": "99.99.9999"});
            }
        });

        bindElement("input[data-type='inn']", function (elements) {
            for (var i=0; i<elements.length; i++){
                jQuery(elements[i])
                    .inputmask({"mask": "9{10,12}"})
                    .on("change", function (e) {
                        if (!this.value){
                            bindElement(jQuery("[data-type='inn-organization-input']").not("input"), function (elements) {
                                elements.addClass("hidden");
                            });
                            bindElement(jQuery("[data-type='inn-director-input']").not("input"), function (elements) {
                                elements.addClass("hidden");
                            });
                        }
                    });
            }
        });

        bindElement("input[data-type='organization']", function (elements) {
            for (var i=0; i<elements.length; i++){
                if (window.dadataKey) {
                    jQuery(elements[i])
                        .suggestions({
                            params: {
                                status:  ["ACTIVE"]
                            },
                            serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                            token: window.dadataKey,
                            type: "PARTY",
                            count: 5,
                            /* Р’С‹Р·С‹РІР°РµС‚СЃСЏ, РєРѕРіРґР° РїРѕР»СЊР·РѕРІР°С‚РµР»СЊ РІС‹Р±РёСЂР°РµС‚ РѕРґРЅСѓ РёР· РїРѕРґСЃРєР°Р·РѕРє */
                            onSelect: function (suggestion) {
                                this.value = suggestion.data.name.short_with_opf;

                                bindElement(jQuery("input[data-type='inn']"), function (elements) {
                                    elements.val(suggestion.data.inn);
                                });

                                bindElement(jQuery("input[data-type='address']"), function (elements) {
                                    if (suggestion.data && suggestion.data.address && suggestion.data.address.value)
                                        elements.val(suggestion.data.address.value);
                                    else
                                        elements.val('');
                                });
                                bindElement(jQuery("input[data-type='kpp']"), function (elements) {
                                    if (suggestion.data && suggestion.data.kpp)
                                        elements.val(suggestion.data.kpp);
                                    else
                                        elements.val('');
                                });
                                bindElement(jQuery("input[data-type='ogrn']"), function (elements) {
                                    if (suggestion.data && suggestion.data.ogrn)
                                        elements.val(suggestion.data.ogrn);
                                    else
                                        elements.val('');
                                });
                                bindElement(jQuery("input[data-type='opf']"), function (elements) {
                                    if (suggestion.data && suggestion.data.opf && suggestion.data.opf.short)
                                        elements.val(suggestion.data.opf.short);
                                    else
                                        elements.val('');
                                });
                                bindElement(jQuery("input[data-type='director']"), function (elements) {
                                    if (suggestion.data && suggestion.data.management && suggestion.data.management.name)
                                        elements.val(suggestion.data.management.name);
                                    else
                                        elements.val('');
                                });
                            }
                        });
                }
            }
        });

        bindElement("input[data-type='bik']", function (elements) {
            for (var i=0; i<elements.length; i++){
                // jQuery(elements[i])
                //     .inputmask({"mask": "9{9}"});
                if (window.dadataKey) {
                    jQuery(elements[i])
                        .suggestions({
                            serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
                            token: window.dadataKey,
                            type: "BANK",
                            count: 5,
                            /* Р’С‹Р·С‹РІР°РµС‚СЃСЏ, РєРѕРіРґР° РїРѕР»СЊР·РѕРІР°С‚РµР»СЊ РІС‹Р±РёСЂР°РµС‚ РѕРґРЅСѓ РёР· РїРѕРґСЃРєР°Р·РѕРє */
                            onSelect: function (suggestion) {
                                bikAutocompleteFlag = true;
                                this.value = suggestion.data.bic;
                                bindElement(jQuery("input[data-type='bank_title']"), function (elements) {
                                    elements.val(suggestion.data.name.payment);
                                });
                                bindElement(jQuery("input[data-type='corr_schet']"), function (elements) {
                                    elements.val(suggestion.data.correspondent_account);
                                });
                            }
                        });
                }
            }
        });

        bindElement("input[data-type='numeric'], input[data-type='decimal'], input[data-type='currency']", function (elements) {
            for (var i=0; i<elements.length; i++){
                var type = jQuery(elements[i]).data("type");
                var alloweMinus = jQuery(elements[i]).data("allow-minus") && jQuery(elements[i]).data("allow-minus") == true ? true : false;
                var min = jQuery(elements[i]).data("min") ? jQuery(elements[i]).data("min") : 0;
                var max = jQuery(elements[i]).data("max") ? jQuery(elements[i]).data("max") : 1000000000000000000000000;
                jQuery(elements[i])
                    .inputmask({
                        "groupSize": false,
                        "alias": type,
                        "rightAlign": false,
                        "alloweMinus": alloweMinus,
                        "prefix": "",
                        "min": min,
                        "max": max
                    });
            }
        });

        bindElement("input[data-type='schet']", function (elements) {
            for (var i=0; i<elements.length; i++){
                var type = jQuery(elements[i]).data("type");
                jQuery(elements[i]).inputmask('Regex', { regex: "[0-9]{20}" });
            }
        });

        bindElement("input[data-type='number']", function (elements) {
            for (var i=0; i<elements.length; i++){
                var type = "numeric";
                var alloweMinus = jQuery(elements[i]).data("allow-minus") && jQuery(elements[i]).data("allow-minus") == true ? true : false;
                var min = jQuery(elements[i]).data("min") ? jQuery(elements[i]).data("min") : 0;
                jQuery(elements[i])
                    .inputmask({
                        "groupSize": false,
                        "alias": type,
                        "rightAlign": false,
                        "alloweMinus": alloweMinus,
                        "prefix": "",
                        "min": min,
                    });
            }
        });

    }
}

