/**
 * Created by jonni on 26.06.19.
 */
var signAppDialog;
$(document).ready(function () {
    signAppDialog = new SignAppDialog();
});

var SignAppDialog = function () {
    this.block = $('#edo_sign_modal');
    this.order_id = null;
    this.is_new = null;
    this.parent_form = null;
    this.code_input = $('input[name="code"]', this.block);
    this.message_block = $('div.edo_message', this.block);
    this.danger_block = $('div[name="danger_block"]', this.block);
    this.error_modal = $('#modal_error');
    this.offer_confirm = $('#modal_edo_confirm');
    this.error_modal_message = $('p[name="message"]', this.error_modal);
    this.int = null;
    this.save_button = $('[name="save"]', this.block);
    this.cancel_button = $('[name="cancel"]', this.block);
    this.reload_button = $('[name="reload"]', this.block);
    this.time_block = $('span[name="time_block"]');
    this.cancel_button.on('click', $.proxy(this.cancel, this));
    this.reload_button.on('click', $.proxy(this.reload_code, this));
    this.save_button.on('click', $.proxy(this.send_code, this));
    $('[name="confirm"]', this.offer_confirm).on('click', $.proxy(this.offer_sign_ready, this));


};

SignAppDialog.prototype = {
    constructor: SignAppDialog,

    offer_show: function (event) {
        var button = event.currentTarget;
        this.parent_form = $(button).parents('form');
        this.order_id = $(button).attr('data_id');
        this.max_offer = parseInt($(button).attr('max_offer'));
        let self = this;

        this.check_form(self).then(function(result){
            if (result)
                self.offer_confirm.modal('show');
        });
    },

    check_offer: function () {
        var form_data = this.parent_form.serializeArray(), input_data;
        for (var i in form_data) {
            input_data = form_data[i];
            if (input_data.name == 'OrderOfferPrice[price]') {
                if (parseInt(input_data.value) >= this.max_offer)
                    return false;
            } else if (input_data.name == 'OrderOfferPrice[start_date]' || input_data.name == 'OrderOfferPrice[start_time]') {
                if (input_data.value != '')
                    return false;
            }
        }
        return true;
    },

    offer_sign_ready:function () {
        var $this = this;
        this.offer_confirm.modal('hide');
        if (this.check_offer()) {
            $.ajax({
                url: "sign-ready",
                data: {
                    id: this.order_id
                },
                method: "POST",
                error: function (err) {
                    console.log(err);
                },
                success: function (resp) {
                    $this.modal_show(resp);
                }
            });
        } else {
            this.parent_form.submit();
        }
    },

    show: function (event) {
        var button = event.currentTarget;
        this.parent_form = $(button).parents('form');
        this.order_id = $(button).attr('data_id');
        this.is_new = $(button).attr('data_new');
        let self = this;
        this.check_form(self).then(function(result){
            if (result)
                $.ajax({
                    url: "sign-ready",
                    data: {
                        id: self.order_id
                    },
                    method: "POST",
                    error: function (err) {
                        console.log(err);
                    },
                    success: function (resp) {
                        self.modal_show(resp);
                    }
                });
        });
    },

    modal_show: function (resp) {
        if (resp.is_ready != undefined && resp.is_ready == 1) {
            if (resp.message != undefined) {
                this.message_block.html(resp.message);
            }
            this.code_input.val('');
            this.danger_block.html('');
            this.save_button.removeClass('hide');
            this.reload_button.addClass('hide');
            this.timer();
            this.block.modal('show');
        }
    },

    timer: function () {
        var time = 60;
        this.time_block.parent('p').removeClass('hide');
        this.time_block.html(time);
        this.time_block.parent('p').removeClass('hide');
        if (this.int != null)
            clearInterval(this.int);
        var $this = this;
        this.int = setInterval(function () {
            $this.time_block.html(time);
            if (time == 0) {
                clearInterval($this.int);
                $this.time_block.parent('p').addClass('hide');
                $this.reload_button.removeClass('hide');
            }
            time = time - 1;
        }, 1000);
    },

    send_code: function () {
        var $this = this;
        var code = this.code_input.val();
        if (code.trim() == '') {
            this.danger_block.html('РџРѕР»Рµ РєРѕРґ РЅРµ РјРѕР¶РµС‚ Р±С‹С‚СЊ РїСѓСЃС‚С‹Рј!');
            return false;
        } else {
            this.save_button.attr('disabled', true);
            pre_loader('show');
            $.ajax({
                url: "sign-check",
                data: {
                    id: this.order_id,
                    code: code,
                    is_new: this.is_new,
                    start: timeOpen
                },
                method: "POST",
                error: function (err) {
                    pre_loader('hide');
                    console.log(err);
                    $this.save_button.removeAttr('disabled');
                },
                success: function (resp) {
                    if (resp.is_valid != undefined) {
                        if (resp.is_valid == 1) {
                            $this.block.modal('hide');
                            $('input[name*="[code]"]', $this.parent_form).val(code);
                            $this.parent_form.submit();
                        }
                        if (resp.is_valid == 0 && resp.error_num != undefined) {
                            pre_loader('hide');
                            $this.show_error(resp.error_num);
                        }
                        $this.save_button.removeAttr('disabled');
                    }
                }
            });
        }
    },

    show_error: function (error_num) {
        var message;
        switch (error_num) {
            case 1:
                message = 'РќРµРїСЂР°РІРёР»СЊРЅС‹Р№ РєРѕРґ!';
                this.danger_block.html(message);
                break;
            case 2:
                message = 'Рљ СЃРѕР¶Р°Р»РµРЅРёСЋ, РґСЂСѓРіРѕР№ РїРµСЂРµРІРѕР·С‡РёРє Р±С‹СЃС‚СЂРµРµ РїРѕРґРїРёСЃР°Р» Р·Р°СЏРІРєСѓ РїРѕ Р·Р°РєР°Р·Сѓ ' + this.order_id;
                this.error_modal_show(message);
                break;
            case 3:
                message = 'Р—Р°РєР°Р· РёР·РјРµРЅРёР»СЃСЏ. РџРѕРїСЂРѕР±СѓР№С‚Рµ Р·Р°РЅРѕРІРѕ РїРѕРґС‚РІРµСЂРґРёС‚СЊ Р·Р°РєР°Р· ' + this.order_id;
                this.error_modal_show(message);
                break;
            case 4:
                message = 'Р—Р°РєР°Р· ' + this.order_id + ' РЅРµ РЅР°Р№РґРµРЅ';
                console.log(message);
                break;
            case 5:
                message = 'РќРµ РїРµСЂРµРґР°РЅ РёРґРµРЅС‚РёС„РёРєР°С‚РѕСЂ Р·Р°РєР°Р·Р°';
                console.log(message);
                break;
        }
    },

    error_modal_show: function(message) {
        this.block.modal('hide');
        this.error_modal_message.html(message);
        this.error_modal.modal('show');
        setTimeout(function () {
            window.location.href= '/carrier/order/index';
        }, 5000);
    },

    reload_code: function () {
        var $this = this;
        $.ajax({
            url: "sign-ready",
            data: {
                id: this.order_id
            },
            method: "POST",
            error: function (err) {
                console.log(err);
            },
            success: function (resp) {
                $this.do_reload(resp);
            }
        });

    },

    do_reload: function (resp) {
        if (resp.is_ready != undefined && resp.is_ready == 1) {
            if (resp.message != undefined) {
                this.message_block.html(resp.message);
            }
            this.code_input.val('');
            this.danger_block.html('');
            this.save_button.removeClass('hide');
            this.reload_button.addClass('hide');
            this.timer();
        }
    },

    cancel: function () {
        this.block.modal('hide');
        if (this.int != null)
            clearInterval(this.int);
    },

    check_form: function(self){
        return new Promise((resolve, reject)=>{
            self.parent_form.on('afterValidate', function(e){
                let has_errors = $(this).find('.has-error').length > 0;
                return resolve(!has_errors);
            });
            yiiActiveFormValidate(self.parent_form);
        });
    },

};

function yiiActiveFormValidate(form) {
    if (typeof form.data('yiiActiveForm') === 'undefined') {
        return;
    }
    var isOnlyValidate = false;
    form
        .one('beforeValidate', function () {
            isOnlyValidate = true;
        })
        .one('afterValidate', function (e, messages, errorAttributes) {
            if (errorAttributes.length) {
                isOnlyValidate = false;
            } else {
                form.one('beforeSubmit', function () {
                    if (isOnlyValidate) {
                        isOnlyValidate = false;
                        return false;
                    }
                })
            }
        })
        .yiiActiveForm('validate', true);
}
