var types;

/*
 $.ajax({
 type: "GET",
 url: '/car/body',
 success: function(data) {
 types = data;
 }
 });
 */


$( document ).ready(function() {
    $("body").on('change', "#newcar-brand", function (event)
    {

        var selected = $('#newcar-brand option:selected').val();

        $.ajax({
            type: "POST",
            url: '/car/select',
            data: { val: selected },
            success: function(data) {
                $('#newcar-auto').empty();
                $('#newcar-auto').append( $('<option value=" " selected></option>') );
                for(var key in data){
                    $('#newcar-auto').append( $('<option value="' + key + '">' + data[key] +'</option>') );
                    //console.log(key + ': ' + data[key]);
                }
                $('#newcar-auto').change();
            }
        });
    });

    $("body").on('change', "#car-brand", function (event)
    {
        var selected = $('#car-brand option:selected').val();

        $.ajax({
            type: "POST",
            url: '/car/select',
            data: { val: selected },
            success: function(data) {
                $('#car-auto').empty();
                $('#car-auto').append( $('<option value=" " selected></option>') );
                for(var key in data){
                    $('#car-auto').append( $('<option value="' + key + '">' + data[key] +'</option>') );
                    //console.log(key + ': ' + data[key]);
                }
                $('#car-auto').change();
            }
        });
    });


    $("body").on('change', "#car-type_body", function (event)
    {

        var current = $('#car-type_body option:selected').data('src');

        bindElement("[data-type-body-src=1]", function (sanEl) {
            if(current !== 0){
                sanEl.removeClass('hidden');
            }else{
                sanEl.addClass('hidden');
            }
        });

        if(current !== 0){
            $(".if_car_has_body").removeClass("hidden");
        }
        if(current === 0){
            $(".if_car_has_body").addClass("hidden");
        }
    });


});
