function sendGoalAnalytic(goal_name){
    if (ym)
    {
        ym(34782355, 'reachGoal', goal_name);
    }
    //console.log(goal_name);
    return true;
}

function sendUserParams(params){
    if (ym)
    {
        ym(34782355, 'userParams', params);
        ym(34782355, 'params', {'params':params});
    }
    //console.log('userParams');
    console.log(params);
    return true;
}

function setUserId(user_id, data = false){
    if (ym)
    {
        ym(34782355, 'setUserID', "" + user_id);
        if (data)
        {
            let params = {
                UserID :  data.user_id ? data.user_id : '',
                companyId :  data.company_id ? data.company_id : '',
                companyType :  data.company_type ? data.company_type : '',
                companyLabel :  data.company_label ? data.company_label : '',
            };
            sendUserParams(params);
        }
    }
    //console.log('setUserId');
    //console.log(user_id);
    return true;
}
