$(document).ready(function () {
    $('#pjax-grid').on('change','#search-filter input, #search-filter select',function(){
        $('#search-filter').submit();
    });

    $('td .mt-checkbox').click(function(e){
        e.stopPropagation();
    });

    var grid = $('#w0');
    var line = false;
    grid.on('kvexprow:beforeLoad', function (event, ind, key, extra) {
        if (line)
        {
            App.unblockUI(grid);
            line = false;
        }
        line = grid.find('table>tbody>tr:eq('+ind+')');

        App.blockUI({target: grid, overlayColor: "rgba(0,0,0,0.5)", animate:!0})
    });

    grid.on('kvexprow:loaded', function (event, ind, key, extra) {
        App.unblockUI(grid);
        line = false;
    });

    $(document).on( 'click', '.offer-order, .take-order', function(e){
        e.preventDefault();

        if(gtag){
            if($(this).hasClass('take-order'))
                gtag("event" , "event_name", { "event_category": "goods", "event_action": "take_click", "event_label": "step2"});
            if($(this).hasClass('offer-order'))
                gtag("event" , "event_name", { "event_category": "goods", "event_action": "offer_click"});
        }

        let take = $(this).data('take');
        if (take !== undefined && take !== false) {
            document.location.href = $(this).attr('data-href');
        } else {
            let popupHtml = $('#canCarrierTakeOrderDiv').html();
            GUI.alert('РџСЂРѕРІРµСЂРєР°', popupHtml,'');
        }
    });

    bindElement("#order-to-trash", function (element) {
        $(element).change(function (e) {
            e.preventDefault();
            setTimeout(function() {
                let trashButtonView =  $("[data-action='trash-button-view']");
                if (element.find("tbody").find(".finished-order").find(":checked").length)
                {
                    trashButtonView.removeClass("btn-primary-disabled");
                    trashButtonView.removeAttr("disabled");
                    trashButtonView.addClass("btn-primary");
                }
                else
                {
                    trashButtonView.addClass("btn-primary-disabled");
                    trashButtonView.attr("disabled","disabled");
                    trashButtonView.removeClass("btn-primary");
                }
            }, 100);
        });

    });
});
