var DATA = {
    route_from: '',
    route_to: '',
    carrier_administrative_area_from: ''
};

var YMAPS_BOUND = [[41.105996, 25.046698], [73.707848, 132.274515]];

$(document).ready(function() {

    ymaps.ready(init);

    function init() {
    }

    //ex iPhone|iPad|iPod
    var mobilePlatform = /Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    var phoneInput = $('input[data-gro-mask="phone"]');

    phoneInput.mask("+7 (999) 999-99-99");
    if (mobilePlatform) {
        phoneInput.attr('type','tel');
        phoneInput.attr('maxlength', 20);
    }


    //hide link from search index
    $('.as_link').replaceWith(function () {
        return '<a onclick="return !window.open(this.href)" href="' + $(this).data('link') + '" title="' + $(this).text() + '" >' + $(this).html() + '</a>';
    });

    var itemCategory = $('.dropdown .item-category');
    if (typeof itemCategory === 'object') {
        itemCategory.click(function () {
            $('input[name=ts_category]').val($(this).attr('data-id'));
        });
    }


    var itemBody = $('.dropdown .item-body');
    if (typeof itemBody === 'object') {
        itemBody.click(function () {
            $('input[name=type_body]').val($(this).attr('data-body')).trigger('change');
            $('input[name=ts_category]').val($(this).attr('data-category')).trigger('change');
        });
    }

    let inputsSuggest = $('.address-suggestions');
    if (inputsSuggest.length > 0) {
        $('.address-suggestions').autocomplete({
            minLength: 3,
            source: function (req, response) {
                let search_result = [];
                ymaps.suggest(req.term, {
                    results: 5,
                    boundedBy: YMAPS_BOUND
                }).then(function (res) {

                    if (typeof yandexMapCounterAdd === "function") {
                        yandexMapCounterAdd(ADDRESS_SUGGEST);
                    }

                    for (let i = 0; i < res.length; i++) {
                        search_result.push({
                            label: res[i].value,
                            value: res[i].value,
                        });
                        response(search_result);
                    }
                });
            },
            select: function (event, ui) {
                DATA.route_from = ui.item.value;
            }
        });
    }

    var inputFrom = $('input[name=from]').not('.address-suggestions');
    if (typeof inputFrom === 'object') {
        inputFrom.autocomplete({
            source: function (req, response) {
                var res;
                yandexApiCounter();
                ymaps.geocode(req.term, {
                    kind:'locality',
                    results:5,
                    json:true,
                    boundedBy: YMAPS_BOUND,
                    strictBounds: true
                }).then(function (data) {
                    res = convert(data);
                    response(res);
                });
            },
            select: function (event, ui) {
                DATA.route_from = ui.item.value;
            },
            minLength: 3
        });
    }

    var inputTo = $('input[name=to]').not('.address-suggestions');;
    if (typeof inputTo === 'object') {
        inputTo.autocomplete({
            source: function (req, response) {
                var res;
                yandexApiCounter();
                ymaps.geocode(req.term,  {
                    kind:'locality',
                    results:5,
                    json:true,
                    boundedBy: YMAPS_BOUND,
                    strictBounds: true
                }).then(function (data) {
                    res = convert(data);
                    response(res);
                });
            },
            select: function (event, ui) {
                DATA.route_to = ui.item.value;
            },
            minLength: 3
        });
    }

    //Р·Р°РєСЂС‹С‚РёРµ РјРѕРґР°Р»СЊРЅС‹С… СЃРѕРѕР±С‰РµРЅРёР№
    $(document).on('click', '.modal__closer, .modal__close__btn', function (e) {
        var modal = $(this).parents('.modal');
        closeModal(modal);

    });

    //Р·Р°СЏРІРєР° РЅР° РІС‹Р·РѕРІ
    $('#callback-landing button').click(function (e) {
        e.preventDefault();
        form = $('#callback-landing');

        clearModalErrors(form);

        if (validateModalForm(form)) {
            $.ajax({
                url: form.attr("action"),
                type: form.attr("method"),
                data: form.serialize(),
                success: function () {
                    $(form).parents('.modal__back-call').removeClass('active');
                    $(form).find('input').val('');
                    sendGoalAnalytic('g_call_hunter');
                    showMessage("РЎРїР°СЃРёР±Рѕ", "РњС‹ СЃРІСЏР¶РµРјСЃСЏ СЃ РІР°РјРё РІ Р±Р»РёР¶Р°Р№С€РµРµ РІСЂРµРјСЏ!");
                }
            });
        }
        return false;
    });

    $('.register_btn').click(function () {
        sendGoalAnalytic('gt_click_registration');
    });

    $('.land-take-order').on('click', function () {
        sendGoalAnalytic('g_submit_take_cargo');
    });

    $('#btn_calculate_book').click(function () {
        sendGoalAnalytic('gt_click_calcucate');
    });

    $('#btn_find_book').click(function () {
        sendGoalAnalytic('gt_click_find_cargo');
    });

    $('.header_phone_link').click(function(){
        sendGoalAnalytic('g_click_top_phone');
    });

    $('.footer_phone_link').click(function(){
        sendGoalAnalytic('g_click_footer_phones');
    });

    $('.mailto_btn').click(function(){
        sendGoalAnalytic('g_click_email');
    });

});


function checkRegion(admArea) {
    for (var i = 0; i < no_regions.length; i++) {
        // console.log(no_regions[i]+ ' vs ' + admArea + ' = ' + no_regions[i].match(new RegExp(admArea, "i")));
        admArea = admArea.replace(/\u2013|\u2014/g, "-");
        if (no_regions[i].match(new RegExp(admArea, "i"))) {
            return false;
        }
    }
    return true;
}

function showRegionError() {
    $('.modal__no_custom_price').addClass('active');
    //showMessage('РћС€РёР±РєР°', 'РЈРІР°Р¶Р°РµРјС‹Рµ РїР°СЂС‚РЅРµСЂС‹! Рљ СЃРѕР¶Р°Р»РµРЅРёСЋ, РїРѕ РґР°РЅРЅРѕРјСѓ РЅР°РїСЂР°РІР»РµРЅРёСЋ GroozGo РІСЂРµРјРµРЅРЅРѕ РЅРµ РѕСЃСѓС‰РµСЃС‚РІР»СЏРµС‚ РґРѕСЃС‚Р°РІРєСѓ.');
}

function yandexApiCounter() {
    //СЃС‡РµС‚С‡РёРє СЏРЅРґРµРєСЃ Р°РїРё
    if (typeof yandexMapCounterAdd === "function") {
        yandexMapCounterAdd(COORDS_BY_ADDRESS_JS);
    }
}

function convert(data) {
    var res = [];
    data.GeoObjectCollection.featureMember.forEach(function (item, i, arr) {
        res.push(item.GeoObject.metaDataProperty.GeocoderMetaData.text);
    });
    return res;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function clearModalErrors(form) {
    $(form).find('.help-block').each(function (index, elem) {
        $(elem).html('');
    });
}

function clearForms(form) {
    $(form).find('input').each(function (index, elem) {
        if ($(elem).attr('name') != 'hash') { // РќРµ С‡РёСЃС‚РёРј С…РµС€ РІ СЂРµРіРёСЃС‚СЂР°С†РёРё Рё РІРѕСЃСЃС‚Р°РЅРѕРІР»РµРЅРёРё РїР°СЂРѕР»СЏ!!!
            $(elem).val('');
        }
    });
    $(form).find('textarea').each(function (index, elem) {
        $(elem).val('');
    });
}

function validateModalForm() {
    validate = true;
    $(form).find('input').each(function (index, elem) {
        if ($(elem).val().trim() == '') {
            validate = false;
            $(elem).parents('.input').next('.help-block').html('Р—Р°РїРѕР»РЅРёС‚Рµ СЌС‚Рѕ РїРѕР»Рµ!');
        }
        else {
            if ($(elem).is('[name="email"]') && !validateEmail($(elem).val())) {
                validate = false;
                $(elem).parents('.input').next('.help-block').html('Р’РІРµРґРёС‚Рµ РєРѕСЂСЂРµРєС‚РЅС‹Р№ Р°РґСЂРµСЃ РїРѕС‡С‚С‹!');
            }
        }
    });
    $(form).find('textarea').each(function (index, elem) {
        if ($(elem).val().trim() == '') {
            validate = false;
            $(elem).next('.help-block').html('Р—Р°РїРѕР»РЅРёС‚Рµ СЌС‚Рѕ РїРѕР»Рµ!');
        }
    });

    return validate;
}

function showMessage(title, message) {
    dialog = $('[data-gui-block="alert"]');
    dialog.find('[data-gui-field="title"]').text(title);
    dialog.find('[data-gui-field="message"]').html(message);
    dialog.addClass('active');
}


function closeModal(modal) {
    $(modal).removeClass('active');
    $('.page').removeClass('page_modal_active');
    form = $(modal).find('form');
    SIGNUP.activate_step(0);
    if (form.length > 0) {
        clearModalErrors(form);
        clearForms(form);
    }
}
