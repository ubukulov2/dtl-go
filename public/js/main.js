if (typeof Promise !== "function") {
    document.write('<script src="../js/es6-promise.auto.min.js"></script>');
}
var review_captcha;

if (!Object.entries) {
    Object.entries = function( obj ) {
        var ownProps = Object.keys( obj ),
            i = ownProps.length,
            resArray = new Array(i); // preallocate the Array
        while (i--)
            resArray[i] = [ownProps[i], obj[ownProps[i]]];

        return resArray;
    };
}
if(!Object.values) {
    Object.values = function ( obj ) {
        var ownProps = Object.keys( obj ),
            i = ownProps.length,
            resArray = new Array();
        while (i--)
            resArray.push(obj[ownProps[i]]);

        return resArray;
    }
}
// Internet Explorer 6-9
if ( /*@cc_on!@*/false || document.documentMode ) {
    if(document.documentMode < 12) {
        alert('Р’Р°С€ Р±СЂР°СѓР·РµСЂ СѓСЃС‚Р°СЂРµР». Internet Explorer '+document.documentMode+' РїР»РѕС…Рѕ РїРѕРґРґРµСЂР¶РёРІР°РµС‚ СЃРѕРІСЂРµРјРµРЅРЅС‹Рµ СЃС‚Р°РЅРґР°СЂС‚С‹. ' +
            'РњС‹ РЅРµ РіР°СЂР°РЅС‚РёСЂСѓРµРј РєРѕСЂСЂРµРєС‚РЅСѓСЋ СЂР°Р±РѕС‚Сѓ СЃР°Р№С‚Р° РІ СЌС‚РѕРј Р±СЂР°СѓР·РµСЂРµ. РСЃРїРѕР»СЊР·СѓР№С‚Рµ СЃРѕРІСЂРµРјРµРЅРЅС‹Рµ Р±СЂР°СѓР·РµСЂС‹ - Chrome, Firefox, Opera, Yandex, Edge.');
        console.log(Error('РЎС‚Р°СЂС‹Р№ IE '+document.documentMode+' РЅРµ РїРѕРґРґРµСЂР¶РёРІР°РµС‚СЃСЏ, РЅСѓР¶РЅС‹ РєРѕСЃС‚С‹Р»Рё.'))
    }
}
$(document).ready(function(){
    var brows = new browserDetect();
    function browserDetect() {
        var body = getBody(), browser, blink, isIE, ieVersion, isOpera, isChrome, isEdge;
        // Opera 8.0+
        if ((window.opr && opr.addons) || window.opera || navigator.userAgent.indexOf(' OPR/') >= 0) {
            browser = 'opera';
            isOpera = true;
        }
        // Firefox 1.0+
        if (typeof InstallTrigger !== 'undefined') {
            browser = 'firefox';
        }
        // Safari 3.0+ "[object HTMLElementConstructor]"
        if (/constructor/i.test(window.HTMLElement) || (function (p) {
            return p.toString() === "[object SafariRemoteNotification]";
        })(!window['safari'] || safari.pushNotification)) {
            browser = 'safari';
        }
        // Internet Explorer 6-11
        if (/*@cc_on!@*/false || document.documentMode) {
            browser = 'ie';
            ieVersion = document.documentMode;
            isIE = true;
        }
        // Edge 20+
        if (!isIE && window.StyleMedia) {
            browser = 'edge';
            isEdge = true;
        }
        // Chrome 1+
        if (!!window.chrome && !!window.chrome.webstore) {
            browser = 'chrome';
            isChrome = true;
        }
        // Blink engine detection
        if ((isChrome || isOpera) && !!window.CSS) {
            blink = 'blink';
        }
        if (browser) toggleClass(body, 'browser_view_' + browser);
        if (ieVersion) toggleClass(body, 'ie_' + ieVersion);
        if (blink) toggleClass(body, 'browser_type_' + blink);
        return {
            browser: browser || '',
            blink: blink || false,
            isIE: isIE || false,
            isEdge: isEdge || false,
            isChrome: isChrome || false,
            isOpera: isOpera || false,
        }
    }

    basedFunctions().then(function () {
        return trailer().then(function () {
            sliding();
        })
    });

    function basedFunctions() {
        return new Promise(function (resolve, reject) {
            curtain();//Р·Р°РЅР°РІРµСЃ
            ulMenu();//РѕР±СЂР°Р±РѕС‚РєР° РїСѓРЅРєС‚РѕРІ РјРµРЅСЋ Р·Р°РЅР°РІРµСЃР°
            twoUlMenu();//РїРµСЂРµРєР»СЋС‡РµРЅРёРµ РѕРїРёСЃР°РЅРёСЏ РІ Р·Р°РЅР°РІРµСЃРµ
            linkModal();//СЃСЃС‹Р»РєРё, РѕС‚РєСЂС‹РІР°СЋС‰РёРµ РјРѕРґР°Р»СЊРЅС‹Рµ РѕРєРЅР°
            //modalBackCall();//РњРѕРґР°Р»СЊРЅРѕРµ РѕРєРЅРѕ - "Р—Р°РєР°Р·Р°С‚СЊ Р·РІРѕРЅРѕРє"
            tabsMenu();// РїРµСЂРµРєР»СЋС‡РµРЅРёРµ С‚Р°Р± РјРµРЅСЋ
            accordion(); //РђРєРѕСЂРґРёРѕРЅ
            buttons();// РєРЅРѕРїРєРё
            inputs(); //input
            massMedia();// РїСЂРѕРєСЂСѓС‚РєР° РЅРѕРІРѕСЃС‚РµР№
            drumMenu(); //"Р‘Р°СЂР°Р±Р°РЅ" РІРёРґРѕРІ РїРµСЂРµРІРѕР·РѕРє
            drumMenuTwo(); //"Р‘Р°СЂР°Р±Р°РЅ 2" РІРёРґРѕРІ РїРµСЂРµРІРѕР·РѕРє
            slider();//РЎР»Р°Р№РґРµСЂ
            scrollToElement();//РџСЂРѕРєСЂСѓС‚РєР° Рє СЏРєРѕСЂСЋ
            checkBox();//checkbox
            tooltip();//РїРѕРґСЃРєР°Р·РєР°
            carsSelect();//Р Р°Р·РјРµСЂ РіСЂСѓР·Р° (РІ Р·Р°СЏРІРєРµ)
            textarea();//textarea
            stepsInit();//С„РѕСЂРјР° СЃ РїРѕС€Р°РіРѕРІС‹Рј Р·Р°РїРѕР»РЅРµРЅРёРµРј
            appForm();//Р—Р°СЏРІРєР°
            //regForm();//Р РµРіРёСЃС‚СЂР°С†РёСЏ
            resolve();
        })
    }

    function buttons() {
        var buttons = getBlock('button');
        document.addEventListener('click',function (e) {
            if (buttons.length) {
                for (var i = 0; i < buttons.length; i++) {
                    var button = buttons[i];
                    var isClickInside = button.contains(e.target);
                    if (!isClickInside) removeClass(button, 'button_focus');
                    else toggleClass(button, 'button_focus');
                }
            }
        });
    }

    function textarea() {
        var text_areas = getBlock('textarea', false);
        if (text_areas.length) {
            for (var i = 0; i < text_areas.length; i++) {
                var text_area = text_areas[i];
                text_area.onblur = function () {
                    toggleClass(this, 'textarea_focused')
                };
                text_area.onfocus = function () {
                    toggleClass(this, 'textarea_focused')
                };
            }
        }
    }

    function massMedia() {
        var massMedia = getBlock('mass-media', false, 0);
        if(massMedia) {
            var wrap = getBlock(massMedia, 'mass-media__wrap', 0),
                marquee = getBlock(massMedia, 'mass-media__marquee', 0),
                block = getBlock(massMedia, 'mass-media__block', 0),
                items = getBlock(block, 'mass-media__item');

            var clone = block.cloneNode(true);
            wrap.appendChild(clone);


            if(items.length && wrap && marquee) {
                var width = items.length*items[0].offsetWidth;
                marquee.style.width = width+'px';

                var style = document.createElement("style");
                style.type="text/css";
                style.innerHTML = htmlKeyFrames(items.length*4.5);
                wrap.appendChild(style);
            }
            function htmlKeyFrames(s) {
                return ".mass-media__wrap {"+
                    "-webkit-animation: mass-media__marquee "+s+"s linear infinite;"+
                    "-moz-animation: mass-media__marquee "+s+"s linear infinite;"+
                    "animation: mass-media__marquee "+s+"s linear infinite;"+
                    "}"
            }
        }
    }

    function carsSelect() {
        var app_cars = getBlock('cars-select', false, 0);
        if (app_cars) {
            var cars_select = getBlock(app_cars, 'cars-select__select', 0),
                cars_items = getBlock(app_cars, 'cars-select__item');
            for (var i = 0; i < cars_items.length; i++) {
                cars_items[i].onclick = function () {
                    var item = this;
                    for (var q = 0; q < cars_items.length; q++) {
                        removeClass(cars_items[q], 'cars-select__item_active')
                    }
                    toggleClass(item, 'cars-select__item_active');
                    //cars_select.value = item.innerText;
                }
            }

        }
    }

    function tooltip() {
        var elements = document.querySelectorAll('[data-title]');
        for (var i = 0; i < elements.length; i++) {
            var tooltip = document.createElement("div");
            tooltip.className = "tooltip";
            elements[i].onmouseover = function () {

                var el = this,
                    el_parent = el.offsetParent;

                el_parent.appendChild(tooltip);
                toggleClass(tooltip, 'tooltip_visible');

                tooltip.innerHTML = getElemDataset(el, 'title');

                function positionTop() {
                    tooltip.style.left = el.offsetLeft + (el.clientWidth / 2) - (tooltip.clientWidth / 2) + "px";
                    tooltip.style.top = el.offsetTop /*+ top_body*/ - tooltip.clientHeight - 27 + "px";
                }

                var place = el.getAttribute('data-placement');
                if (place) {
                    if (place === 'top') positionTop();
                } else positionTop();
            };
            elements[i].onmouseout = function () {
                tooltip.innerHTML = '';
                tooltip.removeAttribute('style');
                removeClass(tooltip, 'tooltip_visible');
                tooltip.parentNode.removeChild(tooltip);
            };
        }
    }

    function checkBox() {
        var checkboxs = getBlock('checkbox'),
            groups = getBlock('checkbox-group'),
            toggle_blocks = document.querySelectorAll('[data-toggle-block]');
        if (checkboxs.length) {

            for (var i = 0; i < checkboxs.length; i++) {
                checkboxs[i].onclick = function (e) {
                    if(e.target.tagName !== "INPUT") {
                        var _this = this;
                        return thisToggleCheckbox(e, _this);
                    }
                }
            }
            function thisToggleCheckboxAction(checkbox) {
                var box = getBlock(checkbox, 'checkbox__control', 0);
                toggleClass(checkbox, 'checkbox_checked');
                if (!hasClass(checkbox, 'work__check')) {
                    toggleAttr(box, 'checked', 'checked');
                }
                var data = getDataValue(checkbox, 'data-toggle');
                if (data) {
                    for (var q = 0; q < toggle_blocks.length; q++) {
                        var block_data = getDataValue(toggle_blocks[q], 'data-toggle-block');
                        if (block_data === data) {
                            toggleClass(toggle_blocks[q], 'hidden')
                        }
                    }
                }
            }
            function thisToggleCheckbox(e, checkbox) {
                if(hasClass(checkbox, 'checkbox_disabled')){
                    return false;
                }
                if(hasClass(checkbox, 'checkbox_has_link')) {
                    if(!hasClass(e.target,'checkbox__text') && e.target.tagName !== "A" ) {
                        thisToggleCheckboxAction(checkbox)
                    }
                } else return thisToggleCheckboxAction(checkbox)
            }
        }
        if (groups.length) {
            for (var i = 0; i < groups.length; i++) {
                var group_checkboxs = getBlock(groups[i], 'checkbox');
                for (var q = 0; q < group_checkboxs.length; q++) {
                    group_checkboxs[q].onclick = function (e) {
                        e.preventDefault();
                        var checkbox = this;
                        for (var m = 0; m < group_checkboxs.length; m++) {
                            removeClass(group_checkboxs[m], 'checkbox_checked');
                            var box = getBlock(group_checkboxs[m], 'checkbox__control', 0);
                            removeAttr(box, 'checked');
                        }
                        toggleClass(checkbox, 'checkbox_checked');
                        var box = getBlock(checkbox, 'checkbox__control', 0);
                        toggleAttr(box, 'checked', 'checked');
                    }
                }
            }
        }
    }

    function scrollToElement() {
        var href = window.location.href,
            path = window.location.pathname;

        if (href.indexOf("#") && href.split("#")[1]) {
            var id = href.split("#")[1];
            if (id) doScroll(id, true);
        }

        var links = document.getElementsByTagName('A');
        for (var i = 0; i < links.length; i++) {
            var split = links[i].href.split("#")[1],
                anchor = split ? split : '';
            if (anchor) {
                links[i].onclick = function (e) {
                    var link = this;
                    if (link.pathname === window.location.pathname) {
                        e.preventDefault();
                        var hash = link.hash.replace('#', '');
                        curtain(false);
                        doScroll(hash)
                    }
                }
            }
        }

        function doScroll(id, timer) {
            var elem = document.getElementById(id);
            if (elem) {
                if (history.pushState) {
                    window.history.pushState("object or string", "Title", path + '#' + id);
                } else {
                    document.location.pathname = path + '#' + id;
                }
                if (elem) {
                    if (timer) {
                        setTimeout(function () {
                            elemScroll(elem);
                        }, 300)
                    } else elemScroll(elem);
                }
            }

        }
    }

    function getDataValue(elem, data) {
        if (brows.isIE) return elem.getAttribute(data);
        else {
            var arr = data.split('-');
            if (arr.length > 2) {
                var a_1 = '', a_2 = '';
                if (arr[0] === 'data') {
                    a_1 = arr[0];
                    arr.splice(0, 1);
                }
                for (var i = 0; i < arr.length; i++) {
                    var str;
                    if (i !== 0) str = arr[i][0].toUpperCase() + arr[i].substring(1);
                    else str = arr[i];
                    a_2 = a_2.concat(str);
                }
                arr = [a_1, a_2];
            }
            if (arr[0] === 'data') return elem.dataset[arr[1]];
            else return elem[data];
        }
    }

    /*function regForm() {
        var block = getBlock('reg', false, 0);
        var modal_get_code = getBlock('modal__get-code', false, 0);
        if(block) {
            var button_confirm_code = getBlock(modal_get_code, 'steps_action_next-step', 0);
            if(button_confirm_code) {
                button_confirm_code.addEventListener('click', nextStepClick);

                function nextStepClick(e) {
                    e.preventDefault();
                    closeModal();
                    stepsInit({key:0,value:1});
                }
                function closeModal() {
                    return linkModal(true);
                }
            }
        }
    }*/



    function appForm() {
        var app = getBlock('app', false, 0);
        if (app) {
            var inputs = getBlock('input__control'),
                dropdowns = [],
                searchParams = queryString(window.location.search);

            if (inputs.length) {
                for (var i = 0; i < inputs.length; i++) {
                    if(searchParams) {
                        for (key in searchParams) {
                            if(inputs[i].name === key) {
                                inputs[i].value = decodeURI(searchParams[key])
                            }
                        }
                    }
                    if (hasClass(inputs[i].parentNode, 'dropdown')) dropdowns.push(inputs[i].parentNode);
                }
            }
            if (dropdowns.length) {
                var block_temperature_range = getBlock(app, 'temperature-range', 0);

                for (var i = 0; i < dropdowns.length; i++) {
                    var input = findChildren(dropdowns[i], 'INPUT', 'array', 0),
                        name = getDataValue(input, 'name');
                    if (name === 'body-type') {//РґРѕР±Р°РІРёРј СЃРѕР±С‹С‚РёРµ РґР»СЏ РјРµРЅСЋ
                        var menu = getBlock(dropdowns[i], 'dropdown-menu', 0),
                            items = getBlock(menu, 'dropdown-menu__item-text');
                        addThisEventsClick(items,input);
                    }
                }
                function addThisEventsClick(items,_input) {
                    for (var q = 0; q < items.length; q++) {
                        items[q].addEventListener("click", function () {
                            var item = this;
                            return addThisEventClick(item,_input)
                        });
                    }
                }
                function addThisEventClick(item,__input) {
                    var parent = item.parentNode;
                    if (!hasClass(parent, 'dropdown-menu__item_disable')) {
                        removeClass(block_temperature_range, 'visible');
                        var placeholder = ', ',
                            __input_arr = __input.value !== '' ? __input.value.split(placeholder) : [];
                        for (var i = __input_arr.length-1; i >= 0; i--) {
                            if (__input_arr[i] === 'Р РµС„СЂРёР¶РµСЂР°С‚РѕСЂРЅС‹Р№') {
                                toggleClass(block_temperature_range, 'visible');
                                break;
                            }
                        }
                    }
                }
            }

            var steps = getBlock(app, 'steps__item'),
                lines = getBlock(app, 'steps__lines-item');

            if (steps.length && lines.length) {

                var obj = {};
                for (var i = 0; i < steps.length; i++) {
                    obj[i] = {
                        button_next: getBlock(steps[i], 'steps_action_next-step', 0),
                        button_previous: getBlock(steps[i], 'steps_action_previous-step', 0),
                        button_draft: getBlock(steps[i], 'steps_action_draft', 0),
                        button_calc: getBlock(steps[i], 'steps_action_calculate', 0),
                        button_send: getBlock(steps[i], 'steps_action_send', 0),
                        calculated: getBlock(steps[i], 'steps__calculated', 0),
                        from_do: getBlock(steps[i], 'steps__from-do', 0)
                    }
                }

                var _obj = Object.entries(obj);
                _obj.forEach(function (data) {

                    var key = Number(data[0]),
                        step = data[1];

                    if(step.button_send) step.button_send.onclick = function (e) {
                        e.preventDefault();
                        linkModal(false,'send-app');
                    };
                    if (step.button_calc) step.button_calc.onclick = function (e) {
                        e.preventDefault();
                        toggleClass(this, 'button_hidden');
                        if (step.button_draft) toggleClass(step.button_draft, 'button_hidden');
                        if (step.calculated) toggleClass(step.calculated, 'steps__calculated_visible');
                    };
                    if (step.from_do) {
                        var do_inputs = getBlock(step.from_do, 'input__control');
                        if (inputs.length) {
                            var reverse = getBlock(step.from_do, 'steps__reverse', 0);
                            if (reverse) {
                                reverse.onclick = function () {
                                    var val_1 = do_inputs[0].value,
                                        val_2 = do_inputs[1].value;
                                    do_inputs[0].value = val_2;
                                    do_inputs[1].value = val_1;
                                }
                            }
                        }
                    }
                    if (step.button_next) step.button_next.onclick = function (e) {
                        e.preventDefault();
                        return otherStep(key, key+1);
                    };
                    if (step.button_previous) step.button_previous.onclick = function (e) {
                        e.preventDefault();
                        return otherStep(key, key-1);
                    };
                    if (step.button_draft) step.button_draft.onclick = function (e) {
                        e.preventDefault();
                        console.log(Error('РќРµ Р·Р°РґР°РЅРѕ СЃРѕР±С‹С‚РёРµ'))
                    };
                    function otherStep(key, value) {
                        var this_data = {
                            key: key,
                            value: value
                        };
                        return stepsInit(this_data)
                    }

                });
            }
        }
    }

    function slider() {
        var sliders = getBlock('slider'),
            timer = 15000;
        if (sliders.length) {
            function getInterval(slider) {
                var body = getBody();
                if (body.offsetWidth < 768) return 1;
                else return hasClass(slider, 'slider_quatro') ? 4 : 2;
            }
            function arrowClick(str,slider) {
                return function () {
                    interval = getInterval(slider);
                    if(str === 'left') {
                        if (index - interval > 0) toggleItems(index -= interval);
                    } else if(str === 'right') {
                        if (index + interval <= items.length) toggleItems(index += interval);
                    }
                }
            }
            for (var i = 0; i < sliders.length; i++) {
                var index = 1,
                    interval = getInterval(sliders[i]),
                    items = getBlock(sliders[i], 'slider__item');
                if (items.length >= index) {
                    var arrow_left = getBlock(sliders[i], 'slider__left', 0),
                        arrow_right = getBlock(sliders[i], 'slider__right', 0);

                    arrow_left.onclick = arrowClick('left', sliders[i], index);
                    arrow_right.onclick = arrowClick('right', sliders[i], index);

                    toggleItems(index);

                    if(hasClass(sliders[i], 'slider_auto')) {
                        setInterval(function(){
                            var done = 0;
                            for (var q = 0; q < items.length; q++) {
                                if(hasClass(items[q], 'active')) done = q;
                            }
                            if(index+interval <= items.length) toggleItems(index += interval);
                            else toggleItems(index = 1);
                        }, timer);
                    }

                    function toggleItems(n) {
                        if (n > 0 && n <= items.length) {
                            for (var q = 0; q < items.length; q++) {
                                removeClass(items[q], 'active');
                                removeClass(arrow_left, 'hidden');
                                removeClass(arrow_right, 'hidden');
                            }
                            if (n < interval) {
                                toggleClass(arrow_left, 'hidden');
                            }
                            if ((items.length - n) < interval) {
                                toggleClass(arrow_right, 'hidden');
                            }
                            if (n <= items.length) {
                                toggleClass(items[n-1], 'active');
                            }

                            if(n < items.length && interval !== 1) {
                                for(var q = 0; q < items.length; q++){
                                    if(q > 0 && q > n && (q < (n+interval))) toggleClass(items[q-1], 'active');
                                    if(items.length-q === 1 && n+interval > items.length-1) {
                                        if(!hasClass(items[q], 'active')) toggleClass(items[q], 'active');
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }
    }

    function drumMenu() {
        var drum = getBlock('drum', false, 0);
        if (drum) {
            var icon = getBlock(drum, 'drum__icon', 0),
                items = getBlock(drum, 'drum__item'),
                item_zero = items[0],
                item_zero_dataset = getDataValue(item_zero, 'data-icon');

            toggleClass(item_zero, 'active');
            toggleClass(icon, 'drum__icon_' + item_zero_dataset);

            for (var i = 0; i < items.length; i++) {
                var arrow = document.createElement("span");
                arrow.className = "drum__arrow";
                items[i].appendChild(arrow);
                items[i].onclick = function () {
                    var el = this,
                        el_dataset = getDataValue(el, 'data-icon');

                    for (var q = 0; q < items.length; q++) {
                        removeClass(items[q], 'active');
                    }
                    toggleClass(el, 'active');
                    for (var q = 0; q < items.length; q++) {
                        var item_dataset = getDataValue(items[q], 'data-icon'),
                            item_class = 'drum__icon_' + item_dataset;
                        if (hasClass(icon, item_class)) removeClass(icon, item_class);
                    }
                    toggleClass(icon, 'drum__icon_' + el_dataset);
                }
            }
        }
    }

    function drumMenuTwo() {
        var drum = getBlock('drum-light', false, 0);
        if (drum) {
            var items = getBlock(drum, 'drum-light__item');

            function itemsOnMouse(event, el) {
                if(event.target.nodeName === 'A') {
                    if(event.type === 'mouseover') {
                        for (var q = 0; q < items.length; q++) {
                            removeClass(items[q], 'hovered');
                        }
                        toggleClass(el, 'hovered');
                    }
                    if(event.type === 'mouseout') {
                        removeClass(el, 'hovered');
                    }
                }
            }
            for (var i = 0; i < items.length; i++) {
                var arrow = document.createElement("span");
                arrow.className = "drum-light__arrow";
                items[i].appendChild(arrow);
                items[i].onmouseover = items[i].onmouseout = function (event) {
                    return itemsOnMouse(event, this);
                }
            }
        }
    }

    function inputs() {
        var inputs = getBlock('input');
        if (inputs.length) {

            var obj = {};
            for (var i = 0; i < inputs.length; i++) {
                obj[i] = {
                    input: inputs[i],
                    el: findChildren(inputs[i], 'INPUT', 'array', 0)
                }
            }

            var _obj = Object.entries(obj);

            _obj.forEach(function (data) {

                var key = Number(data[0]),
                    input = data[1];

                input.el.onfocus = function () {
                    toggleClass(input.input, 'input_focus')
                };
                input.el.addEventListener("focusout", function () {
                    removeClass(input.input, 'input_focus')
                });

                if (hasClass(input.input, 'dropdown')) {

                    var paranja = document.createElement("span"),
                        menu = getBlock(input.input, 'dropdown-menu', 0),
                        items = getBlock(menu, 'dropdown-menu__item-text');

                    paranja.className = "paranja";
                    menu.style.top = hasClass(input.input, 'input_theme_gray') ? (input.input.offsetHeight - 2) + 'px' : input.input.offsetHeight + 'px';

                    document.addEventListener('click',function (e) {
                        if (items.length) {
                            var isClickInside = input.input.contains(e.target);

                            for (var i = 0; i < items.length; i++) {
                                var item = items[i];
                                if (item.contains(e.target))
                                {
                                    isClickInside = true;
                                }
                            }
                            if (!isClickInside)
                            {
                                for (var m = 0; m < items.length; m++) {
                                    removeClass(items[m], 'dropdown-menu__item_checked');
                                    removeClass(items[m], 'hover');
                                }
                                removeClass(menu, 'active');
                            }
                        }
                    });

                    function toggleThisInputValue(__input,__this) {

                        var _el_value = getDataValue(__this, 'data-value'),
                            placeholder = ', ',
                            __input_arr = __input.value !== '' ? __input.value.split(placeholder) : [],
                            done = 0;
                        for (var i = __input_arr.length-1; i >= 0; i--) {
                            if (__input_arr[i] === _el_value) {
                                __input_arr.splice(i, 1);
                                done++;
                                break;
                            }
                        }
                        if (done === 0) __input_arr.push(_el_value);
                        __input.value = __input_arr.join(placeholder)

                    }

                    function clickFunction(_this) {
                        var _parent = _this.parentNode,
                            _ul = hasClass(_parent.parentNode, 'dropdown-menu') ? _parent.parentNode : _parent.parentNode.parentNode.parentNode,
                            _input = findChildren(_ul.parentNode, 'INPUT', 'array', 0),
                            _items = getBlock(_ul, 'dropdown-menu__item');

                        if (!hasClass(_parent, 'dropdown-menu__item_disable')) {
                            if(!hasClass(_ul, 'dropdown-menu_checkbox')) {
                                if (hasClass(_parent, 'dropdown-menu__child-menu')) {
                                    for (var m = 0; m < _items.length; m++) {
                                        if(hasClass(_items[m], 'dropdown-menu__child-menu')) {
                                            removeClass(_items[m], 'hover');
                                        }
                                    }
                                    toggleClass(_parent, 'hover');
                                } else {
                                    for (var m = 0; m < _items.length; m++) {
                                        removeClass(_items[m], 'dropdown-menu__item_checked');
                                        removeClass(_items[m], 'hover');
                                    }
                                    toggleClass(_parent, 'dropdown-menu__item_checked');
                                    _input.value = getDataValue(_this, 'data-value');
                                    removeClass(_ul, 'active');
                                }
                            } else {
                                toggleThisInputValue(_input,_this);
                                toggleClass(_parent, 'dropdown-menu__item_checked');
                            }
                        }

                    }

                    for (var q = 0; q < items.length; q++) {
                        items[q].onclick = function () {
                            clickFunction(this);
                        };
                        var parent = items[q].parentNode, item_menu = findChildren(parent, 'UL', 'array');
                        if (item_menu.length) {
                            toggleClass(parent, 'dropdown-menu__child-menu');
                            var arrow = document.createElement('span');
                            toggleClass(arrow, 'li-arrow');
                            parent.insertBefore(arrow, parent.firstChild);
                        }
                        if(hasClass(menu, 'dropdown-menu_checkbox')) {
                            var box = document.createElement("span"),
                                box_arrow = document.createElement("span");
                            box.className = "help-box";
                            box_arrow.className = "help-box__arrow";
                            box.appendChild(box_arrow);
                            parent.appendChild(box);
                            box.onclick = function () {
                                clickFunction(this);
                            };
                        }
                    }

                    paranja.onclick = function () {
                        var _this = this,
                            this_parent = _this.parentNode,
                            this_menu = getBlock(this_parent, 'dropdown-menu', 0);

                        for (var q = 0; q < inputs.length; q++) {
                            if (hasClass(inputs[q], 'dropdown')) {
                                var q_menu = getBlock(inputs[q], 'dropdown-menu', 0);
                                if (this_menu !== q_menu) removeClass(q_menu, 'active');
                            }
                        }
                        toggleClass(this_menu, 'active');
                        for (var q = 0; q < items.length; q++) {
                            var items_parent = items[q].parentNode,
                                item_menu = findChildren(items_parent, 'UL', 'array');
                            if (item_menu.length) {
                                removeClass(items_parent, 'dropdown-menu__offset');
                                var body = getBody(),
                                    item_menu_rect = item_menu[0].getBoundingClientRect();
                                if (body.offsetWidth < item_menu_rect.left + item_menu_rect.width) {
                                    toggleClass(items_parent, 'dropdown-menu__offset');
                                }
                            }
                        }
                    };
                    input.input.appendChild(paranja);
                }
            });
        }
    }

    function accordion() {
        var accordions = getBlock('accordion');
        if (accordions.length) {
            var obj = {};
            for (var i = 0; i < accordions.length; i++) {
                obj[i] = {
                    accordion: accordions[i],
                    items: getBlock(accordions[i], 'accordion__item')
                }
            }
            var _obj = Object.entries(obj);
            _obj.forEach(function (data) {
                var key = Number(data[0]),
                    accordion = data[1],
                    items = accordion.items;
                Object.values(items).forEach(function(item) {
                    var item_title = getBlock(item, 'accordion__title', 0);
                    item_title.onclick = function () {
                        Object.values(items).forEach(function(it) {
                            if (it !== item && hasClass(it,'active')){
                                removeClass(it, 'active');
                            }
                        });
                        toggleClass(item, 'active');
                    }
                });
            })
        }
    }

    function tabsMenu() {
        var tabs = getBlock('tabs');
        if (tabs.length) {

            var obj = {};
            for (var i = 0; i < tabs.length; i++) {
                obj[i] = {};
                obj[i].nav = getBlock(tabs[i], 'tabs__nav', 0);
                obj[i].nav_items = findChildren(obj[i].nav, 'DIV', 'array');
                obj[i].content = getBlock(tabs[i], 'tabs__content', 0);
                obj[i].content_items = findChildren(obj[i].content, 'DIV', 'array');
            }

            var _obj = Object.entries(obj);
            _obj.forEach(function (data) {
                var key = Number(data[0]),
                    tab = data[1],
                    active_nav,
                    active_item;

                for (var i = 0; i < tab.nav_items.length; i++) {
                    if (hasClass(tab.nav_items[i], 'active')) active_nav = tab.nav_items[i];
                    if (hasClass(tab.content_items[i], 'active')) active_item = tab.content_items[i];
                }

                if (!active_nav) {
                    toggleClass(tab.nav_items[0], 'active');
                    toggleClass(tab.content_items[0], 'active');
                }

                function toggleThisTab(q_link, q_content) {
                    return function () {
                        for (var m = 0; m < tab.nav_items.length; m++) {
                            removeClass(tab.nav_items[m], 'active');
                            removeClass(tab.content_items[m], 'active');
                        }
                        toggleClass(q_link, 'active');
                        toggleClass(q_content, 'active');
                        var input_focused = getBlock(q_content, 'input__control', 0);
                        if(input_focused) input_focused.focus()
                    }
                }
                for (var q = 0; q < tab.nav_items.length; q++) {
                    var q_link = tab.nav_items[q],
                        q_content = tab.content_items[q];
                    q_link.onclick = toggleThisTab(q_link, q_content)
                }

            });
        }
    }

    document.onkeydown = function (e) {
        e = e || window.event;
        var isEscape = false;
        if ("key" in e) isEscape = (e.key === "Escape" || e.key === "Esc");
        else isEscape = (e.keyCode === 27);
        if (isEscape) {
            curtain(false);
            linkModal(true);
        }
    };

    /*function modalBackCall() {
        var modal = getBlock('modal__back-call', false, 0);
        if (modal) {
            var form = getBlock(modal, 'form', 0);
            form.addEventListener('submit', function (e) {
                e.preventDefault();//TODO РµСЃР»Рё Р°СЏРєСЃ
                console.log(Error('РќРµ Р·Р°РґР°РЅРѕ СЃРѕР±С‹С‚РёРµ'));
                removeClass(modal, 'active');
            }, false);
        }
    }*/

    function linkModal(closeModal,openModal) {
        var modals = getBlock('modal'),
            links = getBlock('link_modal'),
            page = getBlock('page', false, 0);
        if(!closeModal) {
            for (var i = 0; i < links.length; i++) {
                links[i].onclick = function (e) {
                    e.preventDefault();
                    var link = this;
                    var input_focused;
                    var linkDataset = getElemDataset(link,'group');
                    if (linkDataset) {
                        var tabs = getBlock('tabs');
                        for (var q = 0; q < tabs.length; q++) {
                            if (getElemDataset(tabs[q],'tabs') === getElemDataset(link,'tabs')) {
                                var nav = getBlock(tabs[q], 'tabs__nav', 0),
                                    nav_items = findChildren(nav, 'DIV', 'array'),
                                    content = getBlock(tabs[q], 'tabs__content', 0),
                                    content_items = findChildren(content, 'DIV', 'array');

                                for (var m = 0; m < nav_items.length; m++) {
                                    removeClass(nav_items[m], 'active');
                                    removeClass(content_items[m], 'active');
                                }
                                toggleClass(nav_items[linkDataset], 'active');
                                toggleClass(content_items[linkDataset], 'active');
                                input_focused = getBlock(content_items[linkDataset], 'input__control', 0);
                            }
                        }
                    }
                    for (var q = 0; q < modals.length; q++) {
                        if (getElemDataset(link,'modal') === getElemDataset(modals[q],'modal')) {
                            openCurrentModal(modals[q], input_focused)
                        }
                    }
                }
            }
            function openCurrentModal(modal,input_focused) {
                removeClass(modal, 'offset-width');
                removeClass(modal, 'offset-height');
                toggleClass(page, 'page_modal_active');
                toggleClass(modal, 'active');
                if (modal.id === 'review_modal' && review_captcha !== undefined)
                {
                    review_captcha.generate();
                }
                var body = getBody(),
                    modal_wrap = getBlock(modal, 'modal__wrap', 0),
                    wrap_rect = modal_wrap.getBoundingClientRect();

                if (body.offsetWidth < wrap_rect.width + 80) toggleClass(modal, 'offset-width');
                if (body.offsetHeight < wrap_rect.height + 80) toggleClass(modal, 'offset-height');

                if(!input_focused) input_focused = getBlock(modal, 'input__control', 0);
                if(input_focused) input_focused.focus();

                modal.onclick = function () {

                    var _this = this;
                    removeClass(_this, 'active');
                    removeClass(page, 'page_modal_active');
                };
                var wrap = findChildren(modal, 'DIV', 'elem'),
                    wrap_items = findChildren(wrap, 'DIV', 'array');
                for (var m = 0; m < wrap_items.length; m++) {
                    wrap_items[m].onclick = function (e) {
                        e.stopPropagation();
                    };
                    if (hasClass(wrap_items[m], 'modal__closer')) {
                        wrap_items[m].onclick = function () {
                            removeClass(modal, 'active');
                            removeClass(page, 'page_modal_active');
                        }
                    }
                }
            }
        }
        if (closeModal) {
            for (var q = 0; q < modals.length; q++) {
                removeClass(modals[q], 'active');
                removeClass(page, 'page_modal_active');
            }
        }
        if (openModal) {
            for (var q = 0; q < modals.length; q++) {
                if (openModal === getElemDataset(modals[q],'modal')) {
                    openCurrentModal(modals[q])
                }
            }
        }
    }

    //header menu button
    function curtain(click) {
        var page = getBlock('page', false, 0),
            menu = getBlock('main-menu', false, 0),
            button = getBlock('menu-button', false, 0),
            header = getBlock('header', false, 0);
        if (button) {
            button.onclick = function () {
                toggleClass(page, 'page_overflow_hidden');
                toggleClass(menu, 'main-menu_hidden');
                toggleClass(header, 'header_z-index');
                toggleClass(button, 'menu-button_active');
            };
            if (typeof click === 'boolean') {
                if (!click && !hasClass(menu, 'main-menu_hidden')) button.onclick();
                if (click) button.onclick();
            }
        }
    }

    function ulMenu() {
        var menu = getBlock('ul-menu');
        if (menu.length) {
            for (var i = 0; i < menu.length; i++) {
                var ul = findChildren(menu[i], 'LI', 'array');
                for (var q = 0; q < ul.length; q++) {
                    var li = ul[q];
                    if (li.children.length) {
                        for (var m = 0; m < li.children.length; m++) {
                            var el = li.children[m];
                            if (el.tagName === 'UL') li.className = 'ul-menu__child-menu';
                        }
                    }
                }
                var child_menus = getBlock(menu[i],'ul-menu__child-menu');
                if (child_menus) thisToggleMenu(child_menus)
            }
            function thisToggleMenu(child_menus) {
                for (var i = 0; i < child_menus.length; i++) {
                    var childrens = child_menus[i].children;
                    if (childrens.length) {
                        var link, ul_child;
                        for (var q = 0; q < childrens.length; q++) {
                            var el = childrens[q];
                            if (el.tagName === 'A') link = el;
                            if (el.tagName === 'UL') ul_child = el;
                        }
                        if (link) link.onclick = toggleThisMenu(ul_child)
                    }
                }
                function toggleThisMenu(ul_child) {
                    return function(){
                        if (ul_child) toggleClass(ul_child, 'active')
                    }
                }
            }
        }
    }

    function twoUlMenu() {
        var main_menu = getBlock('main-menu', false, 0);
        if (main_menu) {
            var block = getBlock('menu-two__links', false, 0);
            if (block) {
                var items = getBlock(main_menu, 'menu-two__items', 0);
                items = findChildren(items, 'DIV', 'array');
                if(items.length && block.children.length) {
                    var links = [];
                    for (var i = 0; i < block.children.length; i++) {
                        var el = block.children[i];
                        if (el.tagName === 'UL') {
                            var array = findChildren(el, 'LI', 'array');
                            for (var i = 0; i < array.length; i++) {
                                links.push(findChildren(array[i], 'A', 'elem'));
                            }
                        }
                    }
                    function toggleThis(item,link) {
                        return function () {
                            for (var q = 0; q < links.length; q++) {
                                removeClass(links[q], 'active');
                            }
                            for (var m = 0; m < items.length; m++) {
                                removeClass(items[m], 'active');
                            }
                            toggleClass(item, 'active');
                            toggleClass(link, 'active');
                        }
                    }
                    for (var i = 0; i < links.length; i++) {
                        links[i].onclick = toggleThis(items[i],links[i]);
                    }
                }
            }
        }
    }

    function sliding() {
        var header = getBlock('header', false, 0),
            arrows = getBlock('arrow'),
            layouts = getBlock('layout'),
            arr = [];
        for (var i = 0; i < arrows.length; i++) {
            arr.push(arrows[i]);
        }
        function toggle(el) {
            return toggleClass(el, 'sliding')
        }
        function headerTimer() {
            if (header) return 100;
            else return 0
        }
        function toggleFirst(el) {
            setTimeout(function () {
                toggle(el);
            }, 100)
        }
        if (header) toggleClass(header, 'sliding');
        setTimeout(function () {
            arr.map(function (el) {
                toggle(el);
            });
            for (var i = 0; i < layouts.length; i++) {
                if(i === 0) toggleFirst(layouts[i]);
                else toggle(layouts[i]);
            }
        }, headerTimer());
    }

    function trailer() {

        return new Promise(function (resolve) {
            var trailer = getBlock("trailer", false, 0),
                pageCss = getBlock("page", false, 0);
            /*if (trailer && pageCss) {
                if (Number(getCookie('view_trailer')) === 14) {
                    removeClass(pageCss, 'page_loading');
                    addClass(trailer, 'hidden');
                    resolve();
                } else {
                    removeClass(trailer, 'hidden');
                    var loading = getBlock(trailer, "loader-wrapper", 0);
                    if (loading) {
                        var done = 0;
                        setTimeout(function () {
                            if (!hasClass(trailer, 'load')) toggleClass(trailer, 'load');
                            if (done === 0) {
                                done++;
                                doneTimer(1000);
                            }
                        }, 10);

                        function doneTimer(time) {
                            setCookie('view_trailer', 1, {path: '/'});
                            setTimeout(function () {
                                toggleClass(trailer, 'hidden');
                                setTimeout(function () {
                                    toggleClass(pageCss, 'page_loading');
                                }, 100);
                                resolve(1);
                            }, time);
                        }

                    }
                }
            } else {
                toggleClass(pageCss, 'page_loading');

            };*/
            resolve(1);
        });
    }
});

function stepsInit(data, reg) {
    var layout = getBlock('layout', false, 0);
    if (reg)
    {
        var block = getBlock('steps_reg', false, 0);
    }
    else
    {
        var block = getBlock('steps', false, 0);
    }


    if (block) {
        var searchParams = queryString(window.location.search),
            query = searchParams && searchParams.step ? searchParams.step : 1,
            items = getBlock(block, 'steps__item'),
            lines = getBlock(block, 'steps__lines-item');

        if (data && typeof data === 'object') {

            for (var q = 0; q < items.length; q++) {
                if (hasClass(items[q], 'active')) removeClass(items[q], 'active');
                if (hasClass(lines[q], 'active')) removeClass(lines[q], 'active');
            }
            if (layout) elemScroll(layout);
            toggleClass(items[data.value], 'active');
            toggleClass(lines[data.value], 'active');
            if (data.key !== 0) {
                if (data.key < items.length) removeClass(lines[data.value], 'done');
            } else if (data.key < items.length) toggleClass(lines[data.key], 'done');

        } else {

            var this_init = function () {
                if (items.length && lines.length) {
                    function togActive(val) {
                        toggleClass(lines[val], 'active');
                        toggleClass(items[val], 'active');
                    }
                    if (searchParams && searchParams.step) {
                        //console.log(Error('РќРµРѕР±С…РѕРґРёРјР° РїСЂРѕРІРµСЂРєР° РїСЂРµРґС‹РґСѓС‰РёС… С€Р°РіРѕРІ'));
                        query = query - 1;
                        if (query > 0 && query <= lines.length) {
                            togActive(query);
                            for (var i = 0; i < lines.length; i++) {
                                removeClass(lines[i], 'done');
                                if (i < query) toggleClass(lines[i], 'done');
                            }
                        } else togActive(0);
                    } else togActive(0);
                }
            };
            return this_init()
        }
    }

}

function getElemDataset(el, value) {
    var result;
    if(el.dataset) result = el.dataset[value];
    else result = el.getAttribute('data-'+value);
    return result
}
function getBody() {
    return document.getElementsByTagName('BODY')[0]
}

function elemScroll(elem) {
    var data = {
        behavior: 'smooth',
        block: 'start',
        inline: 'start'
    };
    return elem.scrollIntoView(data);
}

function queryString(str) {
    var ret = Object.create(null);
    if (typeof str !== 'string') {
        return ret;
    }
    str = str.trim().replace(/^[?#&]/, '');
    if (!str) {
        return ret;
    }
    str.split("&").forEach(function (param) {
        var parts = param.replace(/\+/g, ' ').split('='),
            key = parts.shift(),
            val = parts.length > 0 ? parts.join('=') : undefined;
        ret[key] = val;
    });
    return Object.keys(ret).sort().reduce(function (result, key) {
        var val = ret[key];
        if (Boolean(val) && typeof val === 'object' && !Array.isArray(val)) {
            result[key] = keysSorter(val);
        } else {
            result[key] = val;
        }
        return result;
    }, Object.create(null));
}

function keysSorter(input) {
    if (Array.isArray(input)) {
        return input.sort();
    } else if (typeof input === 'object') {
        return keysSorter(Object.keys(input)).sort(function (a, b) {
            return Number(a) - Number(b);
        }).map(function (key) {
            return input[key];
        });
    }
    return input;
}

function hasClass(target, className) {
    return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
}

function toggleAttr(elem, attr, val) {
    if (!elem.getAttribute(attr)) {
        return elem.setAttribute(attr, val);
    } else {
        return elem.removeAttribute(attr);
    }
}

function removeAttr(elem, attr) {
    if (elem.getAttribute(attr)) {
        return elem.removeAttribute(attr);
    }
}

function toggleClass(obj, cls) {

    //$(obj).toggleClass(cls);

    if (obj.classList) obj.classList.toggle(cls);
    else {
        var classes = obj.className.split(" "),
            i = classes.indexOf(cls);
        if (i >= 0) classes.splice(i, 1);
        else classes.push(cls);
        obj.className = classes.join(" ");
    }
}

function addClass(obj, cls) {

    var classes = obj.className.split(" "),
        i = classes.indexOf(cls);
    if (i < 0) classes.push(cls);
    obj.className = classes.join(" ");

}

function removeClass(obj, cls) {
    if (obj) {
        var classes = obj.className.split(' ');
        for (var i = 0; i < classes.length; i++) {
            if (classes[i] === cls) {
                classes.splice(i, 1);
                i--; // (*)
            }
        }
        obj.className = classes.join(' ');
    }
}

function findChildren(obj, tag, type, num) {
    if (type === 'array') {
        var arr = [];
        if (obj && obj.children.length) {
            for (var i = 0; i < obj.children.length; i++) {
                var el = obj.children[i];
                if (el.tagName === tag) arr.push(el);
            }
        }
        if (num === 0 || num > 0) return arr[num];
        else return arr;
    }
    if (type === 'elem') {
        var elem = {};
        if (obj.children.length) {
            for (var i = 0; i < obj.children.length; i++) {
                if (obj.children[i].tagName === tag) elem = obj.children[i];
            }
        }
        return elem;
    }
}

function getBlock(data, cssClass, num) {
    var arr = [];
    if (typeof data === 'string') {
        arr = document.getElementsByClassName(data);
        if (!num && typeof cssClass === 'number') num = cssClass;
    }
    else if (typeof data === 'object') {
        arr = data.getElementsByClassName(cssClass);
    }
    if (num === 0 || num > 0) return arr[num];
    else return arr;
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options) {
    options = options || {};
    var expires = options.expires;
    if (typeof expires === "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) options.expires = expires.toUTCString();
    value = encodeURIComponent(value);
    var updatedCookie = name + "=" + value;
    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}

function debounce(func, wait, immediate){
    var timeout, args, context, timestamp, result;
    if (null === wait) wait = 100;
    function later() {
        var last = Date.now() - timestamp;
        if (last < wait && last >= 0) {
            timeout = setTimeout(later, wait - last);
        } else {
            timeout = null;
            if (!immediate) {
                result = func.apply(context, args);
                context = args = null;
            }
        }
    }
    var debounced = function(){
        context = this;
        args = arguments;
        timestamp = Date.now();
        var callNow = immediate && !timeout;
        if (!timeout) timeout = setTimeout(later, wait);
        if (callNow) {
            result = func.apply(context, args);
            context = args = null;
        }
        return result;
    };
    debounced.clear = function() {
        if (timeout) {
            clearTimeout(timeout);
            timeout = null;
        }
    };
    debounced.flush = function() {
        if (timeout) {
            result = func.apply(context, args);
            context = args = null;

            clearTimeout(timeout);
            timeout = null;
        }
    };
    return debounced;
};
