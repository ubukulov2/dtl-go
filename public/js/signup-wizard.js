/**
 * РњР°СЃС‚РµСЂ СЂРµРіРёСЃС‚СЂР°С†РёРё, С„РѕСЂРјР° РІС…РѕРґР°, РІРѕСЃСЃС‚Р°РЅРѕРІР»РµРЅРёРµ РїР°СЂРѕР»СЏ
 */
//
var SIGNUP = SIGNUP || {};

// СЃРµР»РµРєС‚РѕСЂ РґРёР°Р»РѕРіР° СЃ РјР°СЃС‚РµСЂРѕРј Рё СЃРµР»РµРєС‚РѕСЂС‹ РґР»СЏ С€Р°РіРѕРІ
// РґР»СЏ РєР°Р¶РґРѕРіРѕ С€Р°РіР° РЅСѓР¶РµРЅ СЃРµР»РµРєС‚РѕСЂ РєРѕРЅС‚РµРЅС‚Р° (Р±Р»РѕРє РІРЅСѓС‚СЂРё РјРѕРґР°Р»СЊРЅРѕРіРѕ РґРёР°Р»РѕРіР°)
// СЃРµР»РµРєС‚РѕСЂ С„РѕСЂРјС‹ РґР»СЏ СЃР±РѕСЂР° РїРѕР»РµР№ РІРІРѕРґР°
// СЃРµР»РµРєС‚РѕСЂ РєРЅРѕРїРєРё РґР»СЏ СѓСЃС‚Р°РЅРѕРІРєРё РѕР±СЂР°Р±РѕС‚С‡РёРєР° РЅР°Р¶Р°С‚РёСЏ (Р”Р°Р»РµРµ)


function loader(){
    let ajaxLoadTimeout, criticalStop;
    $(document).ajaxStart(function () {
        ajaxLoadTimeout = setTimeout(function () {
            $('#loading').removeClass('hidden');
        }, 2*1000);
        criticalStop = setTimeout(function () {
            $('#loading').addClass('hidden');
        }, 60*1000);
    }).ajaxSuccess(function () {
        clearTimeout(ajaxLoadTimeout);
        $('#loading').addClass('hidden');
    });
}

SIGNUP.settings = {
    'user_type': '',
    'modal_selector': '.modal__auth',
    'is_fast_signup': false,
    'steps': [
        {
            'content_sel': '.signup-wizard-content-1',
            'form_sel': '.signup-wizard-form-1',
            'next_btn_sel': '.signup-wizard-next-btn-1'
        },
        {
            'content_sel': '.signup-wizard-content-2',
            'form_sel': '.signup-wizard-form-2',
            'next_btn_sel': '.signup-wizard-next-btn-2',
            'phone_sel': '.signup-wizard-2-phone', // СЃСЋРґР° РґРѕР±Р°РІРёС‚СЃСЏ С‚РµР»РµС„РѕРЅ РЅР° РІС‚РѕСЂРѕРј С€Р°РіРµ
            'phone_input' : '.signup-wizard-2-phone_input',
            'code_input' : '.signup-wizard-2-code_input',
            'reset_btn' : '.signup-wizard-res-btn-2',
            'back_btn' : '.signup-wizard-back-btn-2'
        },
        {
            'content_sel': '.signup-wizard-content-3',
            'form_sel': '.signup-wizard-form-3',
            'next_btn_sel': '.signup-wizard-next-btn-3',
            'phone_sel': '.signup-wizard-3-phone', // СЃСЋРґР° РґРѕР±Р°РІРёС‚СЃСЏ С‚РµР»РµС„РѕРЅ РЅР° РІС‚РѕСЂРѕРј С€Р°РіРµ
            'phone_input' : '.signup-wizard-3-phone_input'
        },
        {
            'content_sel': '.signup-wizard-content-4',
            'form_sel': '.signup-wizard-form-4',
            'next_btn_sel': '.signup-wizard-next-btn-4'
        },
        {
            'content_sel': '.signup-wizard-content-5',
            'form_sel': '.signup-wizard-form-5',
            'next_btn_sel': '.signup-wizard-next-btn-5'
        }
    ]
};

SIGNUP.start = function () {
    // РќРµ СѓСЃС‚Р°РЅРѕРІР»РµРЅ С‚РёРї РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ вЂ” С„РѕСЂРјСѓ РЅРµ РѕС‚РѕР±СЂР°Р¶Р°С‚СЊ.
    // TODO РµСЃР»Рё 0, С‚Рѕ РѕС‚РѕР±СЂР°Р·РёС‚СЊ РІС‹Р±РѕСЂ СЋР·РµСЂР°. РµСЃР»Рё РЅРµС‚, С‚Рѕ СѓСЃС‚Р°РЅРѕРІРёС‚СЊ РєРѕРЅС‚СЂРѕР» РІС‹Р±РѕСЂР°
    // РІ Р·РЅР°С‡РµРЅРёРµ  Рё СЃРєСЂС‹С‚СЊ. РёР»Рё РїРѕС…РѕР¶Рµ СЃРґРµР»Р°С‚СЊ СЌС‚Рѕ РІ РїР»Р°РіРёРЅРµ...
//    if (SIGNUP.settings.user_type == 0) { return; }
    // РІРѕ РІСЃРµС… РїРѕР»СЏС… СѓСЃС‚Р°РЅРѕРІРёС‚СЊ РїСѓСЃС‚С‹Рµ Р·РЅР°С‡РµРЅРёСЏ
    SIGNUP.form_empty_all();
    // РїРѕРєР°Р·Р°С‚СЊ РїРµСЂРІС‹Р№ С€Р°Рі
    //SIGNUP.activate_step(0);
    SIGNUP.activate_step(0);
    // РїРѕРєР°Р·Р°С‚СЊ РґРёР°Р»РѕРі
    SIGNUP.activateAutocomplete('.geocodeField');
    SIGNUP.cleanAutocomplete('.geocodeField');
    var userTypeWidget = $(SIGNUP.settings.modal_selector).find('[data-gro-widget="user-type"]');
    var r = userTypeWidget.find('input[name=user_type]');

    // if (r.val() !== 2)
    // {
    //     $(SIGNUP.settings.modal_selector).find('[data-gro-panel="nds"]').hide();
    // }

    r.on('change', function (e) {
        var $this = $(this);
        var type = $this.val();


        switch (type) {
            case '1':
                $(SIGNUP.settings.modal_selector).find('[data-gro-panel="nds"]').show();
                $('#agreement-document').empty();
                $('#agreement-document').append('РЎ СѓСЃР»РѕРІРёСЏРјРё <a href="/user-assets/files/oferta.pdf" target="_blank" id="oferta-link" target="_blank">РџСѓР±Р»РёС‡РЅРѕР№ РѕС„РµСЂС‚С‹</a> Рё <a href="/user-assets/files/groozgo_broker.pdf" target="_blank"  id="agreement-link" target="_blank">РћС„РµСЂС‚С‹ РґР»СЏ РіСЂСѓР·РѕРѕС‚РїСЂР°РІРёС‚РµР»РµР№</a>&nbsp;РѕР·РЅР°РєРѕРјР»РµРЅ');
                break;
            case '2':
                $(SIGNUP.settings.modal_selector).find('[data-gro-panel="nds"]').show();
                $('#agreement-document').empty();
                $('#agreement-document').append('CРѕРіР»Р°СЃРµРЅ СЃ СѓСЃР»РѕРІРёСЏРјРё <a href="/user-assets/files/oferta.pdf" target="_blank" id="oferta-link" target="_blank">РџСѓР±Р»РёС‡РЅРѕР№ РѕС„РµСЂС‚С‹</a> Рё <a href="/user-assets/files/groozgo_carrier.pdf" id="agreement-link" target="_blank">Р”РѕРіРѕРІРѕСЂР° РїРµСЂРµРІРѕР·РєРё РіСЂСѓР·РѕРІ</a>');
                break;
            default:
                break;
        }
    });


};

SIGNUP.stop = function () {
    closeModal($(SIGNUP.settings.modal_selector));
};

SIGNUP.setDefaultValue = function (def_value, form) {
    var fromInput = $('input[name="from_data"]', form);
    if (def_value && def_value !== "") {
        ymaps.geocode(def_value, {
            results:1,
            json:true,
            boundedBy: YMAPS_BOUND,
            strictBounds: true
        }).then(function (data) {

            //СЃС‡РµС‚С‡РёРє СЏРЅРґРµРєСЃ Р°РїРё
            if (typeof yandexMapCounterAdd === "function") {
                yandexMapCounterAdd(COORDS_BY_ADDRESS_JS);
            }

            var locationGeoObject = data.GeoObjectCollection.featureMember[0];
            if (locationGeoObject)
            {
                var kind = locationGeoObject.GeoObject.metaDataProperty.GeocoderMetaData.kind;
                var name;
                if (locationGeoObject.GeoObject.description != null) {
                    name = locationGeoObject.GeoObject.description + ", " + locationGeoObject.GeoObject.name;
                } else {
                    name = locationGeoObject.GeoObject.name;
                }
                fromInput.val(name);

                var coords = locationGeoObject.GeoObject.Point.pos;
                coords = coords.split(' ');
                var lng = coords[0];
                var lat = coords[1];

                $('[name="from_lat"]', form).val(lat);
                $('[name="from_lng"]', form).val(lng);
                $('[name="from_kind"]', form).val(kind);
            }


        });
    }

};


SIGNUP.onNextStep = function (current_step, btn_sel, refresh = 0) {

    var userTypeWidget = $(SIGNUP.settings.steps[current_step].form_sel).find('[data-gro-widget="user-type"]');

    //statistics
    if (gtag) {
        //console.log('gtag');
        if ($('#signup-wizard-3-comp').val() !== '') {
            gtag("event", "event_name", {"event_category": "registration", "event_action": "organisation_send"});
            //console.log('organisation_send');
        }
        if ($('#signup-wizard-3-name').val() !== '') {
            gtag("event", "event_name", {"event_category": "registration", "event_action": "name_send"});
            //console.log('name_send');
        }
        if ($('input[name="usurname"]').val() !== '') {
            gtag("event", "event_name", {"event_category": "registration", "event_action": "surname_send"});
            //console.log('surname_send');
        }
        if ($('input[name="uemail"]').val() !== '') {
            gtag("event", "event_name", {"event_category": "registration", "event_action": "email_send"});
            //console.log('email_send');
        }
        if ($('#signup-wizard-3-pass').val() !== '') {
            gtag("event", "event_name", {"event_category": "registration", "event_action": "password_send"});
            //console.log('password_send');
        }
        if ($('.checkbox_has_link').hasClass('checkbox_checke')) {
            gtag("event", "event_name", {"event_category": "registration", "event_action": "acception_send"});
            //console.log('acception_send');
        }
    }

    // РќР° СЃС‚СЂР°РЅРёС†Рµ РЅРѕРІРѕРіРѕ Р·Р°РєР°Р·Р° Рё РІР·СЏС‚СЊ Р·Р°РєР°Р· - РїСЂРѕРїРёСЃР°РЅ С‚РёРї РїРѕР»СЊР·РѕРІР°С‚РµР»СЏ, СЃСЂР°Р·Сѓ РµРіРѕ Рё СѓСЃС‚Р°РЅРѕРІРёС‚СЊ
    // Рё РїРѕРєР°Р·Р°С‚СЊ РЅСѓР¶РЅСѓСЋ РїР°РЅРµР»СЊРєСѓ СЃ РґРѕРїРѕР»РЅРёС‚РµР»СЊРЅС‹РјРё Р°С‚СЂРёР±СѓС‚Р°РјРё
    if (SIGNUP.settings.user_type != '') {
        type = SIGNUP.settings.user_type;
        switch (type) {
            case 1:
            case '1':
                $(SIGNUP.settings.modal_selector).find('[data-gro-panel="nds"]').show();
                $('#agreement-document').empty();
                $('#agreement-document').append('РЎ СѓСЃР»РѕРІРёСЏРјРё <a href="/user-assets/files/oferta.pdf" target="_blank" id="oferta-link" target="_blank">РџСѓР±Р»РёС‡РЅРѕР№ РѕС„РµСЂС‚С‹</a> Рё <a href="/user-assets/files/groozgo_broker.pdf" target="_blank"  id="agreement-link" target="_blank">РћС„РµСЂС‚С‹ РґР»СЏ РіСЂСѓР·РѕРѕС‚РїСЂР°РІРёС‚РµР»РµР№</a>&nbsp;РѕР·РЅР°РєРѕРјР»РµРЅ');
                break;
            case 2:
            case '2':
                $(SIGNUP.settings.modal_selector).find('[data-gro-panel="nds"]').show();
                $('#agreement-document').empty();
                $('#agreement-document').append('CРѕРіР»Р°СЃРµРЅ СЃ СѓСЃР»РѕРІРёСЏРјРё <a href="/user-assets/files/oferta.pdf" target="_blank" id="oferta-link" target="_blank">РџСѓР±Р»РёС‡РЅРѕР№ РѕС„РµСЂС‚С‹</a> Рё <a href="/user-assets/files/groozgo_carrier.pdf" id="agreement-link" target="_blank">Р”РѕРіРѕРІРѕСЂР° РїРµСЂРµРІРѕР·РєРё РіСЂСѓР·РѕРІ</a> Рё РіРѕС‚РѕРІ РїРѕРґРїРёСЃР°С‚СЊ Рё РІС‹СЃР»Р°С‚СЊ СЃРєР°РЅС‹ РґРѕРіРѕРІРѕСЂР° РЅР° info@groozgo.ru');
                break;
            default:
                break;
        }
        userTypeWidget.hide();
        var $user_type_radio = userTypeWidget.find('input:radio[name=user_type]');
        $user_type_radio.filter('[value=' + type + ']').prop('checked', true);
        //userTypeWidget.parent().find('[data-gro-panel]').hide();
        userTypeWidget.parent().find('[data-gro-panel=' + type + ']').show();
    }

    var step = SIGNUP.settings.steps[current_step];
    // СЃРѕР±СЂР°С‚СЊ РїРѕР»СЏ РёР· С„РѕСЂРјС‹ С€Р°РіР°
    var form = $(btn_sel).parents(step.form_sel);

    var inputs = form.serializeArray();
    inputs.push({'name': 'step_index', 'value': current_step});
    inputs.push({'name': 'pathname', 'value': window.location.pathname});
    inputs.push({'name': 'is_fast_signup', 'value': SIGNUP.settings.is_fast_signup});
    inputs.push({'name': 'refresh_code', 'value': refresh});

    //СЃРѕР±РёСЂР°РµРј С‡РµРєР±РѕРєСЃС‹, РµСЃР»Рё РѕРЅРё РµСЃС‚СЊ РІ С€Р°РіРµ

    $('input.check_data', $(step.form_sel)).each(function () {
        let name = $(this).attr('name');
        let value = $(this).attr('value');
        if ($(this).attr('checked') == "checked") {
            inputs.push({'name': name, 'value': value});
        } else inputs.push({'name': name, 'value': 0});
    });

    if (SIGNUP.settings.user_type)
    {
        inputs.push({'name': 'user_type', 'value': SIGNUP.settings.user_type});

    }

    var pathname = window.location.pathname;
    var url = (pathname.substring(0,7) == '/invite') ? pathname : '/signup';
    // РѕС‚РїСЂР°РІРёС‚СЊ РёС… ajax-РѕРј.
    loader();
    $.ajax({
        type: "POST",
        url: url,
        data: inputs,
        dataType: "json",
        success: function (data) {
            // РЈРґР°Р»РёС‚СЊ РѕС€РёР±РєРё СЃ С„РѕСЂРјС‹ РµСЃР»Рё Р±С‹Р»Рё Рё СЃС‚СЂР°РЅРёС†Р° РЅРµ РїРµСЂРµР·Р°РіСЂСѓР¶Р°Р»Р°СЃСЊ
            clearModalErrors(form);

            if (data.result == 'success') {
                const USER = data.id;
                current_step = data.next_step - 1;
                // РЅР° РІС‚РѕСЂРѕРј С€Р°РіРµ РїРѕР»Рµ С‚РµР»РµС„РѕРЅР° - СЃС‚Р°С‚РёС‡РЅС‹Р№ С‚РµРєСЃС‚. РЈСЃС‚Р°РЅРѕРІРёС‚СЊ РµРіРѕ РёР· РѕС‚РІРµС‚Р° РѕС‚ СЃРµСЂРІРµСЂР°.
                var phone = data.phone;
                if (current_step == 1 && phone != '') {
                    $(SIGNUP.settings.steps[1].phone_sel).html(data.phone_display);
                    $(SIGNUP.settings.steps[1].phone_input).val(data.phone);
                }
                if (current_step == 2 && phone != '') {
                    $(SIGNUP.settings.steps[2].phone_sel).html(data.phone_display);
                    $(SIGNUP.settings.steps[2].phone_input).val(data.phone);
                }
                if (current_step == 1) {
                    $(SIGNUP.settings.steps[1].reset_btn).addClass('hide');
                    $(SIGNUP.settings.steps[1].back_btn).addClass('hide');
                    $(SIGNUP.settings.steps[1].next_btn_sel).removeClass('hide');
                    $(SIGNUP.settings.steps[1].code_input).val('');
                }
                // if (current_step == 1)
                // {
                //     stepsInit({key:0,value:1}, true);
                // }

                // РџРµСЂРµС…РѕРґ РЅР° СЃР»РµРґСѓСЋС‰РёР№ С€Р°Рі РјР°СЃС‚РµСЂР°
                // SIGNUP.activate_step(current_step + 1, data);
                SIGNUP.activate_step(current_step, data);

                // РЎРєСЂС‹С‚РёРµ РєРЅРѕРїРѕРє РІС‹Р±РѕСЂР° СЃС‚СЂР°РЅС‹ СЂРµРіРёСЃС‚СЂР°С†РёРё
                $('#foreign-choose-btn').addClass('hidden');

            }
            else if (data.result == 'error') {
                // РџРѕРєР°Р·Р°С‚СЊ РѕС€РёР±РєРё
                AUTH.display_form_errors(data.response, form);
                if (current_step == 1) {
                    $(SIGNUP.settings.steps[1].reset_btn).removeClass('hide');
                    $(SIGNUP.settings.steps[1].back_btn).removeClass('hide');
                    $(SIGNUP.settings.steps[1].next_btn_sel).addClass('hide');
                    $(SIGNUP.settings.steps[1].reset_btn).off('click');
                    $(SIGNUP.settings.steps[1].reset_btn).on('click', function (e) {
                        e.preventDefault();
                        SIGNUP.onNextStep(1, $(this), 1);
                    });
                    $(SIGNUP.settings.steps[1].back_btn).off('click');
                    $(SIGNUP.settings.steps[1].back_btn).on('click', function (e) {
                        e.preventDefault();
                        SIGNUP.start();
                    });
                }
            }
            else if (data.result == 'signup_success') {
                // РјРµС‚СЂРёРєР°
                if (SIGNUP.settings.user_type)
                {
                    var type = SIGNUP.settings.user_type;
                }
                else {
                    var userTypeWidget = $(SIGNUP.settings.modal_selector).find('[data-gro-widget="user-type"]');
                    var type = userTypeWidget.find('input:radio[name=user_type]:checked').val();
                }


                if (type == '1')
                {
                    var client = "BROKER";
                }
                else {
                    var client = "CARRIER";
                }
                if (gtag){
                    gtag("event" , "event_name", { "event_category": "registration", "event_action": "send", "event_label": client});
                }

                // РџРѕР»СЊР·РѕРІР°С‚РµР»СЊ РїСЂРѕС€С‘Р» СѓСЃРїРµС€РЅРѕ РІСЃРµ С€Р°РіРё СЂРµРіРёСЃС‚СЂР°С†РёРё.
                // РўРµРїРµСЂСЊ РѕРЅ Р°РІС‚РѕСЂРёР·РѕРІР°РЅ РІ СЃРёСЃС‚РµРјРµ, РЅСѓР¶РЅРѕ Р·Р°РєСЂС‹С‚СЊ РґРёР°Р»РѕРі
                SIGNUP.signup_success(data);
            }
        }
    });

    // РѕРїС†РёРѕРЅР°Р»СЊРЅРѕ:
    // 1. РїРѕРґСЃРІРµС‚РёС‚СЊ РїРѕР»СЏ
    // 2. РїРѕРєР°Р·Р°С‚СЊ loader-РєСЂСѓС‚РёР»РєСѓ
};

SIGNUP.form_empty_all = function () {
    for (var i = 0; i < SIGNUP.settings.steps.length; i++) {
        clearModalErrors($(SIGNUP.settings.steps[i].form_sel));
        $(SIGNUP.settings.steps[i].form_sel).find('input[name="phone"]').val('');
    }
};


SIGNUP.activate_step = function (step_idx, data) {
    if (step_idx == 0)
    {
        SIGNUP.form_empty_all();
    }
    if (step_idx == 4) {
        var target = $('#company_link_list');
        AUTH.showCompanies(target,data.company_list,true);
        $('#signup-wizard-5-comp').on('keyup', function () {
            clearModalErrors($(SIGNUP.settings.steps[4].form_sel));
        })
    }
    if (step_idx == 4 || step_idx == 3) {
        let form = $(SIGNUP.settings.steps[step_idx].form_sel);
        let target_block = $('.carrier_addon', form);
        $('input[name="user_type"]', form).on('change', function () {
            let type = $('input:radio[name=user_type]:checked', form).val();
            if (type == 2) {
                target_block.removeClass('hide');
            } else target_block.addClass('hide');
        });
    }
    if (step_idx > 1) {
        if (data.default_region != undefined) {
            let form = $(SIGNUP.settings.steps[step_idx].form_sel);
            SIGNUP.setDefaultValue(data.default_region, form);
        }
    }

    for (var i = 0; i < SIGNUP.settings.steps.length; i++) {
        var step = SIGNUP.settings.steps[i];
        if (i == step_idx) {
            $(step.next_btn_sel).off('click');
            $(step.next_btn_sel).on('click', function (e) {
                e.preventDefault();
                SIGNUP.onNextStep(step_idx, $(this));
            });
            $(step.content_sel).show();
        } else {
            $(step.next_btn_sel).off('click');
            $(step.content_sel).hide();
        }
    }
};

SIGNUP.cleanAutocomplete = function(selector) { //Р§РёСЃС‚РёРј СЃРєСЂС‹С‚С‹Рµ РїРѕР»СЏ РїСЂРё РІРІРѕРґРµ
    $(selector).on('keyup', function () {
        var self = $(this).parent('.geocode_block');
        $('[data_type="lat"]', self).val('');
        $('[data_type="lng"]', self).val('');
        $('[data_type="kind"]', self).val('');
    });
};

SIGNUP.activateAutocomplete = function(selector) {
    $(selector).autocomplete({
        source: function (req, response) {
            var res;
            // clearFieldErrors();
            ymaps.geocode(req.term, {
                results:5,
                json:true,
                boundedBy: YMAPS_BOUND,
                strictBounds: true
            }).then(function (data) {

                //СЃС‡РµС‚С‡РёРє СЏРЅРґРµРєСЃ Р°РїРё
                if (typeof yandexMapCounterAdd === "function") {
                    yandexMapCounterAdd(COORDS_BY_ADDRESS_JS);
                }

                res = SIGNUP.address_convert(data);
                response(res);
            });
        },
        select: function (event, ui) {
            var self = $(this).parent('.geocode_block');
            //clearFieldErrors();
            ymaps.geocode(ui.item.value, {kind:'locality'}).then(function (location_geocode_result) {

                //СЃС‡РµС‚С‡РёРє СЏРЅРґРµРєСЃ Р°РїРё
                if (typeof yandexMapCounterAdd === "function") {
                    yandexMapCounterAdd(COORDS_BY_ADDRESS_JS);
                }

                var locationGeoObject = location_geocode_result.geoObjects.get(0);

                var kind = locationGeoObject.properties.get('metaDataProperty.GeocoderMetaData.kind')
                $(selector).attr("kind", kind);

                //checkKind("#" + self.attr('id'), kind);

                var coords = locationGeoObject.geometry.getCoordinates();
                var lng = coords[1];
                var lat = coords[0];

                $('[data_type="lat"]', self).val(lat);
                $('[data_type="lng"]', self).val(lng);
                $('[data_type="kind"]', self).val(kind);

            });
        },
        minLength: 3
    });
};

SIGNUP.address_convert = function(data) {
    var res = [];
    data.GeoObjectCollection.featureMember.forEach(function (item, i, arr) {
        if (item.GeoObject.description != null) {
            res.push(item.GeoObject.description + ", " + item.GeoObject.name);
        } else {
            res.push(item.GeoObject.name);
        }
    });
    return res;
}


SIGNUP.signup_success = function (data) {
    var userTypeWidget = $(SIGNUP.settings.modal_selector).find('[data-gro-widget="user-type"]');
    var type = userTypeWidget.find('input:radio[name=user_type]:checked').val();

    closeModal($(SIGNUP.settings.modal_selector));

    var title_sign ="Р’С‹ СѓСЃРїРµС€РЅРѕ Р·Р°СЂРµРіРёСЃС‚СЂРёСЂРѕРІР°Р»РёСЃСЊ!";
    var img = "<img src='/images/like.png' class='register-img'/>";

    if (!SIGNUP.settings.is_fast_signup) {

        if (SIGNUP.settings.user_type == 1)
        {
            var msg = "РњС‹ РѕС‚РїСЂР°РІРёР»Рё РЅР° РјРµР№Р» РїСЂРѕСЃС‚С‹Рµ РёРЅСЃС‚СЂСѓРєС†РёРё РїРѕ СЂР°Р±РѕС‚Рµ РІ СЃРµСЂРІРёСЃРµ, Р° С‚Р°РєР¶Рµ РґРѕРіРѕРІРѕСЂ РѕРєР°Р·Р°РЅРёСЏ С‚СЂР°РЅСЃРїРѕСЂС‚РЅРѕ-СЌРєСЃРїРµРґРёС†РёРѕРЅРЅС‹С… СѓСЃР»СѓРі." +
                " Р”РѕСЃС‚Р°РІРєР° РїРёСЃСЊРјР° РјРѕР¶РµС‚ Р·Р°РЅСЏС‚СЊ РЅРµСЃРєРѕР»СЊРєРѕ РјРёРЅСѓС‚, РїСЂРѕРІРµСЂСЊС‚Рµ РїР°РїРєСѓ \"РЎРїР°Рј\"."
        }
        else
        {
            var msg = "РњС‹ РѕС‚РїСЂР°РІРёР»Рё РЅР° РјРµР№Р» РїСЂРѕСЃС‚С‹Рµ РёРЅСЃС‚СЂСѓРєС†РёРё РїРѕ СЂР°Р±РѕС‚Рµ РІ СЃРµСЂРІРёСЃРµ." +
                " Р”РѕСЃС‚Р°РІРєР° РїРёСЃСЊРјР° РјРѕР¶РµС‚ Р·Р°РЅСЏС‚СЊ РЅРµСЃРєРѕР»СЊРєРѕ РјРёРЅСѓС‚, РїСЂРѕРІРµСЂСЊС‚Рµ РїР°РїРєСѓ \"РЎРїР°Рј\"."
        }
        var url = (data.url != '') ? data.url : "/";

        var msg_sign = img + msg + "<div class='modal__buttons'><a class='button button_view_action button_size_l' id='signup-lk-link' href='"+url+"'> РџРµСЂРµР№С‚Рё РІ Р»РёС‡РЅС‹Р№ РєР°Р±РёРЅРµС‚</a></div>";
        showMessage(title_sign, msg_sign);
        setTimeout(function () {
            window.location.href = url;
        }, 5000);
    }
    if (data.submit) {
        if (SIGNUP.settings.is_fast_signup && SIGNUP.settings.user_type == 1) {
            var type = (data.type > 0) ? data.type : 1;
            $("#order-form-management").attr("is-auth", type);
            $(".link__auth").remove();
            var msg = "РњС‹ РѕС‚РїСЂР°РІРёР»Рё РЅР° РјРµР№Р» РїСЂРѕСЃС‚С‹Рµ РёРЅСЃС‚СЂСѓРєС†РёРё РїРѕ СЂР°Р±РѕС‚Рµ РІ СЃРµСЂРІРёСЃРµ, Р° С‚Р°РєР¶Рµ РґРѕРіРѕРІРѕСЂ РѕРєР°Р·Р°РЅРёСЏ С‚СЂР°РЅСЃРїРѕСЂС‚РЅРѕ-СЌРєСЃРїРµРґРёС†РёРѕРЅРЅС‹С… СѓСЃР»СѓРі." +
                " Р”РѕСЃС‚Р°РІРєР° РїРёСЃСЊРјР° РјРѕР¶РµС‚ Р·Р°РЅСЏС‚СЊ РЅРµСЃРєРѕР»СЊРєРѕ РјРёРЅСѓС‚, РїСЂРѕРІРµСЂСЊС‚Рµ РїР°РїРєСѓ \"РЎРїР°Рј\". ";
            msg = msg + "<br><br> Р’С‹ СѓСЃРїРµС€РЅРѕ Р°РІС‚РѕСЂРёР·РёСЂРѕРІР°РЅС‹ Рё Р±СѓРґРµС‚Рµ РїРµСЂРµРЅР°РїСЂР°РІР»РµРЅС‹ Р°РІС‚РѕРјР°С‚РёС‡РµСЃРєРё!";
            var msg_sign = img + msg + "<div class='modal__buttons'></div>";
            //$('#save_order_btn_auth').addClass('validate-step-2');
            showMessage(title_sign, msg_sign);
            setTimeout(function () {
                $('.validate-form').trigger('click');
            }, 5000);

        }
        else {
            var url = (data.url != '') ? data.url : "/";
            var msg = "РњС‹ РѕС‚РїСЂР°РІРёР»Рё РЅР° РјРµР№Р» РїСЂРѕСЃС‚С‹Рµ РёРЅСЃС‚СЂСѓРєС†РёРё РїРѕ СЂР°Р±РѕС‚Рµ РІ СЃРµСЂРІРёСЃРµ." +
                " Р”РѕСЃС‚Р°РІРєР° РїРёСЃСЊРјР° РјРѕР¶РµС‚ Р·Р°РЅСЏС‚СЊ РЅРµСЃРєРѕР»СЊРєРѕ РјРёРЅСѓС‚, РїСЂРѕРІРµСЂСЊС‚Рµ РїР°РїРєСѓ \"РЎРїР°Рј\"."
            var msg_sign = img + msg + "<div class='modal__buttons'><a class='button button_view_action button_size_l' id='signup-lk-link' href='"+url+"'> РџРµСЂРµР№С‚Рё РІ Р»РёС‡РЅС‹Р№ РєР°Р±РёРЅРµС‚</a></div>";
            showMessage(title_sign, msg_sign);
        }
    }
    //AUTH.action_success(data);
};

var RECOVERY = RECOVERY || {};

// СЃРµР»РµРєС‚РѕСЂ РґРёР°Р»РѕРіР° СЃ РјР°СЃС‚РµСЂРѕРј Рё СЃРµР»РµРєС‚РѕСЂС‹ РґР»СЏ С€Р°РіРѕРІ
// РґР»СЏ РєР°Р¶РґРѕРіРѕ С€Р°РіР° РЅСѓР¶РµРЅ СЃРµР»РµРєС‚РѕСЂ РєРѕРЅС‚РµРЅС‚Р° (Р±Р»РѕРє РІРЅСѓС‚СЂРё РјРѕРґР°Р»СЊРЅРѕРіРѕ РґРёР°Р»РѕРіР°)
// СЃРµР»РµРєС‚РѕСЂ С„РѕСЂРјС‹ РґР»СЏ СЃР±РѕСЂР° РїРѕР»РµР№ РІРІРѕРґР°
// СЃРµР»РµРєС‚РѕСЂ РєРЅРѕРїРєРё РґР»СЏ СѓСЃС‚Р°РЅРѕРІРєРё РѕР±СЂР°Р±РѕС‚С‡РёРєР° РЅР°Р¶Р°С‚РёСЏ (Р”Р°Р»РµРµ)
RECOVERY.settings = {
    'modal_selector': '.recovery-wizard',
    'steps': [
        {
            'content_sel': '.recovery-wizard-content-1',
            'form_sel': '.recovery-wizard-form-1',
            'next_btn_sel': '.recovery-wizard-next-btn-1'
        },
        {
            'content_sel': '.recovery-wizard-content-2',
            'form_sel': '.recovery-wizard-form-2',
            'next_btn_sel': '.recovery-wizard-next-btn-2',
            'phone_sel': '.recovery-wizard-2-phone' // СЃСЋРґР° РґРѕР±Р°РІРёС‚СЃСЏ С‚РµР»РµС„РѕРЅ РЅР° РІС‚РѕСЂРѕРј С€Р°РіРµ
        },
        {
            'content_sel': '.recovery-wizard-content-3',
            'form_sel': '.recovery-wizard-form-3',
            'next_btn_sel': '.recovery-wizard-next-btn-3',
            'company_cel' : '#recovery_companies'
        }
    ]
};

RECOVERY.start = function () {
    // РІРѕ РІСЃРµС… РїРѕР»СЏС… СѓСЃС‚Р°РЅРѕРІРёС‚СЊ РїСѓСЃС‚С‹Рµ Р·РЅР°С‡РµРЅРёСЏ
    RECOVERY.form_empty_all();
    // РїРѕРєР°Р·Р°С‚СЊ РїРµСЂРІС‹Р№ С€Р°Рі
    RECOVERY.activate_step(0);

};

RECOVERY.stop = function () {
    closeModal($(RECOVERY.settings.modal_selector));
};

RECOVERY.onNextStep = function (current_step, btn_sel) {
    var step = RECOVERY.settings.steps[current_step];
    // СЃРѕР±СЂР°С‚СЊ РїРѕР»СЏ РёР· С„РѕСЂРјС‹ С€Р°РіР°
    var form = $(btn_sel).parents(step.form_sel);

    var inputs = form.serializeArray();

    inputs.push({'name': 'step_index', 'value': current_step});
    inputs.push({'name': 'pathname', 'value': window.location.pathname});
    // РѕС‚РїСЂР°РІРёС‚СЊ РёС… ajax-РѕРј.
    $.ajax({
        type: "POST",
        url: "/recovery",
        data: inputs,
        dataType: "json",
        success: function (data) {

            // РЈРґР°Р»СЏРµРј РѕС€РёР±РєРё РµСЃР»Рё Р±С‹Р»Рё
            clearModalErrors(form);

            if (data.result == 'success') {
                const USER = data.id;
                // Р”РµР»Р°РµРј С‡С‚Рѕ-С‚Рѕ РїРѕР»РµР·РЅРѕРµ
                var phone = data.phone;
                if (current_step + 1 == 1 && phone != '') {
                    $(RECOVERY.settings.steps[1].phone_sel).text(data['phone_display']);
                }

                if (current_step == 1)
                {
                    stepsInit({key:0,value:1});
                }

                RECOVERY.activate_step(current_step + 1, data);
            } else if (data.result == 'continue') {
                $(step.form_sel).addClass('hide');
                var target = $(step.company_cel);
                AUTH.showCompanies(target,data.company_list,false);
                $('<div class="modal__buttons"><a class="button button_size_l" href="/logout"> РћС‚РјРµРЅРёС‚СЊ</a></div>').appendTo(target);

            }
            else if (data.result == 'badstep') {
                RECOVERY.stop();
            }
            else if (data.result == 'error') {
                // РџРѕРєР°Р·С‹РІР°РµРј РѕС€РёР±РєРё
                AUTH.display_form_errors(data['response'], form);
            }
            else if (data.result == 'recovery_success') {
                // РџРѕР»СЊР·РѕРІР°С‚РµР»СЊ РїСЂРѕС€С‘Р» СѓСЃРїРµС€РЅРѕ РІСЃРµ С€Р°РіРё СЂРµРіРёСЃС‚СЂР°С†РёРё.
                // РўРµРїРµСЂСЊ РѕРЅ Р°РІС‚РѕСЂРёР·РѕРІР°РЅ РІ СЃРёСЃС‚РµРјРµ, РЅСѓР¶РЅРѕ Р·Р°РєСЂС‹С‚СЊ РґРёР°Р»РѕРі
                RECOVERY.recovery_success(data);
            }
        }
    });
};

RECOVERY.form_empty_all = function () {
    for (var i = 0; i < RECOVERY.settings.steps.length; i++) {
        clearModalErrors($(RECOVERY.settings.steps[i].form_sel));
    }
};


RECOVERY.activate_step = function (step_idx, data) {
    for (var i = 0; i < RECOVERY.settings.steps.length; i++) {
        var step = RECOVERY.settings.steps[i];
        if (i == step_idx) {
            //RECOVERY.activate_step(i);
            if (data) {
                // Р·Р°РїРѕР»РЅРёС‚СЊ РїРѕР»СЏ С„РѕСЂРјС‹ РёР· data

            }
            $(step.next_btn_sel).off('click');
            $(step.next_btn_sel).on('click', function (e) {
                e.preventDefault();
                RECOVERY.onNextStep(step_idx, $(this));
            });
            $(step.content_sel).show();
            if (i==2) {
                $(step.company_cel).empty();
                $(step.form_sel).removeClass('hide');
            }
        } else {
            $(step.next_btn_sel).off('click');
            $(step.content_sel).hide();
        }
    }
};



RECOVERY.recovery_success = function (data) {
    closeModal($(RECOVERY.settings.modal_selector));
    // TODO РµСЃР»Рё РЅР° С„РѕСЂРјРµ РЅРѕРІРѕРіРѕ Р·Р°РєР°Р·Р°, С‚Рѕ РѕС‚РѕСЃР»Р°С‚СЊ РµС‘ РЅР° СЃРµСЂРІРµСЂ, С‡С‚РѕР±С‹ РїРѕРєР°Р·Р°С‚СЊ РѕС€РёР±РєРё Р·Р°РїРѕР»РЅРµРЅРёСЏ.
    AUTH.action_success(data)
};


// LGINN

var LOGIN = LOGIN || {};

LOGIN.settings = {
    'modal_sel': '.modal__auth',
    'form_sel': '.login_form',
    'btn_sel': '.btn_login_send',
    'company_cel' : '#login_companies'
};

LOGIN.start = function () {
    // РІРѕ РІСЃРµС… РїРѕР»СЏС… СѓСЃС‚Р°РЅРѕРІРёС‚СЊ РїСѓСЃС‚С‹Рµ Р·РЅР°С‡РµРЅРёСЏ
    //AUTH.resetForm($(LOGIN.settings.form_sel));


    $(LOGIN.settings.btn_sel).off("click");
    $(LOGIN.settings.btn_sel).on("click", function (e) {
        e.preventDefault();
        LOGIN.doLogin($(this));
    });
    $(LOGIN.settings.company_cel).empty();
    $(LOGIN.settings.form_sel).removeClass('hide');

};
LOGIN.stop = function () {
    closeModal($(LOGIN.settings.modal_sel));
};

LOGIN.doLogin = function (btn_sel) {
    // СЃРѕР±СЂР°С‚СЊ РїРѕР»СЏ РёР· С„РѕСЂРјС‹ Р»РѕРіРёРЅР°
    var form = $(btn_sel).parents(LOGIN.settings.form_sel);
    var inputs = form.serializeArray();
    //inputs.push({'name':'pathname', 'value':window.location.pathname});
    // РѕС‚РїСЂР°РІРёС‚СЊ РёС… ajax-РѕРј.
    loader();
    $.ajax({
        type: "POST",
        url: "/login",
        data: inputs,
        dataType: "json",
        success: function (data) {
            //var form = $(step.form_sel);
            // РЈРґР°Р»СЏРµРј РѕС€РёР±РєРё РµСЃР»Рё Р±С‹Р»Рё
            clearModalErrors(form);

            if (data.result == 'success') {
                if (gtag)
                {
                    if (data.user != undefined) {
                        gtag("event", "authorization", {'uid': data.user});
                    }
                    else{
                        gtag("event", "event_name", {"event_category": "authorization", "event_action": "send"});
                    }
                }

                // Р›РѕРіРёРЅ СѓСЃРїРµС€РµРЅ
                LOGIN.login_success(data);
            } else if (data.result == 'continue') {
                // window.location.href = '/company';
                $(LOGIN.settings.form_sel).addClass('hide');
                var target = $(LOGIN.settings.company_cel);
                AUTH.showCompanies(target,data.company_list,false);
                $('<div class="modal__buttons"><a class="button button_view_action button_size_l" href="/logout"> РћС‚РјРµРЅРёС‚СЊ</a></div>').appendTo(target);

            }
            else if (data.result == 'error') {
                // РџРѕРєР°Р·С‹РІР°РµРј РѕС€РёР±РєРё
                AUTH.display_form_errors(data.response, form);
            }
        }
    });
};

LOGIN.login_success = function (data) {

    closeModal(LOGIN.settings.modal_sel);
    // redirect or submit
    AUTH.action_success(data);
};


var AUTH = AUTH || {};
AUTH.settings = {
    'redirect_to_url': ''
};

AUTH.showCompanies = function (target, company_list, next) {
    var company, view_company, message_company, message_next;
    target.empty();
    if (company_list != undefined && company_list.length > 0) {
        message_company = company_list.length == 1 ?
            'Р’С‹ СѓР¶Рµ Р·Р°СЂРµРіРёСЃС‚СЂРёСЂРѕРІР°РЅС‹ РІ РєРѕРјРїР°РЅРёРё' :
            'Р’С‹ Р·Р°СЂРµРіРёСЃС‚СЂРёСЂРѕРІР°РЅС‹ РІ СЃР»РµРґСѓСЋС‰РёС… РєРѕРјРїР°РЅРёСЏС…:';
        message_next = company_list.length == 1 ?
            'РњРѕР¶РµС‚Рµ Р°РІС‚РѕСЂРёР·РѕРІР°С‚СЊСЃСЏ РёР»Рё РІС‹Р±СЂР°С‚СЊ РЅРѕРІСѓСЋ РєРѕРјРїР°РЅРёСЋ' :
            'РњРѕР¶РµС‚Рµ Р°РІС‚РѕСЂРёР·РѕРІР°С‚СЊСЃСЏ РїРѕРґ РѕРґРЅРѕР№ РёР· РЅРёС… РёР»Рё РІС‹Р±СЂР°С‚СЊ РЅРѕРІСѓСЋ РєРѕРјРїР°РЅРёСЋ';
        $('<p>'+message_company+'</p>').appendTo(target);
        for (var j in company_list) {
            company = company_list[j];
            switch (company.status) {
                case 0:
                    view_company = $('<p class="btn-inactive"><b>'+company.name+'</b><br>'+company.type+'<br> Р—Р°Р±Р»РѕРєРёСЂРѕРІР°РЅР° </p>');
                    break;
                case 1:
                    view_company = $('<a class="btn-active-company" href="/auth/company?company_id='+company.id+'"><b>'+company.name+'</b><br>'+company.type+'<br></a>');
                    break;
                case 2:
                    view_company = $('<p class="btn-active-company"><b>'+company.name+'</b><br>'+company.type+'<br> Р’Р°С€ Р°РєРєР°СѓРЅС‚ Р·Р°Р±Р»РѕРєРёСЂРѕРІР°РЅ </p>');
                    break;
                default: view_company = $('<p>'+company.name+'<br>'+company.type+'</p>');
            }
            view_company.appendTo(target)
        }
        if(next)
            $('<p>'+message_next+'</p>').appendTo(target);
    }
};

/*AUTH.startFastSignup = function () {
    SIGNUP.settings.is_fast_signup = true;
    var e = jQuery.Event('auth.signup.start');
    $(document).trigger(e);
    if (!e.isDefaultPrevented()) {
        //AUTH.switchTo(SIGNUP);
    }
    return false;
}*/

AUTH.switchTo = function (auth_module) {
    LOGIN.stop();
    SIGNUP.stop();
    RECOVERY.stop();
    if (auth_module){
        $('.modal__auth').addClass('active');
        auth_module.start();
    }

};

AUTH.init = function () {
    LOGIN.start();
    SIGNUP.start();
    RECOVERY.start();
};

AUTH.display_form_errors = function (errors, form) {

    if (errors.name === 'ADMIN_REGISTERED') {
        form.find('input[name="phone"]').parents('span').next('.help-block').html("Р’С‹ РЅРµ РјРѕР¶РµС‚Рµ Р·Р°СЂРµРіРёСЃС‚СЂРёСЂРѕРІР°С‚СЊСЃСЏ РїРѕРґ СЌС‚РёРј РЅРѕРјРµСЂРѕРј");
        /*console.log('sdcf');
        AUTH.switchTo(LOGIN);
        var phone = form[0][1]['value'];
        $('#login-wizard-phone').val(phone);
        $('#login-wizard-phone').before('<div class="error alert-danger">Р’С‹ СѓР¶Рµ Р·Р°СЂРµРіРёСЃС‚СЂРёСЂРѕРІР°РЅС‹, РІРІРµРґРёС‚Рµ РїР°СЂРѕР»СЊ РґР»СЏ РІС…РѕРґР° РІ СЃРёСЃС‚РµРјСѓ</div>');
    */
    }
    else {
        for (var k in errors) {
            //form.find('input[name=' + k + ']').parents('span').addClass('select__error');
            //form.find('select[name^=' + k + ']').parent('div').addClass('select__error');
            form.find('input[name=' + k + ']').last().parents('span').next('.help-block').html(errors[k]);
            form.find('span.help-block[name=' + k + ']').html(errors[k]);
            if (k == 'confirm'){
                $('#confirm_error1').html(errors[k]);
                $('#confirm_error2').html(errors[k]);
                //console.log(errors[k]);
            }

        }
    }
};

AUTH.action_success = function (data) {
    if (!SIGNUP.settings.is_fast_signup) {
        if (data.url != '') {
            window.location = data['url'];
        }
    }
    if (data.submit) {
        if (SIGNUP.settings.is_fast_signup && SIGNUP.settings.user_type == 1) {

            var type = (data.type > 0) ? data.type : 1;
            $("#order-form-management").attr("is-auth", type);
            $(".link__auth").remove();
            var title_sign ="Р’С‹ СѓСЃРїРµС€РЅРѕ Р°РІС‚РѕСЂРёР·РѕРІР°Р»РёСЃСЊ!";
            if (data.type == 1)
            {
                var msg_sign = "";

                // if ($('.steps__lines-item.validate-step-1').hasClass('active'))
                // {
                var msg_sign = "Р’С‹ СѓСЃРїРµС€РЅРѕ Р°РІС‚РѕСЂРёР·РёСЂРѕРІР°РЅС‹ Рё Р±СѓРґРµС‚Рµ РїРµСЂРµРЅР°РїСЂР°РІР»РµРЅС‹ Р°РІС‚РѕРјР°С‚РёС‡РµСЃРєРё!";
                // }

                msg_sign = msg_sign + "<div class='modal__buttons'></div>";
            }
            else
            {
                msg_sign = "Р’С‹ СѓСЃРїРµС€РЅРѕ Р°РІС‚РѕСЂРёР·РёСЂРѕРІР°РЅС‹ Рё Р±СѓРґРµС‚Рµ РїРµСЂРµРЅР°РїСЂР°РІР»РµРЅС‹ Р°РІС‚РѕРјР°С‚РёС‡РµСЃРєРё!";
                var msg_sign = "<div class='modal__buttons'><button class='button button_view_action button_size_l modal__close__btn'> OK </button></div>";
            }

            //$('#save_order_btn_auth').addClass('validate-step-2');

            if(data.fix === 1) {
                msg_sign = "Р’С‹ Р°РІС‚РѕСЂРёР·РёСЂРѕРІР°Р»РёСЃСЊ РїРѕРґ РїРѕР»СЊР·РѕРІР°С‚РµР»РµРј СЃ С„РёРєСЃРёСЂРѕРІР°РЅРЅС‹РјРё С‚Р°СЂРёС„Р°РјРё, С†РµРЅР° РІ Р·Р°СЏРІРєРµ Р±СѓРґРµС‚ РїРµСЂРµСЃС‡РёС‚Р°РЅР° Р°РІС‚РѕРјР°С‚РёС‡РµСЃРєРё СЃРѕРіР»Р°СЃРЅРѕ Р’Р°С€РёРј С‚Р°СЂРёС„Р°Рј!";
                $(document).trigger('order-calc-price');
            }

            setTimeout(function () {
                $('.validate-form').trigger('click');
            }, 2000);

            showMessage(title_sign, msg_sign);
        }
        else {
            if (data.url != '') {
                if(data.url != '/broker/order/index') window.location = data['url'];
            }
            else
            {
                window.location.reload();
            }
        }
    }
};

AUTH.resetForm = function (formEl) {
    var form = $(formEl);
    // РЎР±СЂРѕСЃРёС‚СЊ РІСЃРµ РїРѕР»СЏ РѕР±С‹С‡РЅС‹Рј РјРµС‚РѕРґРѕРј С„РѕСЂРјС‹
    form.get(0).reset();

    clearModalErrors(form);
};


jQuery(document).ready(function () {
    AUTH.init();

    $('.recovery-btn').click(function(e){
        e.preventDefault();
        $('.modal__auth .tabs__link').removeClass('active');
        $('.modal__auth .tabs__item').removeClass('active');
        $('.modal__auth .recovery_tab').addClass('active');
        $('.modal__auth .recovery_tab_content').addClass('active');
    });

    $("#signup-wizard-5-comp").css('display','inline-block');
    $("#signup-wizard-5-comp").suggestions({
        params: {
            status:  ["ACTIVE"]
        },
        serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
        token: window.dadataKey,
        type: "PARTY",
        count: 5,
        /* Р’С‹Р·С‹РІР°РµС‚СЃСЏ, РєРѕРіРґР° РїРѕР»СЊР·РѕРІР°С‚РµР»СЊ РІС‹Р±РёСЂР°РµС‚ РѕРґРЅСѓ РёР· РїРѕРґСЃРєР°Р·РѕРє */
        onSelect: function(suggestion) {
            //console.log(suggestion)
            $( "#company-alert" ).text('');
            $( "#company-alert" ).removeClass('alert alert-danger');
            $("#signup-wizard-5-inn").val(suggestion.data.inn);
            $("#signup-wizard-5-kpp").val( (suggestion.data.kpp) ?  suggestion.data.kpp : "");
            $("#signup-wizard-5-ogrn").val( (suggestion.data.ogrn) ?  suggestion.data.ogrn : "");
            $("#signup-wizard-5-org").val(suggestion.value);
            $("#signup-wizard-5-director").val( (suggestion.data.management) ? suggestion.data.management.name : "");
            $("#signup-wizard-5-address").val( (suggestion.data.address.value) ? suggestion.data.address.value : "");
            $("#signup-wizard-5-opf").val( (suggestion.data && suggestion.data.opf && suggestion.data.opf.short) ? suggestion.data.opf.short : "");
        },
        onSelectNothing: function() {
            $( "#company-alert" ).text('Рљ СЃРѕР¶Р°Р»РµРЅРёСЋ, РІР°С€Р° РѕСЂРіР°РЅРёР·Р°С†РёСЏ РЅРµ СЏРІР»СЏРµС‚СЃСЏ РґРµР№СЃС‚РІСѓСЋС‰РµР№. РћР±СЂР°С‚РёС‚РµСЃСЊ Рє РєРѕРјР°РЅРґРµ Groozgo, +7 (499) 229-46-31 Р·Р° РїРѕРјРѕС‰СЊСЋ.');
            $( "#company-alert" ).addClass('alert alert-danger');
        }
    });

    $("#signup-wizard-4-comp").css('display','inline-block');
    $("#signup-wizard-4-comp").suggestions({
        params: {
            status:  ["ACTIVE"]
        },
        serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
        token: window.dadataKey,
        type: "PARTY",
        count: 5,
        /* Р’С‹Р·С‹РІР°РµС‚СЃСЏ, РєРѕРіРґР° РїРѕР»СЊР·РѕРІР°С‚РµР»СЊ РІС‹Р±РёСЂР°РµС‚ РѕРґРЅСѓ РёР· РїРѕРґСЃРєР°Р·РѕРє */
        onSelect: function(suggestion) {
            //console.log(suggestion)
            $( "#company-alert" ).text('');
            $( "#company-alert" ).removeClass('alert alert-danger');
            $("#signup-wizard-4-inn").val(suggestion.data.inn);
            $("#signup-wizard-4-kpp").val( (suggestion.data.kpp) ?  suggestion.data.kpp : "");
            $("#signup-wizard-4-ogrn").val( (suggestion.data.ogrn) ?  suggestion.data.ogrn : "");
            $("#signup-wizard-4-org").val(suggestion.value);
            $("#signup-wizard-4-director").val( (suggestion.data.management) ? suggestion.data.management.name : "");
            $("#signup-wizard-4-address").val( (suggestion.data.address.value) ? suggestion.data.address.value : "");
            $("#signup-wizard-4-opf").val( (suggestion.data && suggestion.data.opf && suggestion.data.opf.short) ? suggestion.data.opf.short : "");
        },
        onSelectNothing: function() {
            $( "#company-alert" ).text('Рљ СЃРѕР¶Р°Р»РµРЅРёСЋ, РІР°С€Р° РѕСЂРіР°РЅРёР·Р°С†РёСЏ РЅРµ СЏРІР»СЏРµС‚СЃСЏ РґРµР№СЃС‚РІСѓСЋС‰РµР№. РћР±СЂР°С‚РёС‚РµСЃСЊ Рє РєРѕРјР°РЅРґРµ Groozgo, +7 (499) 229-46-31 Р·Р° РїРѕРјРѕС‰СЊСЋ.');
            $( "#company-alert" ).addClass('alert alert-danger');
        }
    });

    /**
     * FOREIGN SIGNUP
     */
    $('.btn_russian_signup').on('click', function () {
        $('.step_item.signup-wizard-content-1').removeClass('hidden');
        $('.step_item.foreign-div').addClass('hidden');
        $(this).addClass('button_view_action');
        $('.btn_foreign_signup').removeClass('button_view_action');
        $('.steps__lines.steps__lines_small').removeClass('hidden');
    });

    $('.btn_foreign_signup').on('click', function () {
        $('.step_item.signup-wizard-content-1').addClass('hidden');
        $('.step_item.foreign-div').removeClass('hidden');
        $(this).addClass('button_view_action');
        $('.btn_russian_signup').removeClass('button_view_action');
        $('.steps__lines.steps__lines_small').addClass('hidden');
    });

    $('#country_list').on('keyup', function () {
        let self = $(this);
        self.autocomplete({
            source: function(request, response){
                $.ajax({
                    url:'/system/country-autocomplete',
                    data:{
                        limit: 10,
                        string: self.val()
                    },
                    success: function(data){
                        let source = JSON.parse(data);
                        let search_result = [];
                        $.each(source, function (index, value) {
                            search_result.push({
                                label: value,
                                value: value,
                            })
                        });
                        setTimeout(function () {
                        }, 500);
                        response(search_result);
                    },
                });
            },

        });

    });

    function formErrors(errors, form) {
        for (let k in errors) {
            form.find('input[name=' + k + ']').last().parents('span').next('.help-block').html(errors[k]);
        }
    }

    $('#foreign-signup-send').on('click', function () {
        let form = $('#foreign-signup-form');
        $.ajax({
            type: "POST",
            url: "/foreign-signup",
            data: form.serialize(),
            dataType: "json",
            success: function (data) {
                clearModalErrors(form);
                if (data.result === 'error') {
                    formErrors(data.response, form);
                } else {
                    form.addClass('hidden');
                    $('#foreign-choose-btn').addClass('hidden');
                    $('#foreign-signup-step2').removeClass('hidden');
                }
            }
        });
    });

    /***/

});
