const COORDS_BY_ADDRESS_JS = 1;
const ADDRESS_BY_COORDS_JS = 2;
const ADD_POINT_TO_MAP_JS = 3;
const BUILD_ROUTE_JS = 4;
const COORDS_BY_ADDRESS_HTTP = 5;
const ADDRESS_BY_COORDS_HTTP = 6;
const POINT_TRACKING_JS = 7;
const ADDRESS_SUGGEST = 8;

function yandexMapCounterAdd(category) {
    $.ajax({
        url: '/system/yandexcount',
        dataType: 'json',
        method: 'GET',
        data: {categ: category},
    });
}
