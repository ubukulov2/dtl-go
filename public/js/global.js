$('body').on('focus keyup focusout change', '.hascontent-label .form-control', function(e){
    determInput(this);
});

var YMAPS_BOUND = [[41.105996, 25.046698], [73.707848, 132.274515]];

function determInput(elem)
{
    let control_label = $(elem).parents('div.form-group').find('.control-label');
    setTimeout(function () {
        if ($(elem).val() !== "" && ($(elem).val() !== null && $(elem).val().length > 0)) {
            control_label.show();
        } else {
            control_label.hide();
        }
    }, 150);

}

//Р’РєР»СЋС‡РµРЅРёРµ('show') Рё РІС‹РєР»СЋС‡РµРЅРёРµ('hide') Р»РѕР°РґРµСЂР°
function pre_loader(trigger) {
    var _loader = $("#loading");
    switch (trigger) {
        case 'show':
            _loader.removeClass("hidden");
            break;
        case 'hide':
            _loader.addClass("hidden");
            break;
    }
}

// event.type РґРѕР»Р¶РµРЅ Р±С‹С‚СЊ keypress
function getChar(event) {
    if (event.which == null) { // IE
        if (event.keyCode < 32) return null; // СЃРїРµС†. СЃРёРјРІРѕР»
        return String.fromCharCode(event.keyCode)
    }

    if (event.which != 0 && event.charCode != 0) { // РІСЃРµ РєСЂРѕРјРµ IE
        if (event.which < 32) return null; // СЃРїРµС†. СЃРёРјРІРѕР»
        return String.fromCharCode(event.which); // РѕСЃС‚Р°Р»СЊРЅС‹Рµ
    }

    return null; // СЃРїРµС†. СЃРёРјРІРѕР»
}

function isAdmin() {
    return window.location.href.indexOf('admin') !== -1
}

function isAuthClient() {
    var isAuth = $('#order-form-management').attr('is-auth');
    if (isAuth == "1") {
        return true;
    }
    return false;
}

function loader(){
    let ajaxLoadTimeout, criticalStop;
    $(document).ajaxStart(function () {
        ajaxLoadTimeout = setTimeout(function () {
            $('#loading').removeClass('hidden');
        }, 2*1000);
        criticalStop = setTimeout(function () {
            $('#loading').addClass('hidden');
        }, 60*1000);
    }).ajaxSuccess(function () {
        clearTimeout(ajaxLoadTimeout);
        $('#loading').addClass('hidden');
    });
}
